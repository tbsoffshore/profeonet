<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Discipline;
use App\Model\Admin\Domain;
use App\Model\Admin\Areas;

use DB;

class AreasController extends Controller
{
    //
    public function index()
    {
        $data=array(
            'deleteAction'=>'areas/delete_action',
            'statusAction'=>'areas/change_action',
        );
        return view('admin.areas.list')->with($data); 
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $discipline = new Discipline;
        $discipline_list = $discipline->get();

        $domain = new Domain;
        $domain_list = $domain->get();

        $Areas_data = "";
        $data = array(
            'discipline_list'=>$discipline_list,
            'domain_list'=>$domain_list,
            'Areas_data'=>$Areas_data,
            'action'=>'areas/create_action',
            'heading'=>"Add"
        );
        return view('admin/areas/form')->with($data);
    }

    public function create_action(Request $request)
    { 
      //  print_r($request->post());exit();

        $validator = $this->validate($request, [
            'title' => 'required|unique:area_of_work',
            'mst_discipline_id' => 'required',
            'mst_domain_id' => 'required',
        ],
        [
            'title.required'    => 'Please enter title',
            'mst_discipline_id.required'    => 'Please select discipline',
            'mst_domain_id.required'    => 'Please select domain',
        ]);


        $area = new Areas;
        $area->mst_discipline_id=$request->mst_discipline_id;
        $area->mst_domain_id=$request->mst_domain_id;
        $area->title=$request->title;
        $area->status='Active';
        $area->created_at=date('Y-m-d H:i:s');
        $area->updated_at=date('Y-m-d H:i:s');
        $area->save(); 
     
        return redirect()->route('areas/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']);   
       
    }
    public function delete_action(Request $request)
    { 
        $Areas = new Areas;       
        $Areas->where('id', $request->id)->delete(); 
        return redirect()->route('areas/list')->with(['session'=>"Deleted Successfully",'alert-class'=>'alert-success']);   
    }
    public function change_action(Request $request)
    { 
        //print_r($request->id);exit();
        $Areas = new Areas;
        $Areas = $Areas->find($request->id);
        if($Areas->status=='Active')
        {
            $Areas->status='Inactive';
        }
        else
        {
            $Areas->status='Active';
        }
        $Areas->updated_at=date('Y-m-d H:i:s');
        $Areas->save(); 

        return redirect()->route('areas/list')->with(['session'=>"Change Status Successfully",'alert-class'=>'alert-success']);  
    }


    public function getDomains(Request $request)
    {     
       $domain_id = $request->post('domain_id');  
       //print_r();exit();
       $domain = new Domain;     
       $data = $domain->select('id','title')->where('mst_discipline_id', '=', $request->post('mst_discipline_id'))->get();

       $html = '<option value="">--Select Domain--</option>';

       foreach($data as $row)
       {
            if($row->id==$domain_id)
            {
                $selected= "selected";
            }
            else
            {
                $selected= "";
            }
         $html .= '<option value="'.$row->id.'" '.$selected.'>'.ucfirst($row->title).'</option>';
       }
      echo $html;exit; 
     
    }


    public function update($id)
    {
       // print_r($id);exit();
        //
        $discipline = new Discipline;
        $discipline_list = $discipline->get();

        $Areas = new Areas;
        $Areas_data = $Areas->where('id', $id)->first();

        $domain = new Domain;
        $domain_data = $domain->where('mst_discipline_id',$Areas_data->mst_discipline_id)->get();

       // echo  "<pre>"; print_r($Districts_data);exit();

        $data = array(
            'discipline_list'=>$discipline_list,
            'domain_data'=>$domain_data,
            'Areas_data'=>$Areas_data,
            'action'=>'areas/update_action',
            'heading'=>"Update"
        );
        return view('admin/areas/form')->with($data); 
    }


    public function update_action(Request $request)
    { 
        
        $this->validate($request,[
          'title' => 'required|unique:area_of_work,title,'.$request->id,
        ],
        [
            'title.required'    => 'Please enter  title',
        ]
        );

        $area = new Areas;
        $area = $area->find($request->id);
        $area->mst_discipline_id=$request->mst_discipline_id;
        $area->mst_domain_id=$request->mst_domain_id;
        $area->title=$request->title;
        $area->created_at=date('Y-m-d H:i:s');
        $area->updated_at=date('Y-m-d H:i:s');
        $area->save(); 
 
       return redirect()->route('areas/list')->with('message', 'Record has been updated successfully!');
    }
   
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
