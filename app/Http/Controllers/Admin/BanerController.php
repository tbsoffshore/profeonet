<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Baner;
use DB;
use Session;
use Input;
use Validator;
class BanerController extends Controller
{
    //
    public function index()
    {
        return view('admin.baners.list')->with(['deleteAction'=>'baners/delete_action','statusAction'=>'baners/change_action',]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin/baners/form')->with(['heading'=>"Add",'action'=>'baners/create_action']); 
    }
    public function create_action(Request $request)
    { 
        
        /*dd($_FILES);exit();*/
        $baners = new Baner;
        if ($request->hasFile('image')) 
        {
            $image = $request->file('image');
            $name = str_slug($request->image).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('assets/admin/dist/img/baners');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $baners->image = $name;
        }
        else
        {
            $baners->image = '';
        }
        $baners->title=ucwords($request->title);
        $baners->description=$request->description;
        $baners->image=$baners->image ;
        $baners->status='Active';
        $baners->created_at=date('Y-m-d H:i:s');
        $baners->updated_at=date('Y-m-d H:i:s');
        $baners->save();

        return redirect()->route('baners/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']);  
    }
    /*check duplication for Create & Update..*/
    public function check_duplication(Request $request)
    {
        $id = $request->post('id');
        /*print_r($id);exit;*/
        $response = array();
        $Baner = new Baner;
        if($id=='')
        {
            $Baner_data = $Baner->where('title', $request->post('title'))->get();
        }
        else
        {
            $Baner_data = $Baner->where('title', $request->post('title'))->where('id','<>',$id)->get();
            
        }
        //print_r($Baner_data);exit();
        
        if(count($Baner_data)> 0)
        {
            $response['error'] = 1;
            $response['message'] = "Already exist";
        }
        else
        {
            Session::put('message','Record created successfully');
            $response['error'] = 0;
            $response['message'] = 'success';
        }
        echo json_encode($response);exit; 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //update action
    public function update($id)
    {
        $baners = new Baner;
        $baners_data = $baners->where('id', $id)->first();  
        // dd($profession_data);     
        return view('admin/baners/form')->with(['baners_data'=>$baners_data,'action'=>'baners/update_action','heading'=>"Update"]); 
    }

    public function update_action(Request $request)
    { 
        $baners = new Baner;
        if ($request->hasFile('image')) 
        {
            $image = $request->file('image');
            $name = str_slug($request->image).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('assets/admin/dist/img/baners');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $baners->image = $name;

        }
        else
        {
            $baners->image = $request->old_image;
        }

        $baners = $baners->find($request->id);
        $baners->title=ucwords($request->title);
        $baners->description=$request->description;
        $baners->image =$baners->image;
        $baners->updated_at=date('Y-m-d H:i:s');
        $baners->save(); 
        return redirect()->route('baners/list')->with(['session'=>"Updated Successfully",'alert-class'=>'alert-success']);  
       
    }

    public function delete_action(Request $request)
    { 
        $baners = new Baner;       
        $baners->where('id', $request->id)->delete(); 
        return redirect()->route('baners/list')->with(['session'=>"Deleted Successfully",'alert-class'=>'alert-success']);
    }

    public function change_action(Request $request)
    { 
        $baners = new Baner;
        $baners = $baners->find($request->id);
        if($baners->status=='Active')
        {
             $baners->status='Inactive';
        }
        else
        {
             $baners->status='Active';
        }
        $baners->updated_at=date('Y-m-d H:i:s');
        $baners->save(); 
        return redirect()->route('baners/list')->with(['session'=>"Status has been updated successfully",'alert-class'=>'alert-success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
