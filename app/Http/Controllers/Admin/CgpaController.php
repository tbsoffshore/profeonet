<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Cgpa;
use App\Model\Admin\Discipline;
use DB;
use Session;

class CgpaController extends Controller
{
    //
    public function index()
    {
        
        return view('admin.cgpa.list')->with(['deleteAction'=>'cgpa/delete_action','statusAction'=>'cgpa/change_action',]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
     return view('admin/cgpa/form')->with(['heading'=>"Add",'action'=>'cgpa/create_action']);
       
    }
    public function create_action(Request $request)
    { 

        $this->validate($request,[
                'title' => 'required|unique:mst_cgpa',
            ],
            [
                'title.required'    => 'Please enter title',
            ]
            );

            $cgpa = new Cgpa;
            $cgpa->title=$request->title;
            $cgpa->status='Active';
            $cgpa->created_at=date('Y-m-d H:i:s');
            $cgpa->updated_at=date('Y-m-d H:i:s');
            $cgpa->save(); 
          return redirect()->route('cgpa/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']);
    }

     /*check duplication for Create & Update..*/
    public function check_duplication(Request $request)
    {
        $id = $request->post('id');
        $response = array();
        $cgpa = new Cgpa;
        if($id=='')
        {
            $cgpa_data = $cgpa->where('title', $request->post('title'))->get();
        }
        else
        {
            $cgpa_data = $cgpa->where('title', $request->post('title'))->where('id','<>',$id)->get();
            
        }
        
        if(count($cgpa_data)> 0)
        {
            $response['error'] = 1;
            $response['message'] = "Already exist";
        }
        else
        {
            Session::put('message','Record created successfully');
            $response['error'] = 0;
            $response['message'] = 'success';
        }
        echo json_encode($response);exit; 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //update action
    public function update($id)
    {

        $cgpa = new Cgpa;
        $cgpa_data = $cgpa->where('id', $id)->first();  
        // dd($cgpa_data);     
        return view('admin/cgpa/form')->with(['cgpa_data'=>$cgpa_data,'action'=>'cgpa/update_action','heading'=>"Update"]); 
    }

    public function update_action(Request $request)
    { 

        $this->validate($request,[

        'title' => 'required|unique:mst_cgpa,title,'.$request->id,
        ],
        [
                'title.required'    => 'Please enter title',
        ]
        );


        $cgpa = new Cgpa;
        $cgpa = $cgpa->find($request->id);
        $cgpa->title=$request->title;
        $cgpa->updated_at=date('Y-m-d H:i:s');
        $cgpa->save(); 
        return redirect()->route('cgpa/list')->with(['session'=>"Updated Successfully",'alert-class'=>'alert-success']);  
       
    }

    public function delete_action(Request $request)
    { 
        $cgpa = new Cgpa;       
        $cgpa->where('id', $request->id)->delete(); 
        return redirect()->route('cgpa/list')->with(['session'=>"Delete Successfully",'alert-class'=>'alert-success']);
    }

    public function change_action(Request $request)
    { 
        $cgpa = new Cgpa;
        $cgpa = $cgpa->find($request->id);
        if($cgpa->status=='Active')
        {
             $cgpa->status='Inactive';
        }
        else
        {
             $cgpa->status='Active';
        }
        $cgpa->updated_at=date('Y-m-d H:i:s');
        $cgpa->save(); 
        return redirect()->route('cgpa/list')->with(['session'=>"Status has been updated successfully",'alert-class'=>'alert-success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
