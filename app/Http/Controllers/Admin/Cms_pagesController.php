<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Cms_pages;
use DB;

class Cms_pagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.cms_pages.list')->with(['statusAction'=>'cms_pages/change_action','deleteAction'=>'cms_pages/delete_action']); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/cms_pages/form')->with(['action'=>'cms_pages/create_action']); 
    }

    public function create_action(Request $request)
    {   
        $cms_pages = new Cms_pages;
        $cms_pages->page_name=$request->page_name;
        $cms_pages->title=$request->title;
        $cms_pages->description=$request->description;
        $cms_pages->status='Active';
        $cms_pages->created_at=date('Y-m-d H:i:s');
        $cms_pages->updated_at=date('Y-m-d H:i:s');
        $cms_pages->save(); 
       /* $data = array(
                'session'=>"Created Successfully",
                'alert-class'=>'alert-success'
        );*/
        return redirect()->route('cms_pages/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']); 
       
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $cms_pages = new Cms_pages;
        $cms_pages = $cms_pages->where('id', $id)->first(); 
        return view('admin.cms_pages.view')->with(['cms_data'=>$cms_pages]); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //update action
    public function update($id)
    {
        //
        $cms_pages = new Cms_pages;
        $cms_pages = $cms_pages->where('id', $id)->first(); 
        return view('admin/cms_pages/form')->with(['cms_data'=>$cms_pages,'action'=>'cms_pages/update_action']); 
    }

    public function update_action(Request $request)
    { 
        $cms_pages = new Cms_pages;
        $cms_pages = $cms_pages->find($request->id);
        $cms_pages->title=$request->title;
        $cms_pages->description=$request->description;
        $cms_pages->updated_at=date('Y-m-d H:i:s');
        $cms_pages->save(); 
       /* $data = array(
                'session'=>"Updated Successfully",
                'alert-class'=>'alert-success'
        );*/
        return redirect()->route('cms_pages/list')->with(['session'=>"Updated Successfully",'alert-class'=>'alert-success']); 
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete_action(Request $request)
    { 
        $cms_pages = new Cms_pages;    
        $cms_pages->where('id', $request->id)->delete(); 
        /*$data= array(
            'session'=>"Deleted Successfully",
            'alert-class'=>'alert-success',
        );*/
        return redirect()->route('cms_pages/list')->with(['session'=>"Deleted Successfully",'alert-class'=>'alert-success']); 
    }

    public function change_action(Request $request)
    { 
        $cms_pages = new Cms_pages;
        $cms_pages = $cms_pages->find($request->id);
        if($cms_pages->status=='Active')
        {
             $cms_pages->status='Inactive';
        }
        else
        {
             $cms_pages->status='Active';
        }
        $cms_pages->updated_at=date('Y-m-d H:i:s');
        $cms_pages->save();
        /*$data= array(
            'session'=>"Change Status Successfully",
            'alert-class'=>'alert-success',
        ); */
        return redirect()->route('cms_pages/list')->with(['session'=>"=Status has been updated successfully",'alert-class'=>'alert-success']); 
    }
}
