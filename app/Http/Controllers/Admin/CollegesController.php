<?php 
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Domain;
use App\Model\Admin\Discipline;
use App\Model\Admin\Colleges;
use DB;

class CollegesController extends Controller
{
    //
    public function index()
    {
        $data=array(
            'deleteAction'=>'colleges/delete_action',
            'statusAction'=>'colleges/change_action',
        );
        return view('admin.colleges.list')->with($data); 
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       $discpline = new Discipline;
       $discpline_list = $discpline->get();

        $domain = new Domain;
        $domain_list = $domain->get();

        $Colleges_data="";

        $data = array(
           'discpline_list'=>$discpline_list,
            'domain_list'=>$domain_list,
            'Colleges_data'=>$Colleges_data,
            'action'=>'colleges/create_action',
            'heading'=>"Add"
        );
        return view('admin/colleges/form')->with($data);
    }

    public function create_action(Request $request)
    { 
      //  print_r($request->post());exit();

        $validator = $this->validate($request, [
            'title' => 'required|unique:colleges',
            'mst_discipline_id' => 'required',
            'mst_domain_id' => 'required',
        ],
        [
            'title.required'    => 'Please enter title',
            'mst_discipline_id.required'    => 'Please select discipline',
            'mst_domain_id.required'    => 'Please select domain',
        ]);

        $colleges = new Colleges;
        $colleges->mst_discipline_id=$request->mst_discipline_id;
        $colleges->mst_domain_id=$request->mst_domain_id;
        $colleges->title=$request->title;
        $colleges->status='Active';
        $colleges->created_at=date('Y-m-d H:i:s');
        $colleges->updated_at=date('Y-m-d H:i:s');
        $colleges->save(); 
     
        return redirect()->route('colleges/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']);   
       
    }
      public function delete_action(Request $request)
    { 
        $Colleges = new Colleges;       
        $Colleges->where('id', $request->id)->delete(); 
        return redirect()->route('colleges/list')->with(['session'=>"Deleted Successfully",'alert-class'=>'alert-success']);   
    }
     public function change_action(Request $request)
    { 
        //print_r($request->id);exit();
        $Colleges = new Colleges;
        $Colleges = $Colleges->find($request->id);
        if($Colleges->status=='Active')
        {
            $Colleges->status='Inactive';
        }
        else
        {
            $Colleges->status='Active';
        }
        $Colleges->updated_at=date('Y-m-d H:i:s');
        $Colleges->save(); 
        return redirect()->route('colleges/list')->with(['session'=>"Change Status Successfully",'alert-class'=>'alert-success']);  
    }


    public function getDomain(Request $request)
    {     
    //  print_r($_POST);exit();
       $domain = new Domain;     
       $data = $domain->select('id','title')->where('mst_discipline_id', '=', $request->post('mst_discipline_id'))->get();

       $html = '<option value="">--Select Domain--</option>';
       foreach($data as $row)
       {
         $html .= '<option value="'.$row->id.'">'.ucfirst($row->title).'</option>';
       }
      echo $html;exit; 
     
    }

    public function getColleges(Request $request)
    {     
       
       $colleges = new Colleges;       
       $data = $colleges->select('id','title')->where('mst_domain_id', '=', $request->post('mst_domain_id'))->get();

       $html = '<option value="">--Select Colleges--</option>';
       foreach($data as $row)
       {
         $html .= '<option value="'.$row->id.'">'.ucfirst($row->title).'</option>';
       }

       echo $html;exit; 
     
    }

     public function update($id)
    {
       //print_r($id);exit();
        //
        $discpline = new Discipline;
       $discpline_list = $discpline->get();


        $Colleges = new Colleges;
        $Colleges_data = $Colleges->where('id', $id)->first();
       
        $domain = new Domain;
        $domain_data = $domain->where('mst_discipline_id',$Colleges_data->mst_discipline_id)->get();
       // echo  "<pre>"; print_r($Districts_data);exit();

        $data = array(
            'discpline_list'=>$discpline_list,
            'domain_data'=>$domain_data,
            'Colleges_data'=>$Colleges_data,
            'action'=>'colleges/update_action',
            'heading'=>"Update"
        );
        return view('admin/colleges/form')->with($data); 
    }
   
   public function update_action(Request $request)
    { 
      // print_r($request->post());exit();
        $this->validate($request,[
          'title' => 'required|unique:colleges,title,'.$request->id,
        ],
        [
            'title.required'    => 'Please enter  title',
        ]
        );

        $colleges = new Colleges;
        $colleges = $colleges->find($request->id);
        $colleges->mst_discipline_id=$request->mst_discipline_id;
        $colleges->mst_domain_id=$request->mst_domain_id;
        $colleges->title=$request->title;
        $colleges->updated_at=date('Y-m-d H:i:s');
        $colleges->save(); 
     
        return redirect()->route('colleges/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']);   
       
    }
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
