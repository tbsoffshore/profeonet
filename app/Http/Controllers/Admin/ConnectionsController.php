<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Discipline;
use App\Model\Admin\Domain;
use App\Model\Admin\States;
use App\Model\Admin\Areas;
use App\Model\Admin\Colleges;

use App\Model\Admin\Connections;

use DB;
use Validator;

class ConnectionsController extends Controller
{
    //
    public function index()
    {
        $data=array(
            'deleteAction'=>'connections/delete_action',
            'statusAction'=>'connections/change_action',
        );
        return view('admin.connections.list')->with($data); 
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $discipline = new Discipline;
        $discipline_list = $discipline->get();

        $domain = new Domain;
        $domain_list = $domain->get();

        $state = new States;
        $state_data = $state->get(); 

        $area = new Areas;
        $area_data = $area->get();

        $college = new Colleges;
        $college_data = $college->get();

        $Connections_data="";

        $data = array(
            'discipline_list'=>$discipline_list,
            'domain_list'=>$domain_list,
            'state_data'=>$state_data,
            'area_data'=>$area_data,
            'college_data'=>$college_data,
            'action'=>'connections/create_action',
            'heading'=>"Add",
            'Connections_data'=>$Connections_data,
        );
        return view('admin/connections/form')->with($data);
    }

    public function create_action(Request $request)
    { 
      //  print_r($request->post());exit();

      $validator = $this->validate($request, [
            'mst_discipline_id' => 'required',
      'mst_domain_id' => 'required',
      'states_id' => 'required',
            /*'states_id' => 'required|unique:connections',*/
        ],
      [
      'mst_discipline_id.required'    => 'Please select discipline',
      'mst_domain_id.required'    => 'Please select domain',
      'states_id.required'    => 'Please select states',
    ]);

        /*if ($validator->fails()) {
            return redirect()
            ->route('connections/list')
                         ->withErrors($validator)
                        ->withInput();
        }*/

        $connection = new Connections;

        if ($request->hasFile('image')) 
        {
            $image = $request->file('image');
            $name = str_slug($request->image).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('assets/admin/dist/img/connections');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $connection->image = $name;
        }
        else
        {
            $connection->image = '';
        }

        $connection->mst_discipline_id=$request->mst_discipline_id;
        $connection->mst_domain_id=$request->mst_domain_id;
        $connection->area_of_work_id=$request->area_of_work_id;
        $connection->states_id=$request->states_id;
        $connection->image=$connection->image ;
        $connection->description=$request->description ;
        $connection->status='Active';
        $connection->created_at=date('Y-m-d H:i:s');
        $connection->updated_at=date('Y-m-d H:i:s');
        $connection->save(); 
     
        return redirect()->route('connections/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']);   
       
    }
    public function delete_action(Request $request)
    { 
        $Connections = new Connections;       
        $Connections->where('id', $request->id)->delete(); 
        return redirect()->route('connections/list')->with(['session'=>"Deleted Successfully",'alert-class'=>'alert-success']);   
    }
    public function change_action(Request $request)
    { 
        //print_r($request->id);exit();
        $Connections = new Connections;
        $Connections = $Connections->find($request->id);
        if($Connections->status=='Active')
        {
            $Connections->status='Inactive';
        }
        else
        {
            $Connections->status='Active';
        }
        $Connections->updated_at=date('Y-m-d H:i:s');
        $Connections->save(); 

        return redirect()->route('connections/list')->with(['session'=>"Change Status Successfully",'alert-class'=>'alert-success']);  
    }

/*Get Domains*/
    public function getDomains(Request $request)
    {     
       $domain_id = $request->post('domain_id');  
       //print_r();exit();
       $domain = new Domain;     
       $data = $domain->select('id','title')->where('mst_discipline_id', '=', $request->post('mst_discipline_id'))->get();

       $html = '<option value="">--Select Domain--</option>';

       foreach($data as $row)
       {
            if($row->id==$domain_id)
            {
                $selected= "selected";
            }
            else
            {
                $selected= "";
            }
         $html .= '<option value="'.$row->id.'" '.$selected.'>'.ucfirst($row->title).'</option>';
       }
      echo $html;exit; 
    }

    /*Get Area Of Work*/
    public function getAreaworks(Request $request)
    {     
       $area_of_work_id = $request->post('area_of_work_id');  
       $area = new Areas;     
       $data = $area->select('id','title')->where('mst_domain_id', '=', $request->post('mst_domain_id'))->get();

       $html = '<option value="">--Select Area of work--</option>';
       foreach($data as $row)
       {
            if($row->id==$area_of_work_id)
            {
                $selected= "selected";
            }
            else
            {
                $selected= "";
            }
         $html .= '<option value="'.$row->id.'" '.$selected.'>'.ucfirst($row->title).'</option>';
       }
       echo $html;exit;
      
    }

    public function getCollege(Request $request)
    {
       $colleges_id = $request->post('colleges_id');
       $colleges = new Colleges;     
       $data1 = $colleges->select('id','title')->where('mst_domain_id', '=', $request->post('mst_domain_id'))->get();

       $html = '<option value="">--Select Colleges--</option>';
       foreach($data1 as $row)
       {
            if($row->id==$colleges_id)
            {
                $selected= "selected";
            }
            else
            {
                $selected= "";
            }
         $html .= '<option value="'.$row->id.'" '.$selected.'>'.ucfirst($row->title).'</option>';
       }
       echo $html;exit;

    }

    public function update($id)
    {   
        /*print_r($id);exit;*/

        $discipline = new Discipline;
        $discipline_list = $discipline->get();

        $Connections = new Connections;
        $Connections_data = $Connections->where('id', $id)->first();

        $domain = new Domain;
        $domain_data = $domain->where('mst_discipline_id',$Connections_data->mst_discipline_id)->get();

        $states = new States;
        $state_data = $states->get();

        $area = new Areas;
        $area_data = $area->get();


        $data = array(
            'Connections_data'=>$Connections_data,
            'discipline_list'=>$discipline_list,
            'domain_data'=>$domain_data,
            'state_data'=>$state_data,
            'area_data'=>$area_data,
            'action'=>'connections/update_action',
            'heading'=>"Update"
        );
        return view('admin/connections/form')->with($data); 
    }


    public function update_action(Request $request)
    { 
    
        $connection = new Connections;
        if ($request->hasFile('image')) 
        {
            $image = $request->file('image');
            $name = str_slug($request->image).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('assets/admin/dist/img/connections');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $connection->image = $name;

        }
        else
        {
            $connection->image = $request->old_image;
        }

        $connection = $connection->find($request->id);
        $connection->mst_discipline_id=$request->mst_discipline_id;
        $connection->mst_domain_id=$request->mst_domain_id;
        $connection->area_of_work_id=$request->area_of_work_id;
        $connection->states_id=$request->states_id;
        $connection->image =$connection->image;
        $connection->description=$request->description ;
        $connection->created_at=date('Y-m-d H:i:s');
        $connection->updated_at=date('Y-m-d H:i:s');
        $connection->save(); 
 
       return redirect()->route('connections/list')->with('message', 'Record has been updated successfully!');
    }
   
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
