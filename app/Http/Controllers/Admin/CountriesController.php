<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Countries;
use DB;

class CountriesController extends Controller
{
    //
    public function index()
    {
        return view('admin.countries.list')->with(['deleteAction'=>'countries/delete_action','statusAction'=>'countries/change_action',]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin/countries/form')->with(['heading'=>"Add",'action'=>'countries/create_action']); 
    }

    public function create_action(Request $request)
    { 

         $this->validate($request,[
                'country_name' => 'required|unique:countries',
            ],
            [
               
                'country_name.required'    => 'Please enter country name ',
            ]
            );

         
        $countries = new Countries;
        $countries->country_name=$request->country_name;
        $countries->status='Active';
        $countries->created_at=date('Y-m-d H:i:s');
        $countries->updated_at=date('Y-m-d H:i:s');
        $countries->save(); 
        return redirect()->route('countries/list');  
       
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //update action
    public function update($id)
    {
        $countries = new Countries;
        $country_data = $countries->where('id', $id)->first();       
        return view('admin/countries/form')->with(['country_data'=>$country_data,'action'=>'countries/update_action','heading'=>"Update"]); 
    }

    public function update_action(Request $request)
    { 

        $this->validate($request,[
           
          'country_name' => 'required|unique:countries,country_name,'.$request->id,
        ],
        [
                'country_name.required'    => 'Please enter country name',
          ]
        );
        $countries = new Countries;
        $countries = $countries->find($request->id);
        $countries->country_name=$request->country_name;
        $countries->updated_at=date('Y-m-d H:i:s');
        $countries->save(); 
        return redirect()->route('countries/list');  
       
    }

    public function delete_action(Request $request)
    { 
        $countries = new Countries;       
        $countries->where('id', $request->id)->delete(); 
        return redirect()->route('countries/list');
    }

    public function change_action(Request $request)
    { 
        $countries = new Countries;
        $countries = $countries->find($request->id);
        if($countries->status=='Active')
        {
             $countries->status='Inactive';
        }
        else
        {
             $countries->status='Active';
        }
        $countries->updated_at=date('Y-m-d H:i:s');
        $countries->save(); 
        return redirect()->route('countries/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
