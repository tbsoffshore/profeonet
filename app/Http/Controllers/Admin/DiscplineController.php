<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Discipline;
use DataTables;

use DB;

class DiscplineController extends Controller
{
    //
     public function index()
    {

        return view('admin.discipline.list')->with(['deleteAction'=>'discipline/delete_action','statusAction'=>'discipline/change_action',]); 
    }


    
      function ajaxDisciplineList()

    {
        DB::statement(DB::raw('set @rownum=0'));
        $discipline = Discipline::select([

            DB::raw('@rownum  := @rownum  + 1 AS rownum'),

            'id',

            'discipline_title',

            'status',           

        ])->get();
       
        return DataTables::of($discipline)
        ->addColumn('action', function ($discipline) {

                return '<a href="'.route("discipline/update", $discipline->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$discipline->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

        })->make(true);

    }

   /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin/discipline/form')->with(['heading'=>"Add",'action'=>'discipline/create_action']); 
    }

    public function create_action(Request $request)
    { 

        $this->validate($request,[
                'discipline_title' => 'required|unique:mst_discipline',
            ],
            [
                'discipline_title.required'    => 'Please enter title',
            ]
            );

        $Discipline = new Discipline;
        $Discipline->discipline_title=$request->discipline_title;
        $Discipline->status='Active';
        $Discipline->created_at=date('Y-m-d H:i:s');
        $Discipline->updated_at=date('Y-m-d H:i:s');
        $Discipline->save(); 
        
        return redirect()->route('discipline/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']); 
       
    }
    
    
     /*check duplication for Create & Update..*/
    public function check_duplication(Request $request)
    {
          print_r("expression");exit;
        $id = $request->post('id');
        print_r($id);exit;
        $response = array();
        $Discipline = new Discipline;
        if($id=='')
        {
            $Discipline_data = $Discipline->where('discipline_title', $request->post('discipline_title'))->get();
        }
        else
        {
            $Discipline_data = $Discipline->where('discipline_title', $request->post('discipline_title'))->where('id','<>',$id)->get();
            
        }
       // print_r($Profession_data);exit();
        
        if(count($Discipline_data)> 0)
        {
            $response['error'] = 1;
            $response['message'] = "Already exist";
        }
        else
        {
            Session::put('message','Record created successfully');
            $response['error'] = 0;
            $response['message'] = 'success';
        }
        echo json_encode($response);exit; 
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //update action
    public function update($id)
    {
        $Discipline_data = new Discipline;
        $Discipline_data = $Discipline_data->where('id', $id)->first();  
        // dd($profession_data);     
        return view('admin/discipline/form')->with(['Discipline_data'=>$Discipline_data,'action'=>'discipline/update_action','heading'=>"Update"]); 
    }

    public function update_action(Request $request)
    { 

        $this->validate($request,[
          'discipline_title' => 'required|unique:mst_discipline,discipline_title,'.$request->id,
        ],
        [
                'discipline_title.required'    => 'Please enter title',
          ]
        );
        $disciplin = new Discipline;
        $disciplin = $disciplin->find($request->id);
        $disciplin->discipline_title=$request->discipline_title;
        $disciplin->updated_at=date('Y-m-d H:i:s');
        $disciplin->save(); 
        return redirect()->route('discipline/list')->with(['session'=>"Updated Successfully",'alert-class'=>'alert-success']);
       
    }

    public function delete_action(Request $request)
    { 
        $discipline = new Discipline;       
        $discipline->where('id', $request->id)->delete(); 
        return redirect()->route('discipline/list')->with(['session'=>"Deleted Successfully",'alert-class'=>'alert-success']);
    }

    public function change_action(Request $request)
    { 
        $discipline = new Discipline;
        $discipline = $discipline->find($request->id);
        if($discipline->status=='Active')
        {
             $discipline->status='Inactive';
        }
        else
        {
             $discipline->status='Active';
        }
        $discipline->updated_at=date('Y-m-d H:i:s');
        $discipline->save(); 
        return redirect()->route('discipline/list')->with(['session'=>"Status has been updated successfully",'alert-class'=>'alert-success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
