<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Countries;
use App\Model\Admin\States;
use App\Model\Admin\Districts;

use DB;

class DistrictsController extends Controller
{
    public function index()
    {
        $data=array(
            'deleteAction'=>'districts/delete_action',
            'statusAction'=>'districts/change_action',
        );
        return view('admin.districts.list')->with($data); 
    }

    public function create()
    {
        //
        $country = new Countries;
        $country_list = $country->get();

        $state = new States;
        $state_list = $state->get();

        $data = array(
            'country_list'=>$country_list,
            'state_list'=>$state_list,
            'action'=>'districts/create_action',
            'heading'=>"Add"
        );
        return view('admin/districts/form')->with($data);
    }

    public function create_action(Request $request)
    { 

         $this->validate($request,[
                'country_id' => 'required',
                'state_id' => 'required',
                'district_name' => 'required|unique:districts',
            ],
            [
                'country_id.required'    => 'Please select country',
                'state_id.required'    => 'Please select state',
                'district_name.required'    => 'Please enter district name',
            ]
            );
      //  print_r($request->post());exit();
        $district = new Districts;
        $district->country_id=$request->country_id;
        $district->state_id=$request->state_id;
        $district->district_name=$request->district_name;
        $district->status='Active';
        $district->created_at=date('Y-m-d H:i:s');
        $district->updated_at=date('Y-m-d H:i:s');
        $district->save(); 
     
        return redirect()->route('districts/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']);   
       
    }
      public function delete_action(Request $request)
    { 
        $Districts = new Districts;       
        $Districts->where('id', $request->id)->delete(); 
        return redirect()->route('districts/list')->with(['session'=>"Deleted Successfully",'alert-class'=>'alert-success']);   
    }
     public function change_action(Request $request)
    { 
        //print_r($request->id);exit();
        $Districts = new Districts;
        $Districts = $Districts->find($request->id);
        if($Districts->status=='Active')
        {
            $Districts->status='Inactive';
        }
        else
        {
            $Districts->status='Active';
        }
        $Districts->updated_at=date('Y-m-d H:i:s');
        $Districts->save(); 
        return redirect()->route('districts/list')->with(['session'=>"Change Status Successfully",'alert-class'=>'alert-success']);  
    }


    public function getStates(Request $request)
    {     
      /* print_r($_POST);exit();*/
       $state_id =$request->post('state_id');
       $state = new States;     
       $data = $state->select('id','state_name')->where('country_id', '=', $request->post('country_id'))->get();

       $html = '<option value="">--Select State--</option>';
       foreach($data as $row)
       {
         if($row->id == $state_id)
         {
            $selected= "selected";
         }
         else
         {
            $selected= "";
         }

         $html .= '<option value="'.$row->id.'" '.$selected.'>'.ucfirst($row->state_name).'</option>';
        }
       
      echo $html;exit; 
     
    }

    public function getDistricts(Request $request)
    {     
       
       $districts = new Districts;       
       $data = $districts->select('id','district_name')->where('state_id', '=', $request->post('state_id'))->get();

       $html = '<option value="">--Select District--</option>';
       foreach($data as $row)
       {
         $html .= '<option value="'.$row->id.'">'.ucfirst($row->district_name).'</option>';
       }

       echo $html;exit; 
     
    }

    public function update($id)
    {
        $country = new Countries;
        $country_list = $country->get();

        $Districts = new Districts;
        $Districts_data = $Districts->where('id', $id)->first();

        $states = new States;
        $state_data = $states->where('country_id',$Districts_data->country_id)->get();
       // echo  "<pre>"; print_r($Districts_data);exit();
       
        $data = array(
            'country_list'=>$country_list,
            'state_data'=>$state_data,
            'Districts_data'=>$Districts_data,
            'country_id'=>$Districts_data->country_id,
            'action'=>'districts/update_action',
            'heading'=>"Update"
        );
        return view('admin/districts/form')->with($data); 
        
       
    }

    public function update_action(Request $request)
    {

        $this->validate($request,[
            'country_id' => 'required',
            'state_id' => 'required',
            'district_name' => 'required|unique:districts,district_name,'.$request->id,'country_id,'.$request->country_id,'state_id,'.$request->state_id,
        ],
        [
                'country_id.required'    => 'Please select country',
                'state_id.required'    => 'Please select state',
                'district_name.required'    => 'Please enter district name',
          ]
        );
        /*print_r($_POST);exit();*/
        $district = new Districts;
        $district = $district->find($request->id);
        $district->country_id=$request->country_id;
        $district->state_id=$request->state_id;
        $district->district_name=$request->district_name;
        $district->created_at=date('Y-m-d H:i:s');
        $district->updated_at=date('Y-m-d H:i:s');
        $district->save();
        return redirect()->route('districts/list')->with(['session'=>"Updated Successfully",'alert-class'=>'alert-success']);
    }
   
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
