<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Domain;
use App\Model\Admin\Discipline;
use DB;
use Session;

class DomainController extends Controller
{
    //
    public function index()
    {
        
        return view('admin.domain.list')->with(['deleteAction'=>'domain/delete_action','statusAction'=>'domain/change_action',]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $discipline = new Discipline;
        $discipline_list = $discipline->get();
        return view('admin/domain/form')->with(['discipline_list'=>$discipline_list,'heading'=>"Add",'action'=>'domain/create_action']);
       
    }

  
    
    public function create_action(Request $request)
    { 

        $this->validate($request,[
                'mst_discipline_id' => 'required',
                'title' => 'required|unique:mst_domain',
            ],
            [
                'mst_discipline_id.required'    => 'Please select discipline',
                'title.required'    => 'Please enter title',
            ]
            );

        $domain = new Domain;
         $domain->mst_discipline_id=$request->mst_discipline_id;
            $domain->title=$request->title;
            $domain->status='Active';
            $domain->created_at=date('Y-m-d H:i:s');
            $domain->updated_at=date('Y-m-d H:i:s');
            $domain->save(); 
          return redirect()->route('domain/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']);
    }

     /*check duplication for Create & Update..*/
    public function check_duplication(Request $request)
    {
        $id = $request->post('id');
        /*print_r($id);exit;*/
        $response = array();
        $Domain = new Domain;
        if($id=='')
        {
            $domain_data = $Domain->where('title', $request->post('title'))->get();
        }
        else
        {
            $domain_data = $Domain->where('title', $request->post('title'))->where('id','<>',$id)->get();
            
        }
        //print_r($Baner_data);exit();
        
        if(count($domain_data)> 0)
        {
            $response['error'] = 1;
            $response['message'] = "Already exist";
        }
        else
        {
            Session::put('message','Record created successfully');
            $response['error'] = 0;
            $response['message'] = 'success';
        }
        echo json_encode($response);exit; 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //update action
    public function update($id)
    {

       $discipline = new Discipline;
        $discipline_list = $discipline->get();

        $domain = new Domain;
        $domain_data = $domain->where('id', $id)->first();  
        // dd($domain_data);     
        return view('admin/domain/form')->with(['discipline_list'=>$discipline_list,'domain_data'=>$domain_data,'action'=>'domain/update_action','heading'=>"Update"]); 
    }

    public function update_action(Request $request)
    { 

        $this->validate($request,[
          'mst_discipline_id' => 'required',
          'title' => 'required|unique:mst_domain,title,'.$request->id,'mst_discipline_id,'.$request->mst_discipline_id,
        ],
        [
                'mst_discipline_id.required'    => 'Please select discpline',
                'title.required'    => 'Please enter title',
          ]
        );


        $domain = new Domain;
        $domain = $domain->find($request->id);
        $domain->mst_discipline_id=$request->mst_discipline_id;
        $domain->title=$request->title;
        $domain->updated_at=date('Y-m-d H:i:s');
        $domain->save(); 
        return redirect()->route('domain/list')->with(['session'=>"Updated Successfully",'alert-class'=>'alert-success']);  
       
    }

    public function delete_action(Request $request)
    { 
        $domain = new Domain;       
        $domain->where('id', $request->id)->delete(); 
        return redirect()->route('domain/list')->with(['session'=>"Delete Successfully",'alert-class'=>'alert-success']);
    }

    public function change_action(Request $request)
    { 
        $domain = new Domain;
        $domain = $domain->find($request->id);
        if($domain->status=='Active')
        {
             $domain->status='Inactive';
        }
        else
        {
             $domain->status='Active';
        }
        $domain->updated_at=date('Y-m-d H:i:s');
        $domain->save(); 
        return redirect()->route('domain/list')->with(['session'=>"Status has been updated successfully",'alert-class'=>'alert-success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
