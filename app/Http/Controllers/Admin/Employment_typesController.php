<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Employment_type;
use DB;

class Employment_typesController extends Controller
{
    //
    public function index()
    {
        return view('admin.employment_type.list')->with(['deleteAction'=>'employment_types/delete_action','statusAction'=>'employment_types/change_action',]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin/employment_type/form')->with(['heading'=>"Add",'action'=>'employment_types/create_action']); 
    }

    public function create_action(Request $request)
    { 
        $employment_type = new Employment_type;
        $employment_type->title=$request->title;
        $employment_type->status='Active';
        $employment_type->created_at=date('Y-m-d H:i:s');
        $employment_type->updated_at=date('Y-m-d H:i:s');
        $employment_type->save(); 
        return redirect()->route('employment_types/list');  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //update action
    public function update($id)
    {
        //
        $employment_type = new Employment_type;
        $emp_type_data = $employment_type->where('id', $id)->first();       
        return view('admin/employment_type/form')->with(['emp_type_data'=>$emp_type_data,'action'=>'employment_types/update_action','heading'=>"Update"]); 
    }

    public function update_action(Request $request)
    { 
        $employment_type = new Employment_type;
        $employment_type = $employment_type->find($request->id);
        $employment_type->title=$request->title;
        $employment_type->status='Active';
        $employment_type->updated_at=date('Y-m-d H:i:s');
        $employment_type->save(); 
        return redirect()->route('employment_types/list');  
       
    }

    public function delete_action(Request $request)
    { 
        $employment_type = new Employment_type;       
        $employment_type->where('id', $request->id)->delete(); 
        return redirect()->route('employment_types/list');
    }

    public function change_action(Request $request)
    { 
        $employment_type = new Employment_type;
        $employment_type = $employment_type->find($request->id);
        if($employment_type->status=='Active')
        {
             $employment_type->status='Inactive';
        }
        else
        {
             $employment_type->status='Active';
        }
        $employment_type->updated_at=date('Y-m-d H:i:s');
        $employment_type->save(); 
        return redirect()->route('employment_types/list');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
