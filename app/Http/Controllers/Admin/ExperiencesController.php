<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Experiences;
use DB;

class ExperiencesController extends Controller
{
    //
    public function index()
    {
        return view('admin.experiences.list')->with(['deleteAction'=>'experiences/delete_action','statusAction'=>'experiences/change_action',]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin/experiences/form')->with(['heading'=>"Add",'action'=>'experiences/create_action']); 
    }

    public function create_action(Request $request)
    { 
        $experience = new Experiences;

        $experience->title=$request->title;
        $experience->status='Active';
        $experience->created_at=date('Y-m-d H:i:s');
        $experience->updated_at=date('Y-m-d H:i:s');
        $experience->save(); 
        return redirect()->route('experiences/list');  
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //update action
    public function update($id)
    {
        //
        $experiences = new Experiences;
        $experience_data = $experiences->where('id', $id)->first();       
        return view('admin/experiences/form')->with(['experience_data'=>$experience_data,'action'=>'experiences/update_action','heading'=>"Update"]); 
    }

    public function update_action(Request $request)
    { 
        $experiences = new Experiences;
        $experiences = $experiences->find($request->id);
        $experiences->title=$request->title;
        $experiences->status='Active';
        $experiences->updated_at=date('Y-m-d H:i:s');
        $experiences->save(); 
        return redirect()->route('experiences/list');  
       
    }

    public function delete_action(Request $request)
    { 
        $experiences = new Experiences;       
        $experiences->where('id', $request->id)->delete(); 
        return redirect()->route('experiences/list');
    }

    public function change_action(Request $request)
    { 
        $experiences = new Experiences;
        $experiences = $experiences->find($request->id);
        if($experiences->status=='Active')
        {
             $experiences->status='Inactive';
        }
        else
        {
             $experiences->status='Active';
        }
        $experiences->updated_at=date('Y-m-d H:i:s');
        $experiences->save(); 
        return redirect()->route('experiences/list');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
