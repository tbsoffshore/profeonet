<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Jobs;
use DB;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.jobs.list')->with(['statusAction'=>'skills/change_action','deleteAction'=>'skills/delete_action']); 
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function job_view($id)
    {
        $jobs = new Jobs;
        $jobs_data = $jobs->where('fld_id', $id)->first(); 
        return view('admin.jobs.view')->with(['jobs_data'=>$jobs_data]); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change_action(Request $request)
    { 
        $skill = new Skills;
        $skill = $skill->find($request->id);
        if($skill->status=='Active')
        {
             $skill->status='Inactive';
        }
        else
        {
             $skill->status='Active';
        }
        $skill->updated_at=date('Y-m-d H:i:s');
        $skill->save(); 
        return redirect()->route('skills/list');
    }
}
