<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\NotificationCategory;
use DB;
use DataTables;
use Session;

class NotificationCategoryController extends Controller
{
    //
    public function index()
    {
        return view('admin.notification_category.list')->with(['deleteAction'=>'notification/delete_action','statusAction'=>'notification/change_action',]); 
    }

       
      function ajaxNotificationList()
    {
        //print_r("expression");
        DB::statement(DB::raw('set @rownum=0'));
        $notification = NotificationCategory::select([

            DB::raw('@rownum  := @rownum  + 1 AS rownum'),

            'id',

            'title',

            'status',           

        ])->get();
       
        return DataTables::of($notification)
        ->addColumn('action', function ($notification) {

                return '<a href="'.route("notification/update", $notification->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$notification->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

        })->make(true);

    }

   /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin/notification_category/form')->with(['heading'=>"Add",'action'=>'notification/create_action']); 
    }

    public function create_action(Request $request)
    { 

        $this->validate($request,[
                'title' => 'required|unique:mst_notification_categories',
            ],
            [
                'title.required'    => 'Please enter title',
            ]
            );

        $notification = new NotificationCategory;
        $notification->title=$request->title;
        $notification->status='Active';
        $notification->created_at=date('Y-m-d H:i:s');
        $notification->updated_at=date('Y-m-d H:i:s');
        $notification->save(); 
        
        return redirect()->route('notification/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']); 
       
    }
    
    
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //update action
    public function update($id)
    {
        $notification_data = new NotificationCategory;
        $notification_data = $notification_data->where('id', $id)->first();  
        // dd($profession_data);     
        return view('admin/notification_category/form')->with(['notification_data'=>$notification_data,'action'=>'notification/update_action','heading'=>"Update"]); 
    }

    public function update_action(Request $request)
    { 

        $this->validate($request,[
          'title' => 'required|unique:mst_notification_categories,title,'.$request->id,
        ],
        [
                'title.required'    => 'Please enter title',
          ]
        );
        $notification = new NotificationCategory;
        $notification = $notification->find($request->id);
        $notification->title=$request->title;
        $notification->updated_at=date('Y-m-d H:i:s');
        $notification->save(); 
        return redirect()->route('notification/list')->with(['session'=>"Updated Successfully",'alert-class'=>'alert-success']);
       
    }

    public function delete_action(Request $request)
    { 
        $notification = new NotificationCategory;       
        $notification->where('id', $request->id)->delete(); 
        return redirect()->route('notification/list')->with(['session'=>"Deleted Successfully",'alert-class'=>'alert-success']);
    }

    public function change_action(Request $request)
    { 
        $notification = new NotificationCategory;
        $notification = $notification->find($request->id);
        if($notification->status=='Active')
        {
             $notification->status='Inactive';
        }
        else
        {
             $notification->status='Active';
        }
        $notification->updated_at=date('Y-m-d H:i:s');
        $notification->save(); 
        return redirect()->route('notification/list')->with(['session'=>"Status has been updated successfully",'alert-class'=>'alert-success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
