<?php 
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Countries;
use App\Model\Admin\States;
use App\Model\Admin\Districts;
use App\Model\Admin\Domain;
use App\Model\Admin\Discipline;
use App\Model\Admin\Scopes;
use App\Model\Admin\NotificationCategory;
use App\Model\Admin\Notifications;
use DataTables;
use DB;

class NotificationsController extends Controller
{
    //
    public function index()
    {
        $data=array(
            'deleteAction'=>'notifications/delete_action',
            'statusAction'=>'notifications/change_action',
        );
        return view('admin.notifications.list')->with($data); 
    }


    function ajaxNotificationsList()

    {

        

        $notifications = new Notifications;

     
        $notification_list = $notifications->select('id','title','status','description','notification_date','website','created_via','mst_domain_id','mst_discipline_id','mst_notification_category_id','scope_id','country_id','state_id','district_id')->with('discipline')->with('domain')->get();      

        return DataTables::of($notification_list)->addColumn('action', function ($colleges) {

                return '<a href="'.url("admin/notifications/update/".$notifications->id."").'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$notifications->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

        })->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       $countries = new Countries;
       $country_list = $countries->where('status','Active')->get();

       $state = new States;
       $state_list = $state->get();

       $district = new Districts;
       $district_list = $district->get();
        
       $ncategory = new NotificationCategory;
       $ncategory_list = $ncategory->get();

       $discpline = new Discipline;
       $discpline_list = $discpline->get();

        $domain = new Domain;
        $domain_list = $domain->get();

       $scope = new Scopes;
       $scope_list = $scope->get();

        $Notifications_data="";

        $data = array(
            'country_list'=>$country_list,
            'state_list'=>$state_list,
            'district_list'=>$district_list,
            'scope_list'=>$scope_list,
            'ncategory_list'=>$ncategory_list,
            'discpline_list'=>$discpline_list,
            'domain_list'=>$domain_list,
            'domain_list'=>$domain_list,
            'Notifications_data'=>$Notifications_data,
            'action'=>'notifications/create_action',
            'heading'=>"Add"
        );
        return view('admin/notifications/form')->with($data);
    }

    public function create_action(Request $request)
    { 
    // print_r($request->post());exit();

        /*$validator = $this->validate($request, [
            'title' => 'required|unique:colleges',
            'mst_discipline_id' => 'required',
            'mst_domain_id' => 'required',
        ],
        [
            'title.required'    => 'Please enter title',
            'mst_discipline_id.required'    => 'Please select discipline',
            'mst_domain_id.required'    => 'Please select domain',
        ]);
*/
        $notifications = new Notifications;
        $notifications->scope_id=$request->scope_id;
        $notifications->mst_notification_category_id=$request->mst_notification_category_id;
        $notifications->mst_discipline_id=$request->mst_discipline_id;
        $notifications->mst_domain_id=$request->mst_domain_id;
        $notifications->country_id=$request->country_id;
        $notifications->state_id=$request->state_id;
        $notifications->district_id=$request->district_id;
        $notifications->title=$request->title;
        $notifications->description=$request->description;
        $notifications->notification_date=$request->notification_date;
        $notifications->website=$request->website;
        $notifications->status='Active';
        $notifications->created_via='Admin';
        $notifications->created_at=date('Y-m-d H:i:s');
        $notifications->updated_at=date('Y-m-d H:i:s');
        $notifications->save(); 
     
        return redirect()->route('notifications/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']);   
       
    }
      public function delete_action(Request $request)
    { 
        $notifications = new Notifications;       
        $notifications->where('id', $request->id)->delete(); 
        return redirect()->route('notifications/list')->with(['session'=>"Deleted Successfully",'alert-class'=>'alert-success']);   
    }
     public function change_action(Request $request)
    { 
        //print_r($request->id);exit();
        $notifications = new Notifications;
        $notifications = $Colleges->find($request->id);
        if($notifications->status=='Active')
        {
            $notifications->status='Inactive';
        }
        else
        {
            $notifications->status='Active';
        }
        $notifications->updated_at=date('Y-m-d H:i:s');
        $notifications->save(); 
        return redirect()->route('colleges/list')->with(['session'=>"Change Status Successfully",'alert-class'=>'alert-success']);  
    }


    public function getDomain(Request $request)
    {     
    //  print_r($_POST);exit();
       $domain = new Domain;     
       $data = $domain->select('id','title')->where('mst_discipline_id', '=', $request->post('mst_discipline_id'))->get();

       $html = '<option value="">--Select Domain--</option>';
       foreach($data as $row)
       {
         $html .= '<option value="'.$row->id.'">'.ucfirst($row->title).'</option>';
       }
      echo $html;exit; 
     
    }

    public function getColleges(Request $request)
    {     
       
       $colleges = new Colleges;       
       $data = $colleges->select('id','title')->where('mst_domain_id', '=', $request->post('mst_domain_id'))->get();

       $html = '<option value="">--Select Colleges--</option>';
       foreach($data as $row)
       {
         $html .= '<option value="'.$row->id.'">'.ucfirst($row->title).'</option>';
       }

       echo $html;exit; 
     
    }

     public function update($id)
    {
       //print_r($id);exit();
        //
        $discpline = new Discipline;
       $discpline_list = $discpline->get();


        $Notifications = new Notifications;
        $Notifications_data = $Notifications->where('id', $id)->first();
       
        $domain = new Domain;
        $domain_data = $domain->where('mst_discipline_id',$Colleges_data->mst_discipline_id)->get();
       // echo  "<pre>"; print_r($Districts_data);exit();

        $data = array(
            'discpline_list'=>$discpline_list,
            'domain_data'=>$domain_data,
            'Notifications_data'=>$Notifications_data,
            'action'=>'notifications/update_action',
            'heading'=>"Update"
        );
        return view('admin/notifications/form')->with($data); 
    }
   
   public function update_action(Request $request)
    { 
      // print_r($request->post());exit();
        /*$this->validate($request,[
          'title' => 'required|unique:colleges,title,'.$request->id,
        ],
        [
            'title.required'    => 'Please enter  title',
        ]
        );*/

        $notifications = new Notifications;
        $notifications = $notifications->find($request->id);
        $notifications->mst_discipline_id=$request->mst_discipline_id;
        $notifications->mst_domain_id=$request->mst_domain_id;
        $notifications->title=$request->title;
        $notifications->updated_at=date('Y-m-d H:i:s');
        $notifications->save(); 
     
        return redirect()->route('notifications/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']);   
       
    }




    public function getStates(Request $request)
    {     
    //  print_r($_POST);exit();
       $state = new States;     
       $data = $state->select('id','state_name')->where('country_id', '=', $request->post('country_id'))->get();

       $html = '<option value="">--Select State--</option>';
       foreach($data as $row)
       {
         $html .= '<option value="'.$row->id.'">'.ucfirst($row->state_name).'</option>';
       }
      echo $html;exit; 
     
    }

    public function getDistricts(Request $request)
    {     
    //  print_r($_POST);exit();
       $district = new Districts;     
       $data = $district->select('id','district_name')->where('state_id', '=', $request->post('state_id'))->get();

       $html = '<option value="">--Select District--</option>';
       foreach($data as $row)
       {
         $html .= '<option value="'.$row->id.'">'.ucfirst($row->district_name).'</option>';
       }
      echo $html;exit; 
     
    }
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
