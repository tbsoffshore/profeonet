<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Model\Admin\OurPatners;
use Input;
use DB;

class OurPatnersController extends Controller
{
    //d
    public function index()
    {
    	$data=  array(
    		'deleteAction'=>'ourpatners/delete_action',
    		'statusAction'=>'ourpatners/change_action',
    		'heading'=>'Our Partners List'
    	);
        return view('admin/ourpatners/list')->with($data); 
    }
    public function create()
    {
        $data=  array(
    		'heading'=>'Add ',
    		'action'=>'ourpatners/create_action',
    	);
        return view('admin/ourpatners/form')->with($data); 
    }
    public function create_action(Request $request)
    { 
    	
			$OurPatners = new OurPatners;
			if ($request->hasFile('image')) 
			{
				$image = $request->file('image');
				$name = str_slug($request->image).'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('assets/admin/dist/img/ourpatners');
				$imagePath = $destinationPath. "/".  $name;
				$image->move($destinationPath, $name);
				$OurPatners->image = $name;

			}
			else{
				$OurPatners->image = '';
			}

			$OurPatners->name 			=ucwords($request->name);
			$OurPatners->description 	=$request->description;
			$OurPatners->image			=$OurPatners->image	;
			$OurPatners->status 		='Active';
			$OurPatners->created_at 	=date('Y-m-d H:i:s');
			$OurPatners->updated_at 	=date('Y-m-d H:i:s');
			$OurPatners->save(); 
			$data = array(
				'session'=>"Created Successfully",
				'alert-class'=>'alert-success'
			);
			return redirect()->route('ourpatners/list')->with($data);
    }
     public function update($id)
    {
        $OurPatners = new OurPatners;
        $OurPatners_data = $OurPatners->where('id', $id)->first();    
        $data = array(
        	'OurPatners'=>$OurPatners_data,
        	'action'=>'ourpatners/update_action',
        	'heading'=>"Update",
    	);
        return view('admin/ourpatners/form')->with($data); 
    }

    public function update_action(Request $request)
    { 
      $OurPatners = new OurPatners;
      if ($request->hasFile('image')) 
	  {
        $image = $request->file('image');
        $name = str_slug($request->image).'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('assets/admin/dist/img/ourpatners');
        $imagePath = $destinationPath. "/".  $name;
        $image->move($destinationPath, $name);
        $OurPatners->image = $name;
        
      }
      else{
      	$OurPatners->image = $request->old_image;
      }
      	$OurPatners 				= $OurPatners->find($request->id);
      	$OurPatners->name 			=ucwords($request->name);
        $OurPatners->description 	=$request->description;
        $OurPatners->image			=$OurPatners->image	;
        $OurPatners->status 		='Active';
        $OurPatners->updated_at 	=date('Y-m-d H:i:s');
        $OurPatners->save(); 
        $data = array(
        	'session'=>"Updated Successfully",
        	'alert-class'=>'alert-success'
        );
        return redirect()->route('ourpatners/list')->with($data);    
    }
    public function delete_action(Request $request)
    { 
        $OurPatners = new OurPatners;       
        $OurPatners = $OurPatners->find($request->id);
        
        if(file_exists('assets/admin/dist/img/ourpatners'.'/'.$OurPatners->image))
        {
		    @unlink('assets/admin/dist/img/ourpatners'.'/'.$OurPatners->image);
		}
		$OurPatners->where('id',$request->id)->delete();

        $data= array(
        	'session'=>"Deleted Successfully",
        	'alert-class'=>'alert-success',
        );
        return redirect()->route('ourpatners/list')->with($data);   ;
    }

    public function change_action(Request $request)
    { 
        $OurPatners = new OurPatners;
        $OurPatners = $OurPatners->find($request->id);
        if($OurPatners->status=='Active')
        {
            $OurPatners->status='Inactive';
        }
        else
        {
            $OurPatners->status='Active';
        }
        $OurPatners->updated_at=date('Y-m-d H:i:s');
        $OurPatners->save(); 
        $data= array(
        	'session'=>"Change Status Successfully",
        	'alert-class'=>'alert-success',
        );
        return redirect()->route('ourpatners/list')->with($data);  
    }
}
