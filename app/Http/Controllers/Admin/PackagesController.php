<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Packages;
use DB;
use Redirect;
use Input;

class PackagesController extends Controller
{
    public function index()
    {
    	$data = array(
    		'deleteAction'=>'admin/packages/delete_action',
    		'statusAction'=>'admin/packages/change_action',
    		'create'=>'admin/packages/create',
    		'heading'=>'Packages List'
    	);
    	return view('admin/packages/list')->with($data);
    }
    public function create()
    {
    	 $data=  array(
    		'heading'=>'Add ',
    		'action'=>'admin/packages/create_action',
    		'height'=>'',
    		'width'=>'',
    	);
    	 return view('admin/packages/form')->with($data);
    }
    
    public function create_action(Request $request)
    {
    	 //print_r($_FILES);exit();
    	$Packages = new Packages;
    	if ($request->hasFile('package_image')) 
		{
			$package_image = $request->file('package_image');
			$name = str_slug($request->package_image).'.'.$package_image->getClientOriginalExtension();
			$destinationPath = public_path('assets/admin/dist/img/packages');
			$package_imagePath = $destinationPath. "/".  $name;
			$package_image->move($destinationPath, $name);
			$Packages->package_image = $name;
		}
		else
		{
			$Packages->package_image = '';
		}
		$Packages->package_title 			=ucwords($request->package_title);
		$Packages->package_description 		=$request->package_description;
		$Packages->package_amount			=$request->package_amount;
		$Packages->package_duration			=$request->package_duration;
		$Packages->package_image			=$Packages->package_image;
		$Packages->status 					='Active';
		$Packages->created_at 				=date('Y-m-d H:i:s');
		$Packages->updated_at 				=date('Y-m-d H:i:s');
		$Packages->save(); 
		$data = array(
				'session'=>"Created Successfully",
				'alert-class'=>'alert-success'
		);
		return Redirect::to('admin/packages')->with($data);		
    }

    public function update($id)
    {
        $Packages = new Packages;
        $Packages = $Packages->where('id', $id)->first();    
        $data = array(
        	'Packages'=>$Packages,
        	'action'=>'admin/packages/update_action',
        	'heading'=>"Update",
        	'height'=>'',
    		'width'=>'',
    	);
        return view('admin/packages/form')->with($data); 
    }

    public function update_action(Request $request)
    { 
      $Packages = new Packages;
    	if ($request->hasFile('package_image')) 
		{
			if(file_exists('assets/admin/dist/img/packages'.'/'.$request->old_image))
	        {
			    @unlink('assets/admin/dist/img/packages'.'/'.$request->old_image);
			}
			$package_image = $request->file('package_image');
			$name = str_slug($request->package_image).'.'.$package_image->getClientOriginalExtension();
			$destinationPath = public_path('assets/admin/dist/img/packages');
			$package_imagePath = $destinationPath. "/".  $name;
			$package_image->move($destinationPath, $name);
			$image = $name;
			
		}
		else
		{
			$image = $request->old_image;
		}
		$Packages 							= $Packages->find($request->id);
		$Packages->package_title 			=ucwords($request->package_title);
		$Packages->package_description 		=$request->package_description;
		$Packages->package_amount			=$request->package_amount;
		$Packages->package_duration			=$request->package_duration;
		$Packages->package_image			=$image;
		$Packages->status 					='Active';
		$Packages->updated_at 				=date('Y-m-d H:i:s');
		$Packages->save(); 
		$data = array(
				'session'=>"Updated Successfully",
				'alert-class'=>'alert-success'
		);
		return Redirect::to('admin/packages')->with($data);    
    }

    public function delete_action(Request $request)
    { 
        $Packages = new Packages;       
        $Packages = $Packages->find($request->id);
        
        if(file_exists('assets/admin/dist/img/packages'.'/'.$Packages->package_image))
        {
		    @unlink('assets/admin/dist/img/packages'.'/'.$Packages->package_image);
		}
		$Packages->where('id',$request->id)->delete();
        $data= array(
        	'session'=>"Deleted Successfully",
        	'alert-class'=>'alert-success',
        );
        return Redirect::to('admin/packages')->with($data);   ;
    }

    public function change_action(Request $request)
    { 
        $Packages = new Packages;
        $Packages = $Packages->find($request->id);
        if($Packages->status=='Active')
        {
            $Packages->status='Inactive';
        }
        else
        {
            $Packages->status='Active';
        }
        $Packages->updated_at=date('Y-m-d H:i:s');
        $Packages->save(); 
        $data= array(
        	'session'=>"Change Status Successfully",
        	'alert-class'=>'alert-success',
        );
        return Redirect::to('admin/packages')->with($data);  
    }
}
