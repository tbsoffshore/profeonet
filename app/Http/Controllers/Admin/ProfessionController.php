<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Profession;
use DB;
use Session;

class ProfessionController extends Controller
{
    //
    public function index()
    {
        return view('admin.profession.list')->with(['deleteAction'=>'profession/delete_action','statusAction'=>'profession/change_action',]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin/profession/form')->with(['heading'=>"Add",'action'=>'profession/create_action']); 
    }

    public function create_action(Request $request)
    { 

         $this->validate($request,[
                'title' => 'required|unique:mst_professions',
            ],
            [
                'title.required'    => 'Please enter title',
            ]
            );

        $Profession = new Profession;
        $Profession->title=$request->title;
        $Profession->status='Active';
        $Profession->created_at=date('Y-m-d H:i:s');
        $Profession->updated_at=date('Y-m-d H:i:s');
        $Profession->save(); 
        
        return redirect()->route('profession/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']); 
       
    }
    
    
     /*check duplication for Create & Update..*/
    public function check_duplication(Request $request)
    {
        $id = $request->post('id');
        //print_r($id);exit;
        $response = array();
        $Profession = new Profession;
        if($id=='')
        {
            $Profession_data = $Profession->where('title', $request->post('title'))->get();
        }
        else
        {
            $Profession_data = $Profession->where('title', $request->post('title'))->where('id','<>',$id)->get();
            
        }
       // print_r($Profession_data);exit();
        
        if(count($Profession_data)> 0)
        {
            $response['error'] = 1;
            $response['message'] = "Already exist";
        }
        else
        {
            Session::put('message','Record created successfully');
            $response['error'] = 0;
            $response['message'] = 'success';
        }
        echo json_encode($response);exit; 
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //update action
    public function update($id)
    {
        $profession = new Profession;
        $profession_data = $profession->where('id', $id)->first();  
        // dd($profession_data);     
        return view('admin/profession/form')->with(['profession_data'=>$profession_data,'action'=>'profession/update_action','heading'=>"Update"]); 
    }

    public function update_action(Request $request)
    { 

        $this->validate($request,[
          'title' => 'required|unique:mst_professions,title,'.$request->id,
        ],
        [
                'title.required'    => 'Please enter title',
          ]
        );
        $profession = new Profession;
        $profession = $profession->find($request->id);
        $profession->title=$request->title;
        $profession->updated_at=date('Y-m-d H:i:s');
        $profession->save(); 
        return redirect()->route('profession/list')->with(['session'=>"Updated Successfully",'alert-class'=>'alert-success']);
       
    }

    public function delete_action(Request $request)
    { 
        $profession = new Profession;       
        $profession->where('id', $request->id)->delete(); 
        return redirect()->route('profession/list')->with(['session'=>"Deleted Successfully",'alert-class'=>'alert-success']);
    }

    public function change_action(Request $request)
    { 
        $profession = new Profession;
        $profession = $profession->find($request->id);
        if($profession->status=='Active')
        {
             $profession->status='Inactive';
        }
        else
        {
             $profession->status='Active';
        }
        $profession->updated_at=date('Y-m-d H:i:s');
        $profession->save(); 
        return redirect()->route('profession/list')->with(['session'=>"Status has been updated successfully",'alert-class'=>'alert-success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
