<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\ProfileBenefits;
use DB;
use Redirect;
use Input;

class ProfileBenefitsController extends Controller
{
   public function index()
   {
   		$data = array(
    		'deleteAction'=>'admin/profilebenefits/delete_action',
    		'statusAction'=>'admin/profilebenefits/change_action',
    		'create'=>'admin/profilebenefits/create',
    		'heading'=>'Profile Benefits List'
    	);
    	return view('admin/profilebenefits/list')->with($data);
   }
   public function create()
    {
    	 $data=  array(
    		'heading'=>'Add ',
    		'action'=>'admin/profilebenefits/create_action',
    		'height'=>'',
    		'width'=>'',
    	);
    	 return view('admin/profilebenefits/form')->with($data);
    }
    
    public function create_action(Request $request)
    {
    	for ($i=0; $i < count($request->benefits); $i++)
    	{ 
    		$ProfileBenefits 	= new ProfileBenefits;

			$ProfileBenefits->benefits 			=$request->benefits[$i];
			$ProfileBenefits->status 			='Active';
			$ProfileBenefits->created_at 		=date('Y-m-d H:i:s');
			$ProfileBenefits->updated_at 		=date('Y-m-d H:i:s');	
			$ProfileBenefits->save(); 
    	}
    	// echo "<pre>";print_r($request->benefits);echo "<pre>";exit();
		
		$data = array(
				'session'=>"Created Successfully",
				'alert-class'=>'alert-success'
		);
		return Redirect::to('admin/profilebenefits')->with($data);		
    }

     public function update($id)
    {
        $ProfileBenefits = new ProfileBenefits;
        $ProfileBenefits = $ProfileBenefits->where('id', $id)->first();    
        $data = array(
        	'ProfileBenefits'=>$ProfileBenefits,
        	'action'=>'admin/profilebenefits/update_action',
        	'heading'=>"Update",        	
    	);
        return view('admin/profilebenefits/form')->with($data); 
    }

    public function update_action(Request $request)
    { 

      	$ProfileBenefits = new ProfileBenefits;    
		$ProfileBenefits 							= 	$ProfileBenefits->find($request->id);	
		$ProfileBenefits->benefits					=	$request->benefits[0];
		$ProfileBenefits->status 					=	'Active';
		$ProfileBenefits->updated_at 				=	date('Y-m-d H:i:s');
		$ProfileBenefits->save(); 
    	//print_r($_POST);exit();
		$data = array(
				'session'=>"Updated Successfully",
				'alert-class'=>'alert-success'
		);
		return Redirect::to('admin/profilebenefits')->with($data);    
    }

    public function delete_action(Request $request)
    { 
        $ProfileBenefits = new ProfileBenefits;       
        $ProfileBenefits = $ProfileBenefits->find($request->id);
		$ProfileBenefits->where('id',$request->id)->delete();
        $data= array(
        	'session'=>"Deleted Successfully",
        	'alert-class'=>'alert-success',
        );
        return Redirect::to('admin/profilebenefits')->with($data);   ;
    }

    public function change_action(Request $request)
    { 
        $ProfileBenefits = new ProfileBenefits;
        $ProfileBenefits = $ProfileBenefits->find($request->id);
        if($ProfileBenefits->status=='Active')
        {
            $ProfileBenefits->status='Inactive';
        }
        else
        {
            $ProfileBenefits->status='Active';
        }
        $ProfileBenefits->updated_at=date('Y-m-d H:i:s');
        $ProfileBenefits->save(); 
        $data= array(
        	'session'=>"Change Status Successfully",
        	'alert-class'=>'alert-success',
        );
        return Redirect::to('admin/profilebenefits')->with($data);  
    }
}
