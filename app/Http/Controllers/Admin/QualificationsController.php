<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Qualifications;
use DB;

class QualificationsController extends Controller
{
    public function index()
    {
        return view('admin.qualifications.list')->with(['deleteAction'=>'qualifications/delete_action','statusAction'=>'qualifications/change_action',]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin/qualifications/form')->with(['heading'=>"Add",'action'=>'qualifications/create_action']);
    }

    public function create_action(Request $request)
    { 
        $qualification = new Qualifications;

        $qualification->title=$request->title;
        $qualification->status='Active';
        $qualification->created_at=date('Y-m-d H:i:s');
        $qualification->updated_at=date('Y-m-d H:i:s');
        $qualification->save(); 
        return redirect()->route('qualifications/list');  
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   //update action
    public function update($id)
    {
        //
        $qualifications = new Qualifications;
        $qualification_data = $qualifications->where('id', $id)->first();       
        return view('admin/qualifications/form')->with(['qualification_data'=>$qualification_data,'action'=>'qualifications/update_action','heading'=>"Update"]); 
    }

    public function update_action(Request $request)
    { 
        $qualifications = new Qualifications;
        $qualifications = $qualifications->find($request->id);
        $qualifications->title=$request->title;
        $qualifications->status='Active';
        $qualifications->updated_at=date('Y-m-d H:i:s');
        $qualifications->save(); 
        return redirect()->route('qualifications/list');  
       
    }

    public function delete_action(Request $request)
    { 
        $qualifications = new Qualifications;       
        $qualifications->where('id', $request->id)->delete(); 
        return redirect()->route('qualifications/list');
    }

    public function change_action(Request $request)
    { 
        $qualifications = new Qualifications;
        $qualifications = $qualifications->find($request->id);
        if($qualifications->status=='Active')
        {
             $qualifications->status='Inactive';
        }
        else
        {
             $qualifications->status='Active';
        }
        $qualifications->updated_at=date('Y-m-d H:i:s');
        $qualifications->save(); 
        return redirect()->route('qualifications/list');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
