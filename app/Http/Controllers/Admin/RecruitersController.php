<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Members;
use App\Model\Admin\Jobs;
use App\Model\Admin\Experiences;
use App\Model\Admin\Countries;
use App\Model\Admin\States;
use App\Model\Admin\Districts;
use DB;

class RecruitersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array(
            'viewAction'=>'recruiters/view_action',
           
        );
        return view('admin.recruiters.list')->with($data); 
    }

    public function candidate_index()
    {
        return view('admin.candidates.list')->with(['viewAction'=>'candidates/view_action',  'statusAction'=>'candidates/change_action',
            'deleteAction'=>'candidates/delete_action', ]); 
    }
    
    public function show($id)
    {
        $recruiters = new Members;
        $recruiter_data = $recruiters->where('fld_member_id', $id)->first();    
        return view('admin/recruiters/view')->with(['recruiter_data'=>$recruiter_data,'action'=>'recruiters/list']); 
    }

     public function recruitersedit($id)
    {
            $fld_member_id = $id;
            $Member = new Members;
            $experience = new Experiences;
            $experience_list = $experience->get();
            $get_data = $Member->select('*')->where('fld_member_id', $fld_member_id)->first();
            $country = new Countries;
            $country_list = $country->get();
            //dd($experience_list);

            $data = array(
                'heading' => "Update", 
                'Member'    =>$get_data,            
                'country_list'  =>$country_list,            
                'experience'    =>$experience_list,   
                'action' => 'recruiters/editaction',      
            );

        return view('admin/recruiters/form')->with($data); 
    }

     public function recruiterseditaction(Request $request)
    {
           //print_r($_POST);exit;
            $fld_member_id = $request->post('fld_member_id');
            $Member = new Members;
            if ($request->hasFile('can_profile_image')) 
            {
                $image = $request->file('can_profile_image');
                $name = str_slug($request->can_profile_image).'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('assets/front-end/img/resume');
                $imagePath = $destinationPath. "/".  $name;
                $image->move($destinationPath, $name);
                $can_profile_image = $name;
                if(file_exists('public/assets/front-end/img/resume'.'/'.$request->old_can_profile))
                {
                    @unlink('public/assets/front-end/img/resume'.'/'.$request->old_can_profile);
                }
            }
            else
            {
                $can_profile_image = $request->old_can_profile;
            }


            if ($request->hasFile('re_company_logo')) 
            {
                $image = $request->file('re_company_logo');
                $name = str_slug($request->re_company_logo).'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('assets/front-end/company_logo');
                $imagePath = $destinationPath. "/".  $name;
                $image->move($destinationPath, $name);
                $re_company_logo = $name;
                if(file_exists('public/assets/front-end/company_logo'.'/'.$request->old_logo))
                {
                    @unlink('public/assets/front-end/company_logo'.'/'.$request->old_logo);
                }
            }
            else
            {
                $re_company_logo = $request->old_logo;
            }
            
            $data = array(
                'fld_dob'=> $request->can_date_of_birth,
                'fld_qulification'=>$request->can_qualification,
                'fld_total_experience'=>$request->fld_total_experience,
                'fld_mobile_no'=>$request->can_mobile,
                'fld_mobile_no_2'=>$request->can_mobile2,
                'fld_email_id'=>$request->can_email,
                'fld_email_id_2'=>$request->can_email2,
                'fld_personal_webside'=>$request->fld_personal_webside,

                'fld_country'=>$request->fld_country_id_0,
                'fld_state'=>$request->fld_state_id_0,
                'fld_district'=>$request->fld_district_id_0,

                'fld_company_name'=>$request->re_company_name,
                'fld_company_description'=>$request->re_description,
                'fld_company_tagline'=>$request->re_company_tag,
                'fld_designation'=>$request->re_designation,
                'fld_company_website'=>$request->re_company_website,
                'fld_company_address'=>$request->re_fld_company_address,
                'fld_hiring_for'=>$request->re_fld_hiring_for,

                'fld_memberimage'=>$can_profile_image,
                'fld_company_logo'=>$re_company_logo,
                'fld_profile_is_update'=>'Yes',
                'updated_at'=>date('Y-m-d H:i:s'),
            );
            $query = $Member->where('fld_member_id', $fld_member_id)->update($data);

            $data1 = array(
                'session'=>"Updated Successfully",
                'alert-class'=>'alert-success'
            );
            return redirect()->route('recruiters/list')->with($data1);     

    }


    public function candidate_show($id)
    {
        $Jobs = new Jobs;

        //dd($Jobs->get());
        $candidates = new Members;
        $recruiter_data = $candidates->where('fld_member_id', $id)->first();     
        $data = array(
            'Jobs'=>$Jobs,
            'recruiter_data'=>$recruiter_data,
            'action'=>'candidates/list'
        );
       // echo "<pre>";print_r($recruiter_data); echo "<pre>";exit();   
        return view('admin/candidates/view')->with($data); 
    }

   
    public function edit($id)
    {
            $fld_member_id = $id;
            $Member = new Members;
            $experience = new Experiences;
            $experience_list = $experience->get();
            $get_data = $Member->select('*')->where('fld_member_id', $fld_member_id)->first();
            $country = new Countries;
            $country_list = $country->get();
            //dd($experience_list);

            $data = array(
                'heading' => "Update", 
                'Member'    =>$get_data,            
                'country_list'  =>$country_list,            
                'experience'    =>$experience_list,   
                'action' => 'candidates/editaction',      
            );

        return view('admin/candidates/form')->with($data); 
    }

    public function editaction(Request $request)
    {
           
            $fld_member_id = $request->post('fld_member_id');
            $Member = new Members;
            if ($request->hasFile('can_profile_image')) 
            {
                $image = $request->file('can_profile_image');
                $name = str_slug($request->can_profile_image).'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('assets/front-end/img/resume');
                $imagePath = $destinationPath. "/".  $name;
                $image->move($destinationPath, $name);
                $can_profile_image = $name;
                if(file_exists('public/assets/front-end/img/resume'.'/'.$request->old_can_profile))
                {
                    @unlink('public/assets/front-end/img/resume'.'/'.$request->old_can_profile);
                }
            }
            else
            {
                $can_profile_image = $request->old_can_profile;
            }
            
            $data = array(
                'fld_dob'=> $request->can_date_of_birth,
                'fld_qulification'=>$request->can_qualification,
                'fld_total_experience'=>$request->fld_total_experience,
                'fld_mobile_no'=>$request->can_mobile,
                'fld_mobile_no_2'=>$request->can_mobile2,
                'fld_email_id'=>$request->can_email,
                'fld_email_id_2'=>$request->can_email2,
                'fld_personal_webside'=>$request->fld_personal_webside,

                'fld_country'=>$request->fld_country_id_0,
                'fld_state'=>$request->fld_state_id_0,
                'fld_district'=>$request->fld_district_id_0,

                'fld_country_1'=>$request->fld_country_id_1,
                'fld_state_1'=>$request->fld_state_id_1,
                'fld_district_1'=>$request->fld_district_id_1,

                'fld_country_2'=>$request->fld_country_id_2,
                'fld_state_2'=>$request->fld_state_id_2,
                'fld_district_2'=>$request->fld_district_id_2,

                'fld_country_3'=>$request->fld_country_id_3,
                'fld_state_3'=>$request->fld_state_id_3,
                'fld_district_3'=>$request->fld_district_id_3,

                'fld_memberimage'=>$can_profile_image,
                'fld_profile_is_update'=>'Yes',
                'updated_at'=>date('Y-m-d H:i:s'),
            );
            $query = $Member->where('fld_member_id', $fld_member_id)->update($data);

            $data1 = array(
                'session'=>"Updated Successfully",
                'alert-class'=>'alert-success'
            );
            return redirect()->route('candidates/list')->with($data1);     

    }


     public function delete_action(Request $request)
    { 
        $fld_member_id = $request->fld_member_id;
        $Member = new Members;
         $datau = array(
            'is_deleted'=> 1,
            'updated_at'=>date('Y-m-d H:i:s'),
            );
        $Member->where('fld_member_id', $fld_member_id)->update($datau); 
        $data= array(
            'session'=>"Deleted Successfully",
            'alert-class'=>'alert-success',
        );
        return redirect()->route('candidates/list')->with($data);
    }

    public function change_action(Request $request)
    { 
        $fld_member_id = $request->fld_member_id;
        $Member = new Members;
        $recruiter_data = $Member->where('fld_member_id', $fld_member_id)->first();     
        if($recruiter_data->status=='1')
        {
             $status='0';
        }
        else
        {
             $status='1';
        }

        $datau = array('status'=> $status,
                'updated_at'=>date('Y-m-d H:i:s'),);

         $Member->where('fld_member_id', $fld_member_id)->update($datau); 
         $data= array(
            'session'=>"Change Status Successfully",
            'alert-class'=>'alert-success',
        );
        return redirect()->route('candidates/list')->with($data);
    }
   



    
}
