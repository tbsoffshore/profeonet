<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Scategories;
use DB;
use Session;

class ScategoriesController extends Controller
{
    //
    public function index()
    {
        
        return view('admin.scategories.list')->with(['deleteAction'=>'scategories/delete_action','statusAction'=>'scategories/change_action',]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
     return view('admin/scategories/form')->with(['heading'=>"Add",'action'=>'scategories/create_action']);
       
    }
    public function create_action(Request $request)
    { 

        $this->validate($request,[
                'title' => 'required|unique:mst_schollar_categories',
            ],
            [
                'title.required'    => 'Please enter title',
            ]
            );

            $scategories = new Scategories;
            $scategories->title=$request->title;
            $scategories->status='Active';
            $scategories->created_at=date('Y-m-d H:i:s');
            $scategories->updated_at=date('Y-m-d H:i:s');
            $scategories->save(); 
          return redirect()->route('scategories/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']);
    }

     /*check duplication for Create & Update..*/
    public function check_duplication(Request $request)
    {
        $id = $request->post('id');
        $response = array();
        $scategories = new Scategories;
        if($id=='')
        {
            $scategories_data = $scategories->where('title', $request->post('title'))->get();
        }
        else
        {
            $scategories_data = $scategories->where('title', $request->post('title'))->where('id','<>',$id)->get();
            
        }
        
        if(count($scategories_data)> 0)
        {
            $response['error'] = 1;
            $response['message'] = "Already exist";
        }
        else
        {
            Session::put('message','Record created successfully');
            $response['error'] = 0;
            $response['message'] = 'success';
        }
        echo json_encode($response);exit; 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //update action
    public function update($id)
    {

        $scategories = new Scategories;
        $scategories_data = $scategories->where('id', $id)->first();  
        // dd($scategories_data);     
        return view('admin/scategories/form')->with(['scategories_data'=>$scategories_data,'action'=>'scategories/update_action','heading'=>"Update"]); 
    }

    public function update_action(Request $request)
    { 

        $this->validate($request,[

        'title' => 'required|unique:mst_schollar_categories,title,'.$request->id,
        ],
        [
                'title.required'    => 'Please enter title',
        ]
        );


        $scategories = new Scategories;
        $scategories = $scategories->find($request->id);
        $scategories->title=$request->title;
        $scategories->updated_at=date('Y-m-d H:i:s');
        $scategories->save(); 
        return redirect()->route('scategories/list')->with(['session'=>"Updated Successfully",'alert-class'=>'alert-success']);  
       
    }

    public function delete_action(Request $request)
    { 
        $scategories = new Scategories;       
        $scategories->where('id', $request->id)->delete(); 
        return redirect()->route('scategories/list')->with(['session'=>"Delete Successfully",'alert-class'=>'alert-success']);
    }

    public function change_action(Request $request)
    { 
        $scategories = new Scategories;
        $scategories = $scategories->find($request->id);
        if($scategories->status=='Active')
        {
             $scategories->status='Inactive';
        }
        else
        {
             $scategories->status='Active';
        }
        $scategories->updated_at=date('Y-m-d H:i:s');
        $scategories->save(); 
        return redirect()->route('scategories/list')->with(['session'=>"Status has been updated successfully",'alert-class'=>'alert-success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
