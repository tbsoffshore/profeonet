<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Settings;
use DB;

class SettingsController extends Controller
{
    public function create()
    {
    	$id='1';
    	$Settings = new Settings;
        $Settings = $Settings->where('id', $id)->first(); 

       // dd($Settings);
       
    	$data=  array(
    		'Settings'	=>	$Settings,
    		'action'	=>	'settings/update_action',
    		'heading'	=>	'Update Web Settings',
    	);
        return view('admin/settings/form')->with($data); 
    }
     public function update_action(Request $request)
    { 


     
      $Settings = new Settings;
      if ($request->hasFile('logo')) 
	  {
        $logo = $request->file('logo');
        $name = str_slug($request->logo).'.'.$logo->getClientOriginalExtension();
        $destinationPath = public_path('assets/admin/dist/img/logo');
        $logoPath = $destinationPath. "/".  $name;
        $logo->move($destinationPath, $name);
        $Settings->logo = $name;
        
      }
      else{

      	$Settings->logo = $request->old_logo;

      }
      	$Settings= $Settings->find($request->id);
      	$Settings->site_title= $request->site_title;
      	$Settings->mobile1= $request->mobile1;
      	$Settings->mobile2= $request->mobile2;
      	$Settings->email= $request->email;
      	$Settings->address= $request->address;
      	$Settings->notification= $request->notification;
      	$Settings->copy_right= $request->copy_right;
      	$Settings->facebook_link= $request->facebook_link;
      	$Settings->twitter_link= $request->twitter_link;
      	$Settings->insta_link= $request->insta_link;
      	$Settings->logo= $Settings->logo;
        $Settings->created_at= date('Y-m-d H:i:s');
        $Settings->updated_at= date('Y-m-d H:i:s');
        $Settings->save(); 

        
        /*$data = array(
        	'session'=>"Updated Successfully",
        	'alert-class'=>'alert-success'
        );*/
		return redirect()->route('settings/create')->with(['session'=>"Updated Successfully",'alert-class'=>'alert-success']);
        
       
    }
}
