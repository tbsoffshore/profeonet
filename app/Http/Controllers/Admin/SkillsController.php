<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Skills;
use DB;

class SkillsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.skills.list')->with(['statusAction'=>'skills/change_action','deleteAction'=>'skills/delete_action']); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/skills/form')->with(['action'=>'skills/create_action']); 
    }

    public function create_action(Request $request)
    { 
        $skill = new Skills;
        $skill->title=$request->title;
        $skill->status='Active';
        $skill->created_at=date('Y-m-d H:i:s');
        $skill->updated_at=date('Y-m-d H:i:s');
        $skill->save(); 
        return redirect()->route('skills/list');  
       
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //update action
    public function update($id)
    {
        //
        $skills = new Skills;
        $skill_data = $skills->where('id', $id)->first();       
        return view('admin/skills/form')->with(['skill_data'=>$skill_data,'action'=>'skills/update_action']); 
    }

    public function update_action(Request $request)
    { 
        $skill = new Skills;
        $skill = $skill->find($request->id);
        $skill->title=$request->title;
        $skill->status='Active';
        $skill->updated_at=date('Y-m-d H:i:s');
        $skill->save(); 
        return redirect()->route('skills/list');  
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete_action(Request $request)
    { 
        $skill = new Skills;       
        $skill->where('id', $request->id)->delete(); 
        return redirect()->route('skills/list');
    }

    public function change_action(Request $request)
    { 
        $skill = new Skills;
        $skill = $skill->find($request->id);
        if($skill->status=='Active')
        {
             $skill->status='Inactive';
        }
        else
        {
             $skill->status='Active';
        }
        $skill->updated_at=date('Y-m-d H:i:s');
        $skill->save(); 
        return redirect()->route('skills/list');
    }
}
