<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\States;
use App\Model\Admin\Countries;
use DB;

class StatesController extends Controller
{
    //
    public function index()
    {
        return view('admin.states.list')->with(['deleteAction'=>'states/delete_action','statusAction'=>'states/change_action',]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $country = new Countries;
        $country_list = $country->get();
        $state_data = '';
        return view('admin/states/form')->with(['country_list'=>$country_list,'heading'=>"Add",'action'=>'states/create_action']); 
    }

    public function create_action(Request $request)
    { 

         $validator = $this->validate($request,[
                'country_id' => 'required',
                'state_name' => 'required|unique:states',
            ],
            [
                'country_id.required'    => 'Please select country',
                'state_name.required'    => 'Please enter state name',
            ]
            );

          
       // var_dump($request->post());exit();
        $states = new States;
                $states->country_id=$request->country_id;
                $states->state_name=$request->state_name;
                $states->status='Active';
                $states->created_at=date('Y-m-d H:i:s');
                $states->updated_at=date('Y-m-d H:i:s');
                $states->save(); 
                return redirect()->route('states/list')->with(['session'=>"Created Successfully",'alert-class'=>'alert-success']); 
            
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
       // print_r($id);exit();
        //
        $country = new Countries;
        $country_list = $country->get();

        $states = new States;
        $state_data = $states->where('id', $id)->first();

        return view('admin/states/form')->with(['country_list'=>$country_list,'state_data'=>$state_data,'action'=>'states/update_action','heading'=>"Update"]); 
    }
    public function update_action(Request $request)
    { 

        $validator =$this->validate($request,[
            'country_id' => 'required',
          'state_name' => 'required|unique:states,state_name,'.$request->id,'country_id,'.$request->country_id,
        ],
        [
                'country_id.required'    => 'Please select country',
                'state_name.required'    => 'Please enter state',
          ]
        );


         
        //$states = new States;
        $data= array(
            'country_id' =>$request->country_id,
            'state_name' =>$request->state_name,
            'updated_at' =>date('Y-m-d H:i:s'),
        );
        
        States::where('id',$request->id)->update($data);
        return redirect()->route('states/list')->with(['session'=>"Update Successfully",'alert-class'=>'alert-success']); 
    
    }
    public function delete_action(Request $request)
    { 
        $states = new States;       
        $states->where('id', $request->id)->delete(); 
        return redirect()->route('states/list')->with(['session'=>"Deleted Successfully",'alert-class'=>'alert-success']);   ;
    }

    public function change_action(Request $request)
    { 
        $states = new States;
        $states = $states->find($request->id);
        if($states->status=='Active')
        {
            $states->status='Inactive';
        }
        else
        {
            $states->status='Active';
        }
        $states->updated_at=date('Y-m-d H:i:s');
        $states->save(); 
        return redirect()->route('states/list')->with(['session'=>"Change Status Successfully",'alert-class'=>'alert-success']);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
