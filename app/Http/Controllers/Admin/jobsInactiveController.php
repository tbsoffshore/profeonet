<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\jobsInactive;
use DB;

class jobsInactiveController extends Controller
{
    public function index()
    {
    	$data= array(
    		'statusAction'=>'skills/change_action',
    		'deleteAction'=>'skills/delete_action'
    	);
        return view('admin.jobsinactive.list')->with($data); 
    }
    public function approveJobs(Request $request)
    {
	    $explode = explode(",", $request->post('selected_jobs'));
    	$count = count($explode);
    	if($count>0)
    	{
    		for ($i=0; $i < $count; $i++)
    		{ 
	    		$jobsInactive 	= new jobsInactive;	    	
				$data = array(
					'fld_status' => 'Active',
					'updated_at' => date('Y-m-d H:i:s'),
				);
				$jobsInactive->where('fld_id',$explode[$i])->update($data);
				$data = array(
				'session'=>"Jobs Approved Successfully",
				'alert-class'=>'alert-success'
				);
				return redirect()->route('jobsinactive/list')->with($data); 
			}
    	}
    	
    }
}
