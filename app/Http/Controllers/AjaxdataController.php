<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;

use App\Model\Admin\Skills;

use App\Model\Admin\Qualifications;

use App\Model\Admin\Experiences;

use App\Model\Admin\Countries;

use App\Model\Admin\States;

use App\Model\Admin\Districts;

use App\Model\Admin\Employment_type;

use App\Model\Admin\Members;

use App\Model\Admin\OurPatners;

use App\Model\Admin\Jobs;

use App\Model\Admin\Profession;

use App\Model\Admin\Domain;

use App\Model\Admin\Discipline;

use App\Model\Admin\Colleges;

use App\Model\Admin\Baner;

use App\Model\Admin\jobsInactive;

use App\Model\Admin\Cms_pages;

use App\Model\Admin\Packages;

use App\Model\Admin\ProfileBenefits;

use App\Model\Admin\Areas;

use App\Model\Admin\Connections;

use App\Model\Admin\Cgpa;

use App\Model\Admin\Scategories;

use DataTables;

use DB;



class AjaxdataController extends Controller

{

	function ajaxSkillList()

    {

     	$skills = Skills::select('*');

         

     	return DataTables::of($skills)

        

            ->addColumn('action', function ($skills) {

                return '<a href="'.route("skills/update", $skills->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$skills->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

            })->make(true);

    }



    function ajaxQualificationList()

    {

     	$qualifications = Qualifications::select('*');

     	return DataTables::of($qualifications)

            ->addColumn('action', function ($qualifications) {

                return '<a href="'.route("qualifications/update", $qualifications->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$qualifications->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

            })->make(true);

    }



    function ajaxExperienceList()

    {

        $experiences = Experiences::select('*');

        return DataTables::of($experiences)

        ->addColumn('action', function ($experiences) {

                return '<a href="'.route("experiences/update", $experiences->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$experiences->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

        })->make(true);

    }



    function ajaxCountryList()

    {

        $countries = Countries::select('*');

        return DataTables::of($countries)

        ->addColumn('action', function ($countries) {

                return '<a href="'.route("countries/update", $countries->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$countries->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

        })->make(true);

    }



    function ajaxStatesList()

    { 

        $states = new States; 

        $state_list = $states->select('id','state_name','status','country_id')->with('country')->get();

        return DataTables::of($state_list)

        ->addColumn('action', function ($states) {

                return '<a href="'.url("admin/states/update/".$states->id."").'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$states->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

        })->make(true);

    }



    function ajaxDistrictList()

    {

        

        $districts = new Districts;

        //print_r($districts); 

        $district_list = $districts->select('id','district_name','status','state_id','country_id')->with('country')->with('state')->get();      

        return DataTables::of($district_list)->addColumn('action', function ($districts) {

                return '<a href="'.url("admin/districts/update/".$districts->id."").'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$districts->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

        })->make(true);

    }



    function ajaxEmployment_typeList()

    {

        $employment_types = employment_type::select('*');

        return DataTables::of($employment_types)

        ->addColumn('action', function ($employment_types) {

                return '<a href="'.route("employment_types/update", $employment_types->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$employment_types->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

        })->make(true);

    }



    function ajaxRecruiterList()

    {

        $members = Members::select('*')->where('fld_Iam', 'Recruiter');         

        return DataTables::of($members)

        

            ->addColumn('action', function ($members) {

                return '<a href="'.route("recruiters/view", $members->fld_member_id).'" class="btn btn-primary btn-circle btn-xs" title="view"><i class="fa fa-search"></i></a>';

            })->make(true);

    }



    function ajaxCandidatesList()

    {

        $members = Members::select('*')->where('fld_Iam', 'Candidate');         

        return DataTables::of($members)

        

            ->addColumn('action', function ($members) {

                return '<a href="'.route("candidates/view", $members->fld_member_id).'" class="btn btn-primary btn-circle btn-xs" title="view"><i class="fa fa-search"></i></a>';

            })->make(true);

    }

      function ajaxOurPatnersList()

    {

        $OurPatners = OurPatners::select('*');

        return DataTables::of($OurPatners)

        ->addColumn('action', function ($OurPatners) {

                return '<a href="'.route("ourpatners/update", $OurPatners->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$OurPatners->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

        })->make(true);

    }



    function ajaxJobsList()

    {

        $jobs = Jobs::select('*');         

        return DataTables::of($jobs)

            ->addColumn('action', function ($jobs) {

                return '<a href="'.route("jobs/view", $jobs->fld_id).'" class="btn btn-primary btn-circle btn-xs" title="view"><i class="fa fa-search"></i></a>';

            })->make(true);

    }



    //CMS Pages

    function ajaxCms_pagesList()

    {

        $cms_pages = Cms_pages::select('*');

         

        return DataTables::of($cms_pages)

        

            ->addColumn('action', function ($cms_pages) {

                return '<a href="'.route("cms_pages/update", $cms_pages->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp<a href="'.route("cms_pages/show", $cms_pages->id).'" class="btn btn-warning btn-circle btn-xs"><i class="fa fa-search"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$cms_pages->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

            })->make(true);

    }

    

    //Packages

    function ajaxPackagesList()

    {

        $Packages = Packages::select('*');

         

        return DataTables::of($Packages)

        

            ->addColumn('action', function ($Packages) {

                return '<a href="'.url("admin/packages/update", $Packages->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$Packages->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

            })->make(true);

    }

    //ProfileBenefits

    function ajaxProfileBenefitsList()

    {

        $ProfileBenefits = ProfileBenefits::select('*');

         

        return DataTables::of($ProfileBenefits)

        

            ->addColumn('action', function ($ProfileBenefits) {

                return '<a href="'.url("admin/profilebenefits/update", $ProfileBenefits->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$ProfileBenefits->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

            })->make(true);

    }



    //inactive jobs

     function ajaxJobsInactiveList()

    {

        $jobsInactive = jobsInactive::select('*')->where('fld_status','Inactive');         

        return DataTables::of($jobsInactive)

            ->addColumn('action', function ($jobsInactive) {

                return '<a href="'.route("jobsinactive/view", $jobsInactive->fld_id).'" class="btn btn-primary btn-circle btn-xs" title="view"><i class="fa fa-search"></i></a>';

            })->make(true);

    }


  //profession

      function ajaxProfessionList()

    {
        DB::statement(DB::raw('set @rownum=0'));
        $profession = Profession::select([

            DB::raw('@rownum  := @rownum  + 1 AS rownum'),

            'id',

            'title',

            'status',           

        ])->get();
       
        return DataTables::of($profession)
        ->addColumn('action', function ($profession) {

                return '<a href="'.route("profession/update", $profession->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$profession->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

        })->make(true);

    }


    //Domain

    function ajaxDomainList()
    {
       
      DB::statement(DB::raw('set @rownum=0'));
      $domain = Domain::select([

            DB::raw('@rownum  := @rownum  + 1 AS rownum'),

            'id',

            'mst_discipline_id',

            'title',

            'status',           

        ])->with('discipline')->get();

    //  $domain = Domain::select('*');
      
        return DataTables::of($domain)->addColumn('action', function ($domain) {

                return '<a href="'.route("domain/update", $domain->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$domain->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

        })->make(true);

    }



    function ajaxBanerList()
    {
       
      DB::statement(DB::raw('set @rownum=0'));
      $baners = Baner::select([

            DB::raw('@rownum  := @rownum  + 1 AS rownum'),

            'id',

            'title',

            'description',

            'image',

            'status',           

        ])->get();

    //  $baners = Baner::select('*');
      
        return DataTables::of($baners)->addColumn('action', function ($baners) {

                return '<a href="'.route("baners/update", $baners->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$baners->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

        })->make(true);

    }



    function ajaxCollegesList()

    {

        

        $colleges = new Colleges;

     //   print_r($colleges); exit;

        $colleges_list = $colleges->select('id','title','status','mst_domain_id','mst_discipline_id')->with('discipline')->with('domain')->get();      

        return DataTables::of($colleges_list)->addColumn('action', function ($colleges) {

                return '<a href="'.url("admin/colleges/update/".$colleges->id."").'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                    <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$colleges->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>

                ';

        })->make(true);

    }
    
    /*======= Area of work===========*/

    function ajaxAreasList()
    {
        $areas = new Areas;
        //print_r($areas); 
        $areas_list = $areas->select('id','title','status','mst_discipline_id','mst_domain_id')->with('discipline')->with('domain')->get();      
        return DataTables::of($areas_list)->addColumn('action', function ($areas) 
        {
            return '<a href="'.url("admin/areas/update/".$areas->id."").'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$areas->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>';

        })->make(true);

    }

    function ajaxConnectionList()
    {
        $connection = new Connections;
        //print_r($areas); 
        $connection_list = $connection->select('id','states_id','status','mst_discipline_id','mst_domain_id','image','description')->with('discipline')->with('domain')->with('state')->with('areas')->with('colleges')->get();      
        return DataTables::of($connection_list)->addColumn('action', function ($connection) 
        {
            return '<a href="'.url("admin/connections/update/".$connection->id."").'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

                <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$connection->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>';

        })->make(true);

    }

    /*for Cgpa*/

    function ajaxCgpaList()
    {
       
      DB::statement(DB::raw('set @rownum=0'));
      $cgpa = Cgpa::select([

            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'title',
            'status',           
        ])->get();
        return DataTables::of($cgpa)->addColumn('action', function ($cgpa) {
        return '<a href="'.route("cgpa/update", $cgpa->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

              <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$cgpa->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>';
        })->make(true);
    }

    /*for scategories*/
    function ajaxScategoriesList()
    {
       
      DB::statement(DB::raw('set @rownum=0'));
      $scategories = Scategories::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'title',
            'status',           
        ])->get();
        return DataTables::of($scategories)->addColumn('action', function ($scategories) {
        return '<a href="'.route("scategories/update", $scategories->id).'" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;

              <a href="#deleteData" data-toggle="modal" title="Delete" onclick="checkStatus('.$scategories->id.')" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-trash"></i></a>';
        })->make(true);
    }


}


    