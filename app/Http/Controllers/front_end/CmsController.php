<?php 
namespace App\Http\Controllers\front_end;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Profession;
use App\Model\Admin\Countries;
use App\Model\Admin\States;
use App\Model\Admin\Districts;
use App\Model\Admin\Domain;
use App\Model\front_end\WebUsers;
use App\Model\Admin\Cms_pages;

use Input;
use App\Member;

use Hash;
use App\MSG91;

use Session;
use DB;
use Charts;
use Redirect;
use Validator;
use Crypt;

class CmsController extends Controller
{
	public function about_us()
    {  
        $cms_pages = new Cms_pages;
        $cms_pages = $cms_pages->where('page_name', 'about-us')->first(); 
        return view('front-end.pages.about')->with(['cms_data'=>$cms_pages]); 
    }
}