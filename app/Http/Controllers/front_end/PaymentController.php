<?php

namespace App\Http\Controllers\front_end;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Model\Admin\Payments;
Use App\Model\Admin\PaymentLogs;
Use App\Model\Admin\Settings;
Use App\Model\front_end\WebUsers;
use Validator;
use Session;
use Redirect;
use DateTime;

use View;

class PaymentController extends Controller
{
    //https://support.instamojo.com/hc/en-us/articles/208485675-Test-or-Sandbox-Account
    public function __construct()
    {   
    }
   
    public function payment(Request $request)
    {   
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://test.instamojo.com/api/1.1/payment-requests/');
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array("X-Api-Key:test_2831b7a225e47f05b03fd67476e",
                "X-Auth-Token:test_6ef5a35ad73d5f77d46f6b2340a"));
        $payload = Array(
            'purpose' => 'payment',
            'amount' => $request->post('amount'),
            'phone' => $request->post('mobile'),
            'buyer_name' => $request->post('last_name'),
            'redirect_url' => url('payment/returnurl'),
            'send_email' => false,
            'webhook' => 'http://instamojo.com/webhook/',
            'send_sms' => true,
            'email' => 'laracode101@gmail.com',
            'allow_repeated_payments' => false
        );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch); 

        if ($err) {
            Session::put('error','Payment Failed, Try Again!!');
            return redirect()->route('payment/payment_failed');
        } else {
            $data = json_decode($response);
        }


        if($data->success == true) {
            return redirect($data->payment_request->longurl);
        } else {
            Session::put('error','Payment Failed, Try Again!!');
           return redirect()->route('payment/payment_failed');
        }

    }

    public function returnurl(Request $request)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://test.instamojo.com/api/1.1/payments/'.$request->get('payment_id'));
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array("X-Api-Key:test_2831b7a225e47f05b03fd67476e",
                "X-Auth-Token:test_6ef5a35ad73d5f77d46f6b2340a"));

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch); 

        if ($err) {
            \Session::put('error','Payment Failed, Try Again!!');
            return redirect()->route('payment/payment_failed');
        } else {
            $data = json_decode($response);

        }


        $payment_logs = new PaymentLogs;
        $payment_logs->fld_member_id =  Session::get('web_session')->id;
        $payment_logs->payment_data =  $response;
        $payment_logs->payment_date =  date('Y-m-d H:i:s');
        $payment_logs->save();
        
        if($data->success == true) {
            if($data->payment->status == 'Credit') {
              
                 $date = date('Y-m-d H:i:s');

                $sixMonthDate = date('Y-m-d H:i:s', strtotime('+6 month', strtotime($date)));
               //print_r($sixMonthDate);exit();
                $WebUsers = new WebUsers;
                $memberData = [
                        'is_subscription' => 'Yes',
                        'subscription_end_date' =>$sixMonthDate,
                        'subscription_date' =>date('Y-m-d H:i:s'),
                        ];
                $WebUsers->where('id',Session::get('web_session')->id)->update($memberData);

                $payment = new Payments;
                $payment->fld_member_id =  Session::get('web_session')->id;
                $payment->instamojo_id =  $data->payment->payment_id;
                $payment->amount =  $data->payment->amount;
                $payment->instrument_type =  $data->payment->instrument_type;
                $payment->payment_data =  $response;
                $payment->payment_date =  date('Y-m-d H:i:s');
                $payment->subscription_end_date =$sixMonthDate;
                $payment->save();

                Session::put('success','Payment done successfully.');
                 return redirect()->route('payment/payment_success');
            } else {
                Session::put('error','Payment Failed, Try Again!!');
                 return redirect()->route('payment/payment_success');
            }
        } else {
            Session::put('error','Payment Failed, Try Again!!');
            return redirect()->route('payment/payment_failed');
        }
    }

    public function payment_success(Request $request)
    {
        $WebUsers = new WebUsers;
        $session_id = Session::get('web_session')->id;
        $user = $WebUsers->where('id',$session_id)->first(); 
        $data= array('user' => $user,);
        return view('front-end.pages.payment_succes')->with($data);  
    }


    public function payment_failed(Request $request)
    {
        $WebUsers = new WebUsers;
        $session_id = Session::get('web_session')->id;
        $user = $WebUsers->where('id',$session_id)->first(); 
        $data= array('user' => $user,);
        return view('front-end.pages.payment_failed')->with($data);  
    }

}
