<?php 
namespace App\Http\Controllers\front_end;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Profession;
use App\Model\Admin\Countries;
use App\Model\Admin\States;
use App\Model\Admin\Districts;
use App\Model\Admin\Domain;
use App\Model\Admin\Likes;
use App\Model\front_end\WebUsers;
use App\Model\front_end\Postads;
use App\Model\front_end\Tags;

use Input;
use App\Member;

use Hash;
use App\MSG91;

use Session;
use DB;
use Charts;
use Redirect;
use Validator;
use Crypt;

class PostaddController extends Controller
{


    public function savelike(Request $request)
    {
      $LikesModel = new Likes;
      $postid = $request->post('id');
      $web_user_id = $request->post('unique_session_id');
      $checklike = $LikesModel->where('post_id','=',$postid)->where('web_user_id','=',$web_user_id)->first();
      if(!empty($checklike))
      {
          $LikesModel->where('id', $checklike->id)->delete(); 
      }
      else
       {
        $data = array(
        'post_id' => $request->post('id'),
        'web_user_id' => $request->post('unique_session_id'),
        'likes' => 1,       
        'created_at' => now(),
        'updated_at' => now()
        );
        $LikesModel->insert($data);
       }
      $last_id = DB::getPdo()->lastInsertId();
      $like = $LikesModel->where('post_id','=',$postid)->sum('likes');
      
      echo $like;


    }


    public function save_post_add(Request $request)
    {
    	$session_id = Session::get('web_session')->id;
      $post_adds = new Postads;
      $tag_users = $request->post('tag_member');
    
    	if ($request->hasFile('user_image')) 
  		{
  			$image = $request->file('user_image');
  			$name = str_slug($request->user_image).'.'.$image->getClientOriginalExtension();
  			$destinationPath = public_path('assets/front-end/images/post');
  			$imagePath = $destinationPath. "/".  $name;
  			$image->move($destinationPath, $name);
  			$user_image = $name;
  		}

    	$data = array(
       	 'post_by_id' => $session_id,
       	 'where_to_post' => $request->post('connections'),
       	 'comment' => $request->post('addvertise'),
		 
		    'image' =>  $user_image,
		    'location' => $request->post('location'),

		    'created_at' =>  date('Y-m-d H:i:s')
		    );
            
        
        Postads::insert($data);

        $id = DB::getPdo()->lastInsertId();
        $tags = new Tags;
        foreach ($tag_users as $key => $value) {
       	$data = array(
       	 'posts_id' => $id,
       	 'web_users_id' =>  $value,
		     'created_at' =>  date('Y-m-d H:i:s')
		     );
        Tags::insert($data);
       }
      return redirect()->back();
           
    }
}