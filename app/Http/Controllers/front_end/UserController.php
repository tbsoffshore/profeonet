<?php 
namespace App\Http\Controllers\front_end;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Profession;
use App\Model\Admin\Countries;
use App\Model\Admin\States;
use App\Model\Admin\Districts;
use App\Model\Admin\Domain;
use App\Model\Admin\Comments;
use App\Model\front_end\WebUsers;
use App\Model\Admin\Packages;
use App\Model\front_end\Postads;
use Input;
use App\Member;
use Hash;
use App\MSG91;
use Session;
use DB;
use Charts;
use Redirect;
use Validator;
use Crypt;

class UserController extends Controller
{
	public $WebUsers;
	public $Countries;

	public function __construct(WebUsers $WebUsers,Countries $Countries)
    {
        $this->WebUsers = $WebUsers;
        $this->Countries = $Countries;
    }



	public function register(){

		$Profession = new Profession;
		$Profession_data = $Profession->where('status','Active')->get();

		$Countries = new Countries;
		$Countries_data = $Countries->where('status','Active')->get();

		$States = new States;
		$States_data = $States->where('status','Active')->get();

		$Districts = new Districts;
		$Districts_data = $Districts->where('status','Active')->get();

		$Domain = new Domain;
		$Domain_data = $Domain->where('status','Active')->get();

		$data= array(
			'Profession'=>$Profession_data,
			'Countries'=>$Countries_data,
			'States'=>$States_data,
			'Districts'=>$Districts_data,
			'Domain'=>$Domain_data,
			'registerAction' => '/home/userregister',
			);

		return view('front-end.pages.register')->with($data);
	}
    

	public function userregister(Request $request)
	{	
		$WebUsers = new WebUsers;

		if(!empty($request->post('first_name1')))
		{
			$f_name = $request->post('first_name1');
			$l_name = $request->post('last_name1');
			$password = $request->post('password1');
			$show_password = $request->post('show_password1');

			$data = array(
			'profession_id' => $request->post('profession_id'),
			'profession_name' => $request->post('profession_name'),
			'first_name' => $f_name,
			'last_name'	=> $l_name,
			'email'	=> $request->post('email'),
			'mobile'    => $request->post('mobile1'),
			'username'    => $request->post('mobile1'),
			'password'    => Hash::make($password),
			'company_name' => $request->post('company_name'),
			'show_password'   	=> $request->post('password1'),      		
			'domain1_id'   	=> $request->post('domain1_id_'),      		
			'domain2_id'   	=> $request->post('domain2_id_'),      		
			'domain3_id'   	=> $request->post('domain3_id_'),      		
			'company_country_id'   	=> $request->post('company_country_id'),      		
			'company_state_id'   	=> $request->post('company_state_id'),      		
			'company_city_id'   	=> $request->post('company_city_id'),      		
			'website_link'   	=> $request->post('website_link'),      		
			'company_official_email'   	=> $request->post('company_official_emai'),      			 		
			'created_at' => now(),
			'updated_at' => now()
			);
		}
		else
		{
			$user = $WebUsers->select('*')
												->where('mobile',$request->post('mobile'))
												->where('otp_verify',"1")
												->where('isVerified',"0")
												->first();

			if(isset($user)){

				$data = array(
					'profession_id' 	=> $request->post('profession_id'),
					'profession_name' => $request->post('profession_name'),
					'first_name' 			=>  $request->post('first_name'),
					'last_name'				=>  $request->post('last_name'),
					'email'						=> $request->post('email'),
					'mobile'    			=> $request->post('mobile'),
					'username'    		=> $request->post('mobile'),
					'password'    		=> Hash::make($request->post('password')),
					'show_password'   => $request->post('password'),      		      			 		
					'isVerified'   		=> '1',      		      			 		
					'created_at' 			=> date('Y-m-d H:i:s'),
					'updated_at' 			=> date('Y-m-d H:i:s')
				);
				
				$WebUsers->where('id', $user->id)->update($data);
			}
		}


			//otp
			/*$reg_user = $WebUsers->where('id',$id)->first();
			$otp = rand(100000, 999999);
			$MSG91 = new MSG91();
			WebUsers::where('id', $id)->update(['otp' => $otp]);

			$msg91Response = $MSG91->sendSMS($otp,$reg_user['mobile']);
			$msg91Response = $MSG91->sendSMS($otp,$reg_user['mobile']);

			
			if($msg91Response['error']){
				$response['error'] = 1;
				$response['message'] = $msg91Response['message'];
			}
			else{
				$response['error'] = 0;
				$response['mobile'] = $reg_user['mobile'];
				$response['isVerified'] = $reg_user['verify_otp'];
				$response['OTP'] = $otp;
			}*/


		return Redirect::to('home/login/');	
	}
	/*=======function for login =============*/
/*
	public function otpverification($id)
	{
		$WebUsers = new WebUsers;
		$reg_user = $WebUsers->where('id',$id)->first();
		$data= array(
			'reg_user' => $reg_user,
			);
		return view('front-end.pages.otp_verification')->with($data);

	}*/

	public function sendOTP(Request $request){

		$WebUsers = new WebUsers;
		$response = array();
		$duplicate = $WebUsers->select('*')->where('mobile',$request->post('mobile'))->get();

		if(count($duplicate)>0){
			if($duplicate[0]->otp_verify==1 && $duplicate[0]->isVerified==1){
				$response['error'] = 1;
				$response['message'] = "Mobile no. already exists";
			}
			elseif(($duplicate[0]->otp_verify==0 || $duplicate[0]->otp_verify==1) && $duplicate[0]->isVerified==0)
			{
				$update = WebUsers::where('id', $duplicate[0]->id)->update(['otp_verify' => '0','updated_at'=> date('Y-m-d H:i:s')]);
				$otp = rand(100000, 999999);
				$MSG91 = new MSG91();
				WebUsers::where('id', $duplicate[0]->id)->update(['otp' => $otp]);

				$msg91Response = $MSG91->sendSMS($otp,$duplicate[0]['mobile']);

				
				if($msg91Response['error']){
					$response['error'] = 1;
					$response['message'] = $msg91Response['message'];
				}
				else{
					$response['error'] = 0;
					$response['mobile'] = $duplicate[0]['mobile'];
					$response['message'] = "OTP sent successfully";
					$response['OTP'] = $otp;
				}
			}
		}
		else{
			$data = array(
				'mobile'    	=> $request->post('mobile'),
				'username'    => $request->post('mobile'),
				'isVerified'  => '0',
				'created_at' 	=> date('Y-m-d H:i:s'),
				'updated_at' 	=> date('Y-m-d H:i:s')
				);

			$WebUsers->insert($data);
			$id = DB::getPdo()->lastInsertId();

			//otp
			$reg_user = $WebUsers->where('id',$id)->first();
			$otp = rand(100000, 999999);
			$MSG91 = new MSG91();
			WebUsers::where('id', $id)->update(['otp' => $otp]);

			$msg91Response = $MSG91->sendSMS($otp,$reg_user['mobile']);

			
			if($msg91Response['error']){
				$response['error'] = 1;
				$response['message'] = $msg91Response['message'];
			}
			else{
				$response['error'] = 0;
				$response['mobile'] = $reg_user['mobile'];
				$response['message'] = "OTP sent successfully";
				$response['OTP'] = $otp;
			}
		}

		echo json_encode($response);
	}


	public function verifyOTP(Request $request)
	{
		$WebUsers = new WebUsers;
		$response = array();

		$user = $WebUsers->select('*')
													->where('mobile',$request->post('mobile'))
													->where('otp',$request->post('otp'))
													->where('otp_verify',"0")
													->first();


		if($user)
		{
			$update = WebUsers::where('id', $user->id)->update(['otp_verify' => '1','updated_at'=> date('Y-m-d H:i:s')]);
			$response['error'] = 0;
			$response['message'] = "OTP verified!";
		}
		else{
			$response['error'] = 1;
			$response['message'] = "Wrong OTP!";
		}
		echo json_encode($response);
	}

	public function dashboard()
	{

		$WebUsers = new WebUsers;
		$session_id = Session::get('web_session')->id;

		$user = $WebUsers->where('id',$session_id)->first(); 

		$post_by= new Postads;
		$details= $post_by->where('post_by_id',$session_id)->with('web_users')->get();

		$tags=$WebUsers->get();
 
		$data= array(
			'user' => $user,
			'tags' => $tags,
			'details' =>  $details,
			);
		return view('front-end.pages.my_account')->with($data);
	}

	public function checkduplication_reg(Request $request)
	{	
		$WebUsers = new WebUsers;
		$response = array();

		$data = array(
			'profession_id'	 	=> $request->post('profession_id'),
			'profession_name' => $request->post('profession_name'),
	    'first_name'	 		=> $request->post('first_name'),
	    'last_name'	 			=> $request->post('last_name'),
	    'email'	 					=> $request->post('email'),
	    'mobile'	 				=> $request->post('mobile'),
	    'username'    		=> $request->post('mobile'),
			'password'    		=> Hash::make($request->post('password')),
			'show_password'   => $request->post('password'),      		      			 		
			'isVerified'   		=> '1',      		      			 		
			'created_at' 			=> date('Y-m-d H:i:s'),
			'updated_at' 			=> date('Y-m-d H:i:s')
		);

		$check_email_count = $WebUsers->where('email', $request->post('email'))->count();

		if($check_email_count == 1 || $check_email_count > 1)
		{
			$response['error'] = 2;
			$response['message'] = "Email already registered please try Login";
		}
		else
		{
			$user = $WebUsers->where('mobile', $request->post('mobile'))->where('otp', $request->post('r_verifyOTP'))->first();

			$WebUsers->where('id', $user->id)->update($data);

			$response['error'] = 0;
			$response['message'] = "Registered successfully";
		}

		echo json_encode($response);
	}

	public function checkduplication_reg1(Request $request)
	{	
		$WebUsers = new WebUsers;
		$mobile = $request->post('phone');
		$check_mobile_count = $WebUsers->where('mobile',$mobile)->count();
	    if($check_mobile_count == 1 || $check_mobile_count > 1)
		{
			echo 5; exit;
		}
		else
		{
			echo 2; exit;
		}
	}

	public function getprofession_name(Request $request)
	{	
		$response = array();

		$profession_id = $request->post('domain');
		$Profession = new Profession;
		$Profession_data = $Profession->where('status','Active')->where('id',$profession_id)->first();

		$response['title'] = $Profession_data->title;
		$response['is_industry'] = $Profession_data->is_industry;
		
	    return json_encode($response);
	}

	public function login()
	{
		$data= array(
			'loginaction' => '/home/loginaction'
			);
		return view('front-end.pages.login')->with($data);
	}

	public function loginaction(Request $request)
	{
		$data = WebUsers::select('*')

		->where('username', '=', $request->post('username'))

		->where('show_password', '=', $request->post('password'))

		->first();

		$response = array();
		if(!empty($data))
		{
			Session::put(['web_session' => $data]);
			$response['session'] = Session::get('web_session');
			$response['error'] = 0; 
			$response['message'] = 'Success';
		}
		else
		{
			$response['error'] = 1; 
			$response['message'] = 'Wrong Credential';
		}
		return json_encode($response);
	}

	public function getStates(Request $request)
    {     
       $state_id = $request->post('state_id');
       $state = new States;     
       $data = $state->select('id','state_name')->where('country_id', '=', $request->post('country_id'))->get();

       $html = '<option value="">Select State</option>';
       foreach($data as $row)
       { 
        if($row->id==$state_id)
        {
            $selected = "selected";
        }else{
            
            $selected = "";
        }
            
         $html .= '<option value="'.$row->id.'" '.$selected.'>'.ucfirst($row->state_name).'</option>';
       }
      echo $html;exit; 
     
    }
    public function getDistricts(Request $request)
    {     
        $district_id = $request->post('district_id');
       $districts = new Districts;       
       $data = $districts->select('id','district_name')->where('state_id', '=', $request->post('state_id'))->get();

       $html = '<option value="">--Select District--</option>';
       foreach($data as $row)
       {
            if($row->id==$district_id)
            {
                $selected = "selected";
            }else{
                $selected = "";
            }
         $html .= '<option value="'.$row->id.'" '.$selected.'>'.ucfirst($row->district_name).'</option>';
       }

       echo $html;exit; 
     
    }


	 public function getStates_(Request $request)
    {     
       $state = new States;     
       $data = $state->select('id','state_name')->where('country_id', '=', $request->post('country_id'))->get();

       $html = '<option value="">--Select State--</option>';
       foreach($data as $row)
       {
         $html .= '<option value="'.$row->id.'">'.ucfirst($row->state_name).'</option>';
       }
      echo $html;exit; 
     
    }
    public function getDistricts_(Request $request)
    {     
       $districts = new Districts;       
       $data = $districts->select('id','district_name')->where('state_id', '=', $request->post('state_id'))->get();

       $html = '<option value="">--Select District--</option>';
       foreach($data as $row)
       {
         $html .= '<option value="'.$row->id.'">'.ucfirst($row->district_name).'</option>';
       }

       echo $html;exit; 
     
    }

    public function activateprofile()
    {
    	if (Session::get('web_session')){
		$WebUsers = new WebUsers;
		$session_id = Session::get('web_session')->id;
		$user = $WebUsers->where('id',$session_id)->first(); 

		$Profession = new Profession;
		$Profession_data = $Profession->where('status','Active')->get();

		$Countries = new Countries;
		$Countries_data = $Countries->where('status','Active')->get();

		$States = new States;
		$States_data = $States->where('status','Active')->get();

		$Districts = new Districts;
		$Districts_data = $Districts->where('status','Active')->get();

		$Domain = new Domain;
		$Domain_data = $Domain->where('status','Active')->get();

		$packages = new Packages;
		$packages_data = $packages->where('status','Active')->first();

		$data= array(
			'user' => $user,
			'editprofileaction' => '/home/editprofileaction',
			'Profession'=>$Profession_data,
			'Countries'=>$Countries_data,
			'States'=>$States_data,
			'Districts'=>$Districts_data,
			'Domain'=>$Domain_data,
			'packages_data' => $packages_data,
			);
				
		return view("front-end.pages.activateprof")->with($data);;			

		}else{ return redirect('/'); }		

    }



	public function editprofile(Request $request){

		if (Session::get('web_session')){
		$WebUsers = new WebUsers;
		$session_id = Session::get('web_session')->id;
		$user = $WebUsers->where('id',$session_id)->first(); 

		$Profession = new Profession;
		$Profession_data = $Profession->where('status','Active')->get();

		$Countries = new Countries;
		$Countries_data = $Countries->where('status','Active')->get();

		$States = new States;
		$States_data = $States->where('status','Active')->get();

		$Districts = new Districts;
		$Districts_data = $Districts->where('status','Active')->get();

		$Domain = new Domain;
		$Domain_data = $Domain->where('status','Active')->get();

		$packages = new Packages;
		$packages_data = $packages->where('status','Active')->first();

		$data= array(
			'user' => $user,
			'editprofileaction' => '/home/editprofileaction',
			'Profession'=>$Profession_data,
			'Countries'=>$Countries_data,
			'States'=>$States_data,
			'Districts'=>$Districts_data,
			'Domain'=>$Domain_data,
			'packages_data' => $packages_data,
			);
				
		return view("front-end.pages.editprofile")->with($data);;			

		}else{ return redirect('/'); }		

		}


	public function editprofileaction(Request $request)
	{
		$WebUsers = new WebUsers;
		$session_id = Session::get('web_session')->id;
		$user = $WebUsers->where('id',$session_id)->first();

		if ($request->hasFile('user_image')) 
		{
			$image = $request->file('user_image');
			$name = str_slug($request->user_image).'.'.$image->getClientOriginalExtension();
			$destinationPath = public_path('assets/front-end/images/profile');
			$imagePath = $destinationPath. "/".  $name;
			$image->move($destinationPath, $name);
			$user_image = $name;
		}
		else
		{
			$user_image = $request->post('old_user_image');
		}

		if(!empty($request->post('first_name1')))
		{
			$f_name = $request->post('first_name1');
			$l_name = $request->post('last_name1');
			$password = $request->post('password1');
			$show_password = $request->post('show_password1');

			$data = array(
			'first_name' => $f_name,
			'last_name'	=> $l_name,
			'email'	=> $request->post('email'),
			'mobile'    => $request->post('mobile1'),
			'username'   => $request->post('mobile1'),
			'user_image' => $user_image,
			'company_name' => $request->post('company_name'),		
			'domain1_id'   	=> $request->post('domain1_id_'),      		
			'domain2_id'   	=> $request->post('domain2_id_'),      		
			'domain3_id'   	=> $request->post('domain3_id_'),      		
			'company_country_id'   	=> $request->post('company_country_id'),      		
			'company_state_id'   	=> $request->post('company_state_id'),      		
			'company_city_id'   	=> $request->post('company_city_id'),      		
			'website_link'   	=> $request->post('website_link'),  
			'company_tagline' => $request->post('company_tagline'),    		
			'company_official_email'   	=> $request->post('company_official_emai'),      			 		
			'updated_at' => now()
			);
		}
		else
		{
			$f_name = $request->post('first_name');
			$l_name = $request->post('last_name');
			$password = $request->post('password');
			$show_password = $request->post('show_password');

			$data = array(
			'first_name' => $f_name,
			'last_name'	=> $l_name,
			'user_image' => $user_image,
			'email'	=> $request->post('email'),
			'mobile'    => $request->post('mobile'),
			'username'    => $request->post('mobile'),	      			 		
			'updated_at' => now()
			);
		}

		$query = $WebUsers->where('id', $session_id)->update($data);
		//print_r($query);exit();
		return redirect()->back();
	}

	public function savelike(Request $request)
	{
	    $Comments = new Comments;
		$data = array(
		'post_id' => $request->post('id'),
		'web_user_id' => $request->post('unique_session_id'),
		'likes' => 1,		 		
		'created_at' => now(),
		'updated_at' => now()
		);
		$Comments->insert($data);
		$last_id = DB::getPdo()->lastInsertId();
		$postid = $request->post('id');
		$like = $Comments->where('post_id',$postid)->sum('likes');
		echo $like;

	}


	


	public function logout(){

		Session::forget(['web_session']);
		return redirect('/');

	}

}