<?php
namespace app;

/**
* class MSG91 to send SMS on Mobile Numbers.
**/

class MSG91 {

	function __construct() {

	}

	private $API_KEY = 'gUUS1AREJ0C4zkA5i0CMxg';
	private $SENDER_ID = "HRNGIN";

	public function sendSMS($OTP, $mobileNumber){		
		$isError = 0;
		$errorMessage = true;

		//Your message to send, Adding URL encoding.
	    //$message = urlencode("Welcome to codershood.info. Your OPT is : $OTP");
	 	$message = rawurlencode("Your OTP  ".$OTP." for Profeonet is Valid For Next 20 Mins.");
	 
	    $url = 'https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey='.$this->API_KEY.'&senderid='.$this->SENDER_ID.'&channel=2&DCS=0&flashsms=0&number='.$mobileNumber.'&text='.$message.'&route=1';

	 
	    $ch = curl_init();
	    curl_setopt_array($ch, array(
	        CURLOPT_URL => $url,
	        CURLOPT_RETURNTRANSFER => true,
	        CURLOPT_POST => true,
	        CURLOPT_POSTFIELDS => ''
	    ));
	 
	    //Ignore SSL certificate verification
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	 
	 
	    //get response
	    $output = curl_exec($ch);
	 
	    //Print error if any
	    if (curl_errno($ch)) {
	    	$isError = true;
	        $errorMessage = curl_error($ch);
	    }
	    curl_close($ch);
	    if($isError){
	    	return array('error' => 1 , 'message' => $errorMessage);
	    }else{
	    	return array('error' => 0 );
	    }
	}
}
?>