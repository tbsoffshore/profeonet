<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = ['fld_Iam','fld_username','fld_first_name','fld_last_name','fld_full_name','fld_email_id','fld_mobile_no','fld_password','fld_show_password','created_at','updated_at'];
}
