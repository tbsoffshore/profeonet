<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Model\Admin\Discipline;
use App\Model\Admin\Domain;

class Colleges extends Model
{
    //
    protected $table='colleges';
    
    
    public function discipline(){
    	
    	return $this->hasOne('App\Model\Admin\Discipline','id','mst_discipline_id');
    }

    public function domain(){
    	
    	return $this->hasOne('App\Model\Admin\Domain','id','mst_domain_id');
    }
}
