<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Model\Admin\Discipline;
use App\Model\Admin\Domain;
use App\Model\Admin\States;
use App\Model\Admin\Areas;
use App\Model\Admin\Colleges;

class Connections extends Model
{
    //
    protected $table='connections';
    protected $fillable = ['*'];
    
    public function discipline()
    {
        return $this->hasOne('App\Model\Admin\Discipline','id','mst_discipline_id');
    }

    public function domain()
    {
        return $this->hasOne('App\Model\Admin\Domain','id','mst_domain_id');
    }

     public function state(){
        
        return $this->hasOne('App\Model\Admin\States','id','states_id');
    }

    public function areas(){
        
        return $this->hasOne('App\Model\Admin\Areas','id','area_of_work_id');
    }
    public function colleges(){
        
        return $this->hasOne('App\Model\Admin\Colleges','id','colleges_id');
    }
}
