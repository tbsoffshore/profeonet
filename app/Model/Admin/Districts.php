<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Model\Admin\Countries;
use App\Model\Admin\States;

class Districts extends Model
{
    //
    protected $table='districts';
    protected $fillable = ['*'];
    
    public function country(){
    	
    	return $this->hasOne('App\Model\Admin\Countries','id','country_id');
    }

    public function state(){
    	
    	return $this->hasOne('App\Model\Admin\States','id','state_id');
    }
}
