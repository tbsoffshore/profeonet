<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Model\Admin\Discipline;

class Domain extends Model
{
    //
    protected $table='mst_domain';

    public function discipline(){
    	
    	return $this->hasOne('App\Model\Admin\Discipline','id','mst_discipline_id');
    }

  
}
