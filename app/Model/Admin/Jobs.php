<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $table = 'jobs';

    protected $primarykey = "fld_id";
}
