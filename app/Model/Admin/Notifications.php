<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Model\Admin\Discipline;
use App\Model\Admin\Domain;
use App\Model\Admin\Countries;

use App\Model\Admin\States;

use App\Model\Admin\Districts;
use App\Model\Admin\Scopes;
use App\Model\Admin\NotificationCategory;

class Notifications extends Model
{
    //
    protected $table='notifications';
    
    
    public function discipline(){
    	
    	return $this->hasOne('App\Model\Admin\Discipline','id','mst_discipline_id');
    }

    public function domain(){
    	
    	return $this->hasOne('App\Model\Admin\Domain','id','mst_domain_id');
    }

    public function country(){
        
        return $this->hasOne('App\Model\Admin\Countries','id','country_id');
    }

     public function state(){
        
        return $this->hasOne('App\Model\Admin\States','id','state_id');
    }

     public function district(){
        
        return $this->hasOne('App\Model\Admin\Districts','id','district_id');
    }

    public function scope(){
        
        return $this->hasOne('App\Model\Admin\Scopes','id','scope_id');
    }

    public function category(){
        
        return $this->hasOne('App\Model\Admin\NotificationCategory','id','mst_notification_category_id');
    }
}
