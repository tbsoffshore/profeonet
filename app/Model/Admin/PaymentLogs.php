<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class PaymentLogs extends Model
{
    protected $table='payment_logs';
}
