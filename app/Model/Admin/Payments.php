<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Model\Admin\Members;
class Payments extends Model
{
    protected $table='payment';


    public function member()
    {    	
    	return $this->hasOne('App\Model\Admin\Members','fld_member_id','fld_member_id');
    }
}
