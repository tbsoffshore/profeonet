<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class ProfileBenefits extends Model
{
    protected $table = 'profile_benefits';
}
