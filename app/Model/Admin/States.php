<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Model\Admin\Countries;
class States extends Model
{
    protected $table='states';

    public function country()
    {    	
    	return $this->hasOne('App\Model\Admin\Countries','id','country_id');
    }
}
