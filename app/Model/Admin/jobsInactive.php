<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class jobsInactive extends Model
{
    protected $table = 'jobs';
}
