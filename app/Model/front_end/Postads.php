<?php

namespace App\Model\front_end;

use Illuminate\Database\Eloquent\Model;
use App\Model\Admin\WebUsers;

class Postads extends Model
{
    protected $table = 'posts';

    protected $fillable = ['*'];
     public function web_users()
    {    	
    	return $this->hasOne('App\Model\front_end\WebUsers','id','web_users_id');
    }
}
   

