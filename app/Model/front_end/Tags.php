<?php

namespace App\Model\front_end;

use Illuminate\Database\Eloquent\Model;
use App\Model\Admin\WebUsers;
use App\Model\Admin\Postads;

class Tags extends Model
{
    protected $table = 'tags';

    protected $fillable = ['*'];
     public function user()
    {    	
    	return $this->hasOne('App\Model\front_end\WebUsers','id','web_users_id');
    }

     public function post()
    {    	
    	return $this->hasOne('App\Model\front_end\Postads','id','posts_id');
    }
}
   

