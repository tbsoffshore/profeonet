<?php

namespace App\Model\front_end;

use Illuminate\Database\Eloquent\Model;

class WebUsers extends Model
{
    protected $table = 'web_users';
    protected $hidden = ['password','show_password'];
}
