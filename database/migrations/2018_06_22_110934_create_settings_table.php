<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('site_title');
            $table->bigint('mobile1');
            $table->bigint('mobile2');
            $table->string('copy_right');
            $table->string('email');
            $table->string('address');
            $table->string('notification');
            $table->string('logo');
            $table->text('site_map');
            $table->string('facebook_link');
            $table->string('twitter_link');
            $table->string('insta_link');
            $table->enum('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
