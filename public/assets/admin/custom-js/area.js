$(function(){
  // alert("hidsfsfsii");
   getDomains();
});
function getDomains()
{
  var url = BASE_URL+'admin/areas/getDomains';
  //alert(url);
  var mst_discipline_id = $("#mst_discipline_id").val();
  var hide_domain_id = $("#hide_domain_id").val();
   $.ajax({  
      url:url,
      cache: false,
      type:'POST',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data:{mst_discipline_id:mst_discipline_id,domain_id:hide_domain_id},
      success: function(response)
      {
         //alert(response);return false;
          $("#mst_domain_id").html(response);
      }
   });
}


function areasValidation()
{
  var mst_discipline_id = $.trim($("#mst_discipline_id").val());
  var mst_domain_id = $.trim($("#mst_domain_id").val());
  var title = $.trim($("#title").val());
  var flag = 0;

  if(mst_discipline_id == "")
  {
    $("#errorDiscipline").fadeIn().html("Required");
    setTimeout(function(){$("#errorDiscipline").html("&nbsp;");},3000)
    $("#mst_discipline_id").focus();
    flag++;   
    return false;
  }

  if(mst_domain_id == "")
  {
    $("#errorDomain").fadeIn().html("Required");
    setTimeout(function(){$("#errorDomain").html("&nbsp;");},3000)
    $("#mst_domain_id").focus();
    flag++;   
    return false;
  }

  if(title == "")
  {
    $("#errorTitle").fadeIn().html("Required");
    setTimeout(function(){$("#errorTitle").html("&nbsp;");},3000)
    $("#title").focus();
    flag++;   
    return false;
  }
  else if(flag == 0)
  {
     $(".preloader").show();
  }

}
