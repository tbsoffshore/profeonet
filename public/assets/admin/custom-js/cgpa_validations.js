var master ={

 'cgpaAdd':function()
{
	var title = $.trim($("#title").val());
	var id = $.trim($("#primary_id").val());
	var flag = 0;

	if(title == "")
	{

		$("#errorTitle").fadeIn().html("Required");

		setTimeout(function(){$("#errorTitle").html("&nbsp;");},3000)

		$("#title").focus();

		
		return false;
	}

	else
	{
		var datastring = "title="+title+"&id="+id;
		//alert(datastring);return false;
	   $.ajax({
	            type:'POST',
	            url: BASE_URL + 'admin/cgpa/check_duplication',
	            data:datastring,
	            cache:false,
	            success:function(response){
	            	//alert(response);return false;
	        	d = JSON.parse(response);

	        	if(d.error==1)
	        	{
	        		$("#errorTitle").fadeIn().html(d.message);
	                setTimeout(function(){$("#errorTitle").html("&nbsp;");},3000)
					$("#title").focus();
					return false;		
	        	}
	        	else
	        	{
	        		
	        		$("#form").submit();
	        		return false;
	        		//window.location.href=BASE_URL + 'admin/baners/list';
	        	}
	        }   
	    });
	}

}

}