function getDomain (pointer)
{
  var url = pointer.getAttribute("data-url");
  var mst_discipline_id = $("#mst_discipline_id").val();
 // alert(url);return;
  $.ajax({  
      url:url,
      cache: false,
      type:'POST',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data:{mst_discipline_id:mst_discipline_id},
      success: function(response)
      {
        //alert(response);
          $("#mst_domain_id").html(response);
      }
   });
}


function getDistricts(pointer)
{
  var url = pointer.getAttribute("data-url");
  var state_id = $("#state_id").val();
   $.ajax({  
      url:url,
      cache: false,
      type:'POST',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data:{state_id:state_id},
      success: function(response)
      {
          $("#district_id").html(response);
      }
   });
}
