/*Get Domains*/

$(function(){
  // alert("hidsfsfsii");
   getDomains();
});
function getDomains()
{
  var url = BASE_URL+'admin/connections/getDomains';
  //alert(url);
  var mst_discipline_id = $("#mst_discipline_id").val();
  var hide_domain_id = $("#hide_domain_id").val();
   $.ajax({  
      url:url,
      cache: false,
      type:'POST',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data:{mst_discipline_id:mst_discipline_id,domain_id:hide_domain_id},
      success: function(response)
      {
         //alert(response);return false;
          $("#mst_domain_id").html(response);
      }
   });
}
/*get Area of work*/

function getAreaworks()
{
  var url = BASE_URL+'admin/connections/getAreaworks';
  var mst_domain_id = $("#mst_domain_id").val();
  var hide_area_of_work_id = $("#hide_area_of_work_id").val();
   $.ajax({  
      url:url,
      cache: false,
      type:'POST',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data:{mst_domain_id:mst_domain_id,area_of_work_id:hide_area_of_work_id},
      success: function(response)
      {
          $("#area_of_work_id").html(response);
      },
      complete: function(){
        getCollege();

      }
   });
}

/*Get Colleges*/
function getCollege()
{
  var url = BASE_URL+'admin/connections/getCollege';
  var mst_domain_id = $("#mst_domain_id").val();
  var hide_colleges_id = $("#hide_colleges_id").val();
   $.ajax({  
      url:url,
      cache: false,
      type:'POST',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data:{mst_domain_id:mst_domain_id,colleges_id:hide_colleges_id},
      success: function(response)
      {
          $("#colleges_id").html(response);
      }
   });
}


function connectionValidation()
{
  var mst_discipline_id = $.trim($("#mst_discipline_id").val());
  var mst_domain_id = $.trim($("#mst_domain_id").val());
  var states_id = $.trim($("#states_id").val());
  var description = CKEDITOR.instances.description.getData(); 
  var image = $("#image").val();
  var id = $.trim($("#primary_id").val());
 

  if(mst_discipline_id == "")
  {
    $("#errorDiscipline").fadeIn().html("Required");
    setTimeout(function(){$("#errorDiscipline").html("&nbsp;");},3000)
    $("#mst_discipline_id").focus();
    return false;
  }
  if(mst_domain_id == "")
  {
    $("#errorDomain").fadeIn().html("Required");
    setTimeout(function(){$("#errorDomain").html("&nbsp;");},3000)
    $("#mst_domain_id").focus();
    return false;
  }

  if(states_id == "")
  {
    $("#errorCountry").fadeIn().html("Required");
    setTimeout(function(){$("#errorCountry").html("&nbsp;");},3000)
    $("#states_id").focus();
    return false;
  }

  if(description == "")
  {
    $("#errorDescription").fadeIn().html("Required");
    setTimeout(function(){$("#errorDescription").html("&nbsp;");},3000)
    $("#description").focus();
    
    return false;
  }

   if(id=='')
    {
      if($.trim(image)=='')
      {   
        $("#errimage").fadeIn().html("Required");
        setTimeout(function(){$("#errimage").html("&nbsp;");},3000);
        $("#image").focus();
        return false;
      }
   }

}


