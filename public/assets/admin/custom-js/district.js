$(function()
{
   getStates();
});

function getStates()
{
  /*var url = pointer.getAttribute("data-url");*/
  var url = BASE_URL+'admin/districts/getStates';
 /* alert(url);return false;*/
  var country_id = $("#country_id").val();
  var hide_state_id = $("#hide_state_id").val();

   $.ajax({  
      url:url,
      cache: false,
      type:'POST',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data:{country_id:country_id,state_id:hide_state_id},
      success: function(response)
      {
          //alert(response);return false;
          $("#state_id").html(response);
      }
   });
}


function getDistricts(pointer)
{
  var url = pointer.getAttribute("data-url");
  var state_id = $("#state_id").val();
   $.ajax({  
      url:url,
      cache: false,
      type:'POST',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data:{state_id:state_id},
      success: function(response)
      {
          $("#district_id").html(response);
      }
   });
}

