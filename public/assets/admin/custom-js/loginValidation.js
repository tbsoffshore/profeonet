function loginValidation()
{
	var email = $.trim($("#email").val());
	var pattern_email= /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
	var password = $.trim($("#password").val());
	var flag = 0;
	
	if(email == "")
	{
		$("#email_error").fadeIn().html("Required");
		setTimeout(function(){$("#email_error").html("&nbsp;");},3000)
		$("#email").focus();
		flag++;		
		return false;

	}

	else if(!pattern_email.test(email))
	{
		$("#email_error").fadeIn().html("Invalid");
		setTimeout(function(){$("#email_error").html("&nbsp;");},3000)
		$("#email").focus();
		flag++;
		return false;

	}

	else if(password == "")
	{
		$("#password_error").fadeIn().html("Required");
		setTimeout(function(){$("#password_error").html("&nbsp;");},3000)
		$("#password").focus();
		flag++;
		return false;

	}
	else if(flag == 0)
	{
	   $(".preloader").show();
	}
}
function showloader()
{
	$(".preloader").show();
}