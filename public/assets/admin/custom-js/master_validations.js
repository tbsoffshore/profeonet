function countryAdd()
{

	var title = $.trim($("#country_name").val());
	var flag = 0;

	if(title == "")

	{

		$("#errorCountry").fadeIn().html("Required");

		setTimeout(function(){$("#errorCountry").html("&nbsp;");},3000)

		$("#country_name").focus();

		flag++;		

		return false;



	}

	else if(flag == 0)

	{

	   $(".preloader").show();

	}

}



function stateAdd()

{



	var country_id = $.trim($("#country_id").val());

	var state_name = $.trim($("#state_name").val());

	

	var flag = 0;



	if(country_id == "")

	{

		$("#errorCountry").fadeIn().html("Required");

		setTimeout(function(){$("#errorCountry").html("&nbsp;");},3000)

		$("#country_id").focus();

		flag++;		

		return false;

	}

	

	if(state_name == "")

	{

		$("#errorState").fadeIn().html("Required");

		setTimeout(function(){$("#errorState").html("&nbsp;");},3000)

		$("#state_name").focus();

		flag++;		

		return false;



	}

	else if(flag == 0)

	{

	   $(".preloader").show();

	}

}



function districtAdd()

{



	var country_id = $.trim($("#country_id").val());

	var state_id = $.trim($("#state_id").val());

	var district_name = $.trim($("#district_name").val());

	

	var flag = 0;



	if(country_id == "")

	{

		$("#errorCountry").fadeIn().html("Required");

		setTimeout(function(){$("#errorCountry").html("&nbsp;");},3000)

		$("#country_id").focus();

		flag++;		

		return false;

	}



	if(state_id == "")

	{

		$("#errorState").fadeIn().html("Required");

		setTimeout(function(){$("#errorState").html("&nbsp;");},3000)

		$("#state_id").focus();

		flag++;		

		return false;

	}

	

	if(district_name == "")

	{

		$("#errorDistrict").fadeIn().html("Required");

		setTimeout(function(){$("#errorDistrict").html("&nbsp;");},3000)

		$("#district_name").focus();

		flag++;		

		return false;



	}

	else if(flag == 0)

	{

	   $(".preloader").show();

	}

}






/*validation for ourpartners end*/

/*validation for setting start*/

 function settingsVal()

 {

 	var site_title		 	= $.trim($("#site_title").val());

 	var mobile1 			= $.trim($("#mobile1").val());

 	var mobile2 			= $.trim($("#mobile2").val());

 	var email 				= $.trim($("#email").val());

 	var address 			= $.trim($("#address").val());

 	var notification 		= $.trim($("#notification").val());

 	var copy_right 			= $.trim($("#copy_right").val());

 	var facebook_link 		= $.trim($("#facebook_link").val());

 	var twitter_link 		= $.trim($("#twitter_link").val());

 	var insta_link 			= $.trim($("#insta_link").val());

	var id 					= $.trim($("#primary_id").val());



	var pattern_email = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i; 

    var pattern_mobile = /^[0-9]{10,15}$/i;



	if(site_title == "")

	{

		$("#errorsite_title").fadeIn().html("Required");

		setTimeout(function(){$("#errorsite_title").html("&nbsp;");},3000)

		$("#site_title").focus();

		return false;

	}

	if(mobile1 == "")

	{

		$("#errormobile1").fadeIn().html("Required");

		setTimeout(function(){$("#errormobile1").html("&nbsp;");},3000)

		$("#mobile1").focus();

		return false;

	}

	else if(!pattern_mobile.test(mobile1))

	{

	  	$("#errormobile1").fadeIn().html("Please enter valid mobile");

	    setTimeout(function(){$("#errormobile1").html("&nbsp;");},3000)

	    $("#mobile1").focus();

	    return false;       

	}

	if(mobile2 == "")

	{

		$("#errormobile2").fadeIn().html("Required");

		setTimeout(function(){$("#errormobile2").html("&nbsp;");},3000)

		$("#mobile2").focus();

		return false;

	}

	if(email == "")

	{

		$("#erroremail").fadeIn().html("Required");

		setTimeout(function(){$("#erroremail").html("&nbsp;");},3000)

		$("#email").focus();

		return false;

	}

	else if(!pattern_email.test(email))

	{

		$("#erroremail").fadeIn().html("Invalid");

		setTimeout(function(){$("#erroremail").fadeOut();},3000);

		$("#email").focus();

		return false;    

	}

	if(address == "")

	{

		$("#erroraddress").fadeIn().html("Required");

		setTimeout(function(){$("#erroraddress").html("&nbsp;");},3000)

		$("#address").focus();

		return false;

	}

	if(notification == "")

	{

		$("#errornotification").fadeIn().html("Required");

		setTimeout(function(){$("#errornotification").html("&nbsp;");},3000)

		$("#notification").focus();

		return false;

	}

	if(copy_right == "")

	{

		$("#errorcopy_right").fadeIn().html("Required");

		setTimeout(function(){$("#errorcopy_right").html("&nbsp;");},3000)

		$("#copy_right").focus();

		return false;

	}

	if(facebook_link == "")

	{

		$("#errorfacebook_link").fadeIn().html("Required");

		setTimeout(function(){$("#errorfacebook_link").html("&nbsp;");},3000)

		$("#facebook_link").focus();

		return false;

	}

	if(twitter_link == "")

	{

		$("#errortwitter_link").fadeIn().html("Required");

		setTimeout(function(){$("#errortwitter_link").html("&nbsp;");},3000)

		$("#twitter_link").focus();

		return false;

	}

	if(insta_link == "")

	{

		$("#errorinsta_link").fadeIn().html("Required");

		setTimeout(function(){$("#errorinsta_link").html("&nbsp;");},3000)

		$("#insta_link").focus();

		return false;

	}

	



 }

/*validation for setting end*/





function showloader()

{

	$(".preloader").show();

}

function imageFile()

{ 

	var _URL = window.URL || window.webkitURL;

    $('#image').change(function () {  

    var files = this.files;   

    var reader = new FileReader();

    name=this.value;  

    var file, img;

    if ((file = this.files[0])) {

    

        img = new Image();

        img.onload = function () {



        	  var height = this.height;

			   var width = this.width;

             if (height != 100 && width!= 150  ) {

            // if (height > 100 ) {

			       // alert("Height and Width must not exceed 100px.");

			          $("#errorimage").html("Invalid");   

			        //  $("#errorimage").html("Height and Width must not exceed 100px and 150px");   

    					setTimeout(function(){$("#errorimage").html("&nbsp;")},3000);

   					 $("#image").val("");

			        //return false;

			    }

			    //alert("Uploaded image has valid Height and Width.");

			   // return true;

        };

        img.src = _URL.createObjectURL(file);

    }  



    //validation for photo upload type    

    var filetype = name.split(".");

    ext = filetype[filetype.length-1];  //alert(ext);return false;

    if(!(ext=='jpg') && !(ext=='png') && !(ext=='PNG') && !(ext=='jpeg') && !(ext=='img') && !(ext=='JPEG') && !(ext=='JPG'))

    { 

    $("#errorimage").html("Invalid");   

    //$("#errorimage").html("Please select proper type like jpg, png, jpeg, JPEG, PNG image");   

    setTimeout(function(){$("#errorimage").html("&nbsp;")},3000);

    $("#image").val("");

    //return false;

    }



    reader.readAsDataURL(files[0]);

    });

}



function imageLogoFile()

{ 

	var _URL = window.URL || window.webkitURL;

    $('#image').change(function () {  

    var files = this.files;   

    var reader = new FileReader();

    name=this.value;    

   





     var file, img;

    if ((file = this.files[0])) {

    

        img = new Image();

        img.onload = function () {



        	  var height = this.height;

			   var width = this.width;

             if (height != 50 && width!= 148  ) {

            // if (height > 100 ) {

			       // alert("Height and Width must not exceed 100px.");

			          $("#errorimage").html("Invalid");   

			        //  $("#errorimage").html("Height and Width must not exceed 100px and 150px");   

    					setTimeout(function(){$("#errorimage").html("&nbsp;")},3000);

   					 $("#image").val("");

			        //return false;

			    }

			    //alert("Uploaded image has valid Height and Width.");

			   // return true;

        };

        img.src = _URL.createObjectURL(file);

    }  



    //validation for photo upload type    

    var filetype = name.split(".");

    ext = filetype[filetype.length-1];  //alert(ext);return false;

    if(!(ext=='jpg') && !(ext=='png') && !(ext=='PNG') && !(ext=='jpeg') && !(ext=='img') && !(ext=='JPEG') && !(ext=='JPG'))

    { 

    $("#errorimage").html("Invalid");   

    //$("#errorimage").html("Please select proper type like jpg, png, jpeg, JPEG, PNG image");   

    setTimeout(function(){$("#errorimage").html("&nbsp;")},3000);

    $("#image").val("");

    //return false;

    }



    reader.readAsDataURL(files[0]);

    });

}

//only number validaton

function only_number(event)

{



  var x = event.which || event.keyCode;

  console.log(x);

  if((x >= 48 ) && (x <= 57 ) || x == 8 | x == 9 || x == 13 )

  {

    return;

  }else{

    event.preventDefault();

  }    

}



// globle image validation start

function imageVal(id_name,h,w)

{	

	var _URL = window.URL || window.webkitURL;

    $('#'+id_name).change(function () {  

    var files = this.files;   

    var reader = new FileReader();

    name=this.value;

    if(h!='' && w!='')

    {

	    var file, img;

	    if ((file = this.files[0])) {

	    

	        img = new Image();

	        img.onload = function () {



	        	  var height = this.height;

				   var width = this.width;

	             if (height != h && width!= w  ) {

	            // if (height > 100 ) {

				       // alert("Height and Width must not exceed 100px.");

				          $("#error"+id_name).html("Invalid");   

				        //  $("#errorimage").html("Height and Width must not exceed 100px and 150px");   

	    					setTimeout(function(){$("#error"+id_name).html("&nbsp;")},3000);

	   					 $("#"+id_name).val("");

				        //return false;

				    }

				    //alert("Uploaded image has valid Height and Width.");

				   // return true;

	        };

	        img.src = _URL.createObjectURL(file);

	    }  

	 }



    //validation for photo upload type    

    var filetype = name.split(".");

    ext = filetype[filetype.length-1];  //alert(ext);return false;

    if(!(ext=='jpg') && !(ext=='png') && !(ext=='PNG') && !(ext=='jpeg') && !(ext=='img') && !(ext=='JPEG') && !(ext=='JPG'))

    { 

    $("#error"+id_name).html("Invalid");   

    //$("#errorimage").html("Please select proper type like jpg, png, jpeg, JPEG, PNG image");   

    setTimeout(function(){$("#error"+id_name).html("&nbsp;")},3000);

    $("#"+id_name).val("");

    //return false;

    }



    reader.readAsDataURL(files[0]);

    });

	 

}




var master ={

 'professionAdd':function()
{
	var title = $.trim($("#title").val());
	var id = $.trim($("#primary_id").val());
	var flag = 0;

	if(title == "")
	{

		$("#errorTitle").fadeIn().html("Required");

		setTimeout(function(){$("#errorTitle").html("&nbsp;");},3000)

		$("#title").focus();

		
		return false;
	}

	else
	{
		var datastring = "title="+title+"&id="+id;
		//alert(datastring);return false;
	   $.ajax({
	            type:'POST',
	            url: BASE_URL + 'admin/profession/check_duplication',
	            data:datastring,
	            cache:false,
	            success:function(response){
	            	//alert(response);return false;
	        	d = JSON.parse(response);

	        	if(d.error==1)
	        	{
	        		$("#errorTitle").fadeIn().html(d.message);
	                setTimeout(function(){$("#errorTitle").html("&nbsp;");},3000)
					$("#title").focus();
					return false;		
	        	}
	        	else
	        	{
	        		
	        		$("#form").submit();
	        		return false;
	        		//window.location.href=BASE_URL + 'admin/baners/list';
	        	}
	        }   
	    });
	}

}

}

function domainAdd()
{
    var mst_discipline_id = $.trim($("#mst_discipline_id").val());
    var title = $.trim($("#title").val());
	var id = $.trim($("#primary_id").val());
	var flag = 0;

	if(mst_discipline_id == "")

	{
       
		$("#errorDiscipline").fadeIn().html("Required");

		setTimeout(function(){$("#errorDiscipline").html("&nbsp;");},3000)

		$("#mst_discipline_id").focus();

		return false;

	}

	if(title == "")
	{

		$("#errorTitle").fadeIn().html("Required");

		setTimeout(function(){$("#errorTitle").html("&nbsp;");},3000)

		$("#title").focus();

		return false;
	}

	else
	{
		var datastring = "title="+title+"&id="+id;
		//alert(datastring);return false;
	   $.ajax({
	            type:'POST',
	            url: BASE_URL + 'admin/domain/check_duplication',
	            data:datastring,
	            cache:false,
	            success:function(response){
	            	//alert(response);return false;
	        	d = JSON.parse(response);

	        	if(d.error==1)
	        	{
	        		$("#errorTitle").fadeIn().html(d.message);
	                setTimeout(function(){$("#errorTitle").html("&nbsp;");},3000)
					$("#title").focus();
					return false;		
	        	}
	        	else
	        	{
	        		
	        		$("#form").submit();
	        		return false;
	        		//window.location.href=BASE_URL + 'admin/baners/list';
	        	}
	        }   
	    });
	}

}

function banerAdd($val)
{
	var title = $.trim($("#title").val());
	var description = CKEDITOR.instances.description.getData(); 
	var id = $val;
	alert(id);return false;
	/*var image = $.trim($("#image").val());
	if(title == "")
	{
		$("#errorTitle").fadeIn().html("Required");
		setTimeout(function(){$("#errorTitle").html("&nbsp;");},3000)
		$("#title").focus();
		return false;
	}
    if(description == "")
	{
		$("#errorDescription").fadeIn().html("Required");
		setTimeout(function(){$("#errorDescription").html("&nbsp;");},3000)
		$("#description").focus();		
		return false;
	}
	
	if(image == "")
	{
		$("#errorimage").fadeIn().html("Required");
		setTimeout(function(){$("#errorimage").html("&nbsp;");},3000)
		$("#image").focus();	
		return false;
	}
	
	
	if(title && description && image)
	{
		var extension = image.split('.').pop().toLowerCase();
    	if($.inArray(extension, ['png','jpg','jpeg','gif']) == -1) 
    	{
            $("#errorimage").fadeIn().html("Only jpg,jpeg,png,gif files allowed");
            setTimeout(function(){$("#errorimage").fadeOut();},3000);
            $("#image").focus();
            return false;
		}
		else
    	{
	        var datastring = "title="+title+"&id="+id;
		    $.ajax({
		            type:'POST',
		            url: BASE_URL + 'admin/baners/check_duplication',
		            data:datastring,
		            cache:false,
		            success:function(response){
		        	d = JSON.parse(response);

		        	if(d.error==1)
		        	{
		        		$("#errorTitle").fadeIn().html(d.message);
		                setTimeout(function(){$("#errorTitle").html("&nbsp;");},3000)
						$("#title").focus();
						return false;		
		        	}
		        	else
		        	{
		        		$("#form").submit();
		        		return false;
		        		//window.location.href=BASE_URL + 'admin/baners/list';
		        	}
		        }   
		    });
    }

	}*/
}