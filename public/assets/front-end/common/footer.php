<footer class="main-footer">
    
    <!--Widgets Section-->
    <div class="widgets-section">
        <div class="container">
            <div class="row">
                <!--Big Column-->
                <div class="big-column col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        
                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget about-widget contact-widget">
                                <h3 class="footer-title">About Us</h3>
                                
                                <div class="widget-content">
                                    <div class="text"><p class="colorccc">The Profeonet consulting over 20 years of experience we’ll ensure you always get the best guidance.</p> </div>
                                       
                                     <ul class="social">
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget links-widget">
                                <h3 class="footer-title">Our Services</h3>
                                <div class="widget-content">
                                    <ul class="list">
                                        <li><a href="#" class="colorccc">Business Growth</a></li>
                                        <li><a href="#" class="colorccc">Sustainability</a></li>
                                        <li><a href="#" class="colorccc">Performance</a></li>
                                        <li><a href="#" class="colorccc">Advanced Analytics</a></li>
                                    
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Big Column-->
                <div class="big-column col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        

                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget contact-widget">
                                <h3 class="footer-title">Contact us</h3>
                                <div class="widget-content">
                                    <ul class="contact-info">
                                        <li><span class="icon-signs colorccc"></span>22/121 Apple Street, New York, <br>NY 10012, USA</li>
                                        <li><span class="icon-phone-call colorccc"></span> Phone: +123-456-7890</li>
                                        <li><span class="icon-e-mail-envelope colorccc"></span>Mail@Fortuneteam.com</li>
                                    </ul>
                                </div>
                          
                            </div>
                        </div>

                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget news-widget">
                                <h3 class="footer-title">Newsletter</h3>
                                <div class="widget-content">
                                    <!--Post-->
                                    <div class="text"><p class="colorccc">Sign up for latest product news</p></div>
                                    <!--Post-->
                                    <form action="#" class="default-form">
                                        <input type="email" placeholder="Email Address">
                                        <button type="submit" class="thm-btn">Subscribe Us</button>
                                    </form>
                                </div>
                                
                            </div>
                        </div>                     
                        
                    </div>
                </div>
                
             </div>
         </div>
     </div>
     
     <!--Footer Bottom-->
     <section class="footer-bottom">
        <div class="container">
            <div class="pull-left copy-text">
                <p class="colorccc">Copyrights © 2018. All Rights Reserved. Powered by  <a href="#"> <b> The Profeonet. </b></a></p>
                
            </div><!-- /.pull-right -->
            <div class="pull-right get-text">
                <ul>
                    <li><a href="#" class="colorccc">Support |  </a></li>
                    <li><a href="#" class="colorccc">Privacy & Policy |</a></li>
                    <li><a href="#" class="colorccc"> Terms & Conditions</a></li>
                </ul>
            </div><!-- /.pull-left -->
        </div><!-- /.container -->
    </section>
     
</footer>

<!-- Scroll Top Button -->
	<button class="scroll-top tran3s color2_bg">
		<span class="fa fa-angle-up"></span>
	</button>
	<!-- pre loader  -->
	<div class="preloader"></div>


	<!-- jQuery js -->
	<script src="js/jquery.js"></script>
	<!-- bootstrap js -->
	<script src="js/bootstrap.min.js"></script>
	<!-- jQuery ui js -->
	<script src="js/jquery-ui.js"></script>
	<!-- owl carousel js -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- jQuery validation -->
	<script src="js/jquery.validate.min.js"></script>

	<!-- mixit up -->
	<script src="js/wow.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.fitvids.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
	<script src="js/menuzord.js"></script>

	<!-- revolution slider js -->
	<script src="js/jquery.themepunch.tools.min.js"></script>
	<script src="js/jquery.themepunch.revolution.min.js"></script>
	<script src="js/revolution.extension.actions.min.js"></script>
	<script src="js/revolution.extension.carousel.min.js"></script>
	<script src="js/revolution.extension.kenburn.min.js"></script>
	<script src="js/revolution.extension.layeranimation.min.js"></script>
	<script src="js/revolution.extension.migration.min.js"></script>
	<script src="js/revolution.extension.navigation.min.js"></script>
	<script src="js/revolution.extension.parallax.min.js"></script>
	<script src="js/revolution.extension.slideanims.min.js"></script>
	<script src="js/revolution.extension.video.min.js"></script>

	<!-- fancy box -->
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.polyglot.language.switcher.js"></script>
	<script src="js/nouislider.js"></script>
	<script src="js/jquery.bootstrap-touchspin.js"></script>
	<script src="js/SmoothScroll.js"></script>
	<script src="js/jquery.appear.js"></script>
	<script src="js/jquery.countTo.js"></script>
	<script src="js/jquery.flexslider.js"></script>
	<script src="js/imagezoom.js"></script>	
    <script src="js/bxslider.js"></script> 
	<script id="map-script" src="js/default-map.js"></script>
	<script src="js/custom.js"></script>

</div>
</body>
</html>