<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PROFEONET</title> 
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/mystyle.css">
	<link rel="apple-touch-icon" sizes="180x180" href="images/fav-icon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="images/fav-icon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="images/fav-icon/favicon-16x16.png" sizes="16x16">
</head>
<body>
<div class="boxed_wrapper">
<header class="top-bar">
    <div class="container">
        <div class="clearfix">
            <div class="col-left float_left">
                <ul class="top-bar-info">
                    <li><i class="icon-technology"></i>Phone: (123) 0200 12345</li>
                    <li><i class="icon-note2"></i>help@profeonet.com</li>
                </ul>
            </div>
            <div class="col-right float_right">
                <ul class="social">
                    <li>Stay Connected: </li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                </ul>
                <div class="link">
                    <a href="account.php" class="thm-btn">LOGIN / SIGNUP</a>
                </div>
            </div>
                
                
        </div>
            

    </div>
</header>

<section class="theme_menu stricky">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="main-logo">
                    <a href="index.php"><img src="images/logo/logo.png" alt=""></a>
                </div>
            </div>
            <div class="col-md-8 menu-column">
                <nav class="menuzord" id="main_menu">
                   <ul class="menuzord-menu">
                        <?php $page = basename($_SERVER['PHP_SELF']);?>     
                        <li class="<?php if($page=='index.php'){?>active<?php } ?>"><a href="index.php">Home</a></li>

                        <li><a href="#">About us</a>
                        <ul class="dropdown">
                            <li><a href="about.php">About us</a></li>
                            <li><a href="team.php">Meet Our Team</a></li>
                            <li><a href="faq.php">FAQ’s</a></li>
                            <li><a href="testimonials.php">Client Feedback</a></li>
                         </ul>
                        </li>

                        <li><a href="service.php">services</a>
                        <ul class="dropdown">
                            <li><a href="service-1.php">Business Growth</a></li>
                            <li><a href="service-2.php">Sustainability</a></li>
                            <li><a href="service-3.php">Performance</a></li>
                            <li><a href="service-4.php">Advanced Analytics</a></li>
                            <li><a href="service-5.php">Organization</a></li>
                            <li><a href="service-6.php">Customer Insights</a></li>
                         </ul>
                            
                        </li>

                        <li><a href="project.php">Projects</a></li>

                        <li><a href="blog-grid.php">blog</a></li>
                      
                        <li><a href="contact.php">Contact us</a></li>

                        <li><a href="#" class="paddinglimenu"> <span> <img src="images/profile/user.jpeg" alt="" class="userprof"> <span> </a>
                         <ul class="dropdown margint15">
                            <li><a href="my_account.php">My Account</a></li>
                            <li><a href="#">Edit Profile</a></li>
                            <li><a href="#">Change Password</a></li>
                            <li><a href="#">Logout</a></li>
                         </ul>
                         </li>


                    </ul><!-- End of .menuzord-menu -->
                </nav> <!-- End of #main_menu -->
            </div>
            <div class="right-column">
                <div class="right-area">
                    <div class="nav_side_content">
                        <div class="search_option">
                            <button class="search tran3s dropdown-toggle color1_bg" id="searchDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search" aria-hidden="true"></i></button>
                            <form action="#" class="dropdown-menu" aria-labelledby="searchDropdown">
                                <input type="text" placeholder="Search">
                                <button><i class="fa fa-search" aria-hidden="true"></i></button>
                            </form>
                       </div>
                   </div>
                </div>
                    
            </div>
        </div>
                

   </div> <!-- End of .conatiner -->
</section>
  