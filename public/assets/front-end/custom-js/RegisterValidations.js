
function editprofilevalidation()
{
    var user_image = $.trim($("#user_image").val());
    var old_user_image = $("#old_user_image").val();
    var profession_id = $.trim($("#profession_id").val());
    var first_name = $.trim($("#first_name").val());
    var last_name = $.trim($("#last_name").val());
    var email = $.trim($("#email").val());
    var mobile = $.trim($("#mobile").val());
    var password = $.trim($("#password").val());
    var confirm_password = $.trim($("#confirm_password").val());
    var flag = 0;
    var pattern_name = /^[A-Za-z ]{0,50}$/i;
    var pattern_email = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i; 
    var pattern_mobile = /^[7-9]{1}[0-9]{9}$/i;
    var pattern_link = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/;

 
    if(profession_id!="Industry/Company")
    {
        if(first_name == "")
        {
            $("#errormsg").slideDown().html("Enter First Name");
            setTimeout(function(){$("#errormsg").slideUp();},3000)
            $("#first_name").focus();
            flag++;     
            return false;

        }
        else if(!pattern_name.test(first_name))
        {
          $("#errormsg").slideDown().html("Invalid First Name");
            setTimeout(function(){$("#errormsg").slideUp();},3000)
            $("#first_name").focus();
            flag++;     
            return false;
        }

        if(last_name == "")
        {
            $("#errormsg").slideDown().html("Enter Last Name");
            setTimeout(function(){$("#errormsg").slideUp();},3000)
            $("#last_name").focus();
            flag++;     
            return false;
        }
         else if(!pattern_name.test(last_name))
        {
          $("#errormsg").slideDown().html("Invalid Last Name");
            setTimeout(function(){$("#errormsg").slideUp();},3000)
            $("#last_name").focus();
            flag++;     
            return false;
        }
        
        if(email == "")
        {
            $("#errormsg").slideDown().html("Enter Email");
            setTimeout(function(){$("#errormsg").slideUp();},3000)
            $("#email").focus();
            flag++;     
            return false;

        }

        else if(!pattern_email.test(email))
        {
            $("#errormsg").slideDown().html("Invalid Email");
            setTimeout(function(){$("#errormsg").slideUp();},3000)
            $("#email").focus();
            flag++;
            return false;
        }

         if(mobile == "")
        {
            $("#errormsg").slideDown().html("Enter Mobile");
            setTimeout(function(){$("#errormsg").slideUp();},3000)
            $("#mobile").focus();
            flag++;     
            return false;

        }

         else if(!pattern_mobile.test(mobile))
        {
            $("#errormsg").slideDown().html("Invalid Mobile");
            setTimeout(function(){$("#errormsg").slideUp();},3000)
            $("#mobile").focus();
            flag++;
            return false;
        }

         if($.trim(user_image)==''  && $.trim(old_user_image)=='')
        {   
            $("#errormsg").slideDown().html("Select Profile Image");
            setTimeout(function(){$("#errormsg").slideUp();},3000)
            $("#user_image").focus();
            flag++;
            return false;
        }

        $("#clicksbm1").click();
      }
      else
      {
      var company_name = $.trim($("#company_name").val());
      var domain1 = $.trim($("#domain1").val());
      var domain2 = $.trim($("#domain2").val());
      var domain3 = $.trim($("#domain3").val());
      var country = $.trim($("#country_id_").val());
      var state = $.trim($("#state_id_").val());
      var district = $.trim($("#district_id_").val());
      var web_link = $.trim($("#web_link").val());
      var cmpny_email = $.trim($("#cmpny_email").val());
      var username = $.trim($("#username").val());
      var phone = $.trim($("#phone").val());
      var firstname = $.trim($("#firstname").val());
      var lastname = $.trim($("#lastname").val());
      var can_profile_image           = $("#can_profile_image").val();
      var company_tagline = $.trim($("#company_tagline").val());

         if(company_name == "")
            {
                $("#errormsg").slideDown().html("Enter Company Name");
                setTimeout(function(){$("#errormsg").slideUp();},3000)
                $("#company_name").focus();
                flag++;     
                return false;
            }

         if(domain1=="" && domain2=="" && domain3=="")
            {
                $("#errormsg").slideDown().html("Select Domain");
                setTimeout(function(){$("#errormsg").slideUp();},3000)
                $("#domain1").focus();
                flag++;     
                return false;
            }

          if(country == "")
            {
                $("#errormsg").slideDown().html("Select Country");
                setTimeout(function(){$("#errormsg").slideUp();},3000)
                $("#country_id_").focus();
                flag++;     
                return false;
            }

             if(state == "")
            {
                $("#errormsg").slideDown().html("Select State");
                setTimeout(function(){$("#errormsg").slideUp();},3000)
               $("#state_id_").focus();
                flag++;     
                return false;

            }

              if(district == "")
            {
                $("#errormsg").slideDown().html("Select District");
                setTimeout(function(){$("#errormsg").slideUp();},3000)
                $("#district_id_").focus();
                flag++;     
                return false;

            }

            if(firstname == "")
            {
                $("#errormsg").slideDown().html("Enter First Name");
                setTimeout(function(){$("#errormsg").slideUp();},3000)
                $("#firstname").focus();
                flag++;     
                return false;

            }
              else if(!pattern_name.test(firstname))
              {
                $("#errormsg").slideDown().html("Invalid First Name");
                  setTimeout(function(){$("#errormsg").slideUp();},3000)
                  $("#firstname").focus();
                  flag++;     
                  return false;
              }
            if(lastname == "")
            {
                $("#errormsg").slideDown().html("Enter Last Name");
                setTimeout(function(){$("#errormsg").slideUp();},3000)
                $("#lastname").focus();
                flag++;     
                return false;
            }

             else if(!pattern_name.test(lastname))
              {
                $("#errormsg").slideDown().html("Invalid Last Name");
                  setTimeout(function(){$("#errormsg").slideUp();},3000)
                  $("#lastname").focus();
                  flag++;     
                  return false;
              }

            if(web_link == "")
            {
                $("#errormsg").slideDown().html("Enter Company Link");
                setTimeout(function(){$("#errormsg").slideUp();},3000)
                $("#web_link").focus();
                flag++;     
                return false;
            }

             else if(!pattern_link.test(web_link))
            {
                $("#errormsg").slideDown().html("Invalid Company Link");
                setTimeout(function(){$("#errormsg").slideUp();},3000)
                $("#web_link").focus();
                flag++;     
                return false;
            }

            if(cmpny_email == "")
            {
                $("#errormsg").slideDown().html("Enter Email");
                setTimeout(function(){$("#errormsg").slideUp();},3000)
                $("#cmpny_email").focus();
                flag++;     
                return false;
            }

            else if(!pattern_email.test(cmpny_email))
            {
                $("#errormsg").slideDown().html("Invalid Email");
                setTimeout(function(){$("#errormsg").slideUp();},3000)
                $("#cmpny_email").focus();
                flag++;
                return false;
            }

            if(phone == "")
            {
                $("#errormsg").slideDown().html("Enter Phone");
                setTimeout(function(){$("#errormsg").slideUp();},3000)
                $("#phone").focus();
                flag++;     
                return false;
            }

             else if(!pattern_mobile.test(phone))
            {
                $("#errormsg").slideDown().html("Invalid Mobile");
                setTimeout(function(){$("#errormsg").slideUp();},3000)
                $("#phone").focus();
                flag++;
                return false;
            }

             if(company_tagline == "")
            {
                $("#errormsg").slideDown().html("Enter Company Tagline");
                setTimeout(function(){$("#errormsg").slideUp();},3000)
                $("#company_tagline").focus();
                flag++;     
                return false;
            }

             if($.trim(user_image)==''  && $.trim(old_user_image)=='')
            {   
              $("#errormsg").slideDown().html("Select Profile Image");
              setTimeout(function(){$("#errormsg").slideUp();},3000)
              $("#user_image").focus();
              flag++;
              return false;
            }



            $("#clicksbm1").click();

        }

}



function checkvalidation()
{
  var profession_id = $.trim($("#profession_id").val());
  var profession_name = $("#profession_name").val();
  var data_change = $("#profession_id").attr("data-change");
  var first_name = $.trim($("#first_name").val());
  var last_name = $.trim($("#last_name").val());
  var email = $.trim($("#email").val());
  var mobile = $.trim($("#mobile").val());
  var r_verifyOTP = $.trim($("#r_verifyOTP").val());
  var password = $.trim($("#password").val());
  var confirm_password = $.trim($("#confirm_password").val());
  var flag = 0;
  var pattern_name = /^[A-Za-z ]{0,50}$/i;
  var pattern_email = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i; 
  //var pattern_mobile = /^[0-9]{10,15}$/i;
  var pattern_mobile = /^[7-9]{1}[0-9]{9}$/i;
  var pattern_link = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/;

  if(profession_id=="")
  {
    $("#errormsg").slideDown().html("Select Profession");
    setTimeout(function(){$("#errormsg").slideUp();},3000)
    $("#profession_id").focus();
    flag++;     
    return false;
  }
  if(data_change!="Yes")
  {
    if(first_name == "")
    {
      $("#errormsg").slideDown().html("Enter First Name");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#first_name").focus();
      flag++;     
      return false;
    }
    else if(!pattern_name.test(first_name))
    {
      $("#errormsg").slideDown().html("Invalid First Name");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#first_name").focus();
      flag++;     
      return false;
    }
    if(last_name == "")
    {
      $("#errormsg").slideDown().html("Enter Last Name");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#last_name").focus();
      flag++;     
      return false;
    }
    else if(!pattern_name.test(last_name))
    {
      $("#errormsg").slideDown().html("Invalid Last Name");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#last_name").focus();
      flag++;     
      return false;
    }
    if(mobile == "")
    {
      $("#errormsg").slideDown().html("Enter Mobile");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#mobile").focus();
      flag++;     
      return false;
    }
    else if(!pattern_mobile.test(mobile))
    {
      $("#errormsg").slideDown().html("Invalid Mobile");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#mobile").focus();
      flag++;
      return false;
    }
    if(r_verifyOTP == "")
    {
      $("#errormsg").slideDown().html("Enter OTP");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#r_verifyOTP").focus();
      flag++;     
      return false;
    }
    if(email == "")
    {
      $("#errormsg").slideDown().html("Enter Email");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#email").focus();
      flag++;     
      return false;
    }
    else if(!pattern_email.test(email))
    {
      $("#errormsg").slideDown().html("Invalid Email");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#email").focus();
      flag++;
      return false;
    }
    if(password == "")
    {
      $("#errormsg").slideDown().html("Enter Password");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#password").focus();
      flag++;
      return false;
    }
    if(password.length < 6)
    {
      $("#errormsg").slideDown().html("Password length should be more than 6 character");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#password").focus();
      flag++;
      return false;
    }
    if(confirm_password == "")
    {
      $("#errormsg").slideDown().html("Enter Confirm Password");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#errormsg").focus();
      flag++;
      return false;
    }
    else if(confirm_password!=password)
    {
      $("#errormsg").slideDown().html("Confirm Password not matched with Password");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#errormsg").focus();
      flag++;
      return false;
    }

      $.ajax({  
        url:BASE_URL+'home/checkduplication',
        cache: false,
        type:'POST',
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
        data:{
          profession_id:profession_id,profession_name:profession_name,first_name:first_name,last_name:last_name,
          email:email,mobile:mobile,r_verifyOTP:r_verifyOTP,password:password
        },    
        success: function(response)
        {  
          var d = JSON.parse(response);

          if(d.error==2)
          {
            $("#errormsg").slideDown().html(d.message).css("color","red");
            setTimeout(function(){$("#errormsg").html("&nbsp;");},4000)
            $("#email").focus();
            return false;
          }
         
          if(d.error==0)
          {
            $("#myDiv").load(location.href + "#myDiv>*", "");
            window.location.href=BASE_URL + 'home/login';
          }

        }
    }); 
  }
  else
  {
    var company_name = $.trim($("#company_name").val());
    var domain1 = $.trim($("#domain1").val());
    var domain2 = $.trim($("#domain2").val());
    var domain3 = $.trim($("#domain3").val());
    var country = $.trim($("#country_id").val());
    var state = $.trim($("#state_id").val());
    var district = $.trim($("#district_id").val());
    var web_link = $.trim($("#web_link").val());
    var cmpny_email = $.trim($("#cmpny_email").val());
    var username = $.trim($("#username").val());
    var phone = $.trim($("#phone").val());
    var pswd = $.trim($("#pswd").val());
    var confirm_pswd = $.trim($("#confirm_pswd").val());
    var firstname = $.trim($("#firstname").val());
    var lastname = $.trim($("#lastname").val());

    if(company_name == "")
    {
      $("#errormsg").slideDown().html("Enter Company Name");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#company_name").focus();
      flag++;     
      return false;
    }
    if(domain1=="" && domain2=="" && domain3=="")
    {
      $("#errormsg").slideDown().html("Select Domain");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#domain1").focus();
      flag++;     
      return false;
    }

    if(country == "")
    {
      $("#errormsg").slideDown().html("Select Country");
      setTimeout(function(){$("#errormsg").slideUp();},3000)
      $("#country_id").focus();
      flag++;     
      return false;
    }

    if(state == "")
    {
        $("#errormsg").slideDown().html("Select State");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
       $("#state_id").focus();
        flag++;     
        return false;

    }

    if(district == "")
    {
        $("#errormsg").slideDown().html("Select District");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#district_id").focus();
        flag++;     
        return false;

    }

    if(firstname == "")
    {
        $("#errormsg").slideDown().html("Enter First Name");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#firstname").focus();
        flag++;     
        return false;

    }
    else if(!pattern_name.test(firstname))
    {
      $("#errormsg").slideDown().html("Invalid First Name");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#firstname").focus();
        flag++;     
        return false;
    }
    if(lastname == "")
    {
        $("#errormsg").slideDown().html("Enter Last Name");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#lastname").focus();
        flag++;     
        return false;
    }

    else if(!pattern_name.test(lastname))
    {
      $("#errormsg").slideDown().html("Invalid Last Name");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#lastname").focus();
        flag++;     
        return false;
    }

    if(web_link == "")
    {
        $("#errormsg").slideDown().html("Enter Company Link");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#web_link").focus();
        flag++;     
        return false;
    }

     else if(!pattern_link.test(web_link))
    {
        $("#errormsg").slideDown().html("Invalid Company Link");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#web_link").focus();
        flag++;     
        return false;
    }

    if(cmpny_email == "")
    {
        $("#errormsg").slideDown().html("Enter Email");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#cmpny_email").focus();
        flag++;     
        return false;
    }

    else if(!pattern_email.test(cmpny_email))
    {
        $("#errormsg").slideDown().html("Invalid Email");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#cmpny_email").focus();
        flag++;
        return false;
    }

    if(phone == "")
    {
        $("#errormsg").slideDown().html("Enter Phone");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#phone").focus();
        flag++;     
        return false;
    }

     else if(!pattern_mobile.test(phone))
    {
        $("#errormsg").slideDown().html("Invalid Mobile");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#phone").focus();
        flag++;
        return false;
    }
    
    if(pswd == "")
    {
        $("#errormsg").slideDown().html("Enter Password");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#pswd").focus();
        flag++;     
        return false;
    }
    if(pswd.length < 6){
      $("#errormsg").slideDown().html("Password length should be more than 6 character");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#pswd").focus();
        flag++;
        return false;
    }
    if(confirm_pswd == "")
    {
        $("#errormsg").slideDown().html("Enter Confirm Password");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#confirm_pswd").focus();
        flag++;     
        return false;
    }
    else if(confirm_pswd!=pswd)
    {
        $("#errormsg").slideDown().html("Confirm Password not matched with Password");
        setTimeout(function(){$("#errormsg").slideUp();},3000)
        $("#errormsg").focus();
        flag++;
        return false;
    }

      $.ajax({  
        url:BASE_URL+'home/checkduplication1',
        cache: false,
        type:'POST',
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
        data:{
          phone:phone,
        },    
        success: function(returndata)
        {  
          if(returndata==5)
          {
            $("#errormsg").slideDown().html("Mobile already exists ").css("color","red");
            setTimeout(function(){$("#errormsg").html("&nbsp;");},4000)
            $("#mobile").focus();
            return false;
          }
          if(returndata==2)
          {
            $("#sbtmclick").click();
          }

        }
      }); 

    }       
  }


  function isNumberKey(evt) {

    var charCode = (evt.which) ? evt.which : evt.keyCode;
    // Added to allow decimal, period, or delete
    if (charCode == 110 || charCode == 190 || charCode == 46) 
        return true;
    
    if (charCode > 31 && (charCode < 48 || charCode > 57)) 
        return false;
    
    return true;
  }



function getStates(pointer)
{
  var url = pointer.getAttribute("data-url");
  var country_id = $("#country_id").val();
   $.ajax({  
      url:url,
      cache: false,
      type:'POST',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data:{country_id:country_id},
      success: function(response)
      {
          $("#state_id").html(response);
      }
   });
}

function getStates1(pointer)
{
  var url = pointer;
  var country_id = $("#country_id").val();
   $.ajax({  
      url:url,
      cache: false,
      type:'POST',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data:{country_id:country_id},
      success: function(response)
      {
          $("#state_id").html(response);
      }
   });
}


function getDistricts(pointer)
{
  var url = pointer.getAttribute("data-url");
  var state_id = $("#state_id").val();
   $.ajax({  
      url:url,
      cache: false,
      type:'POST',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data:{state_id:state_id},
      success: function(response)
      {
          $("#district_id").html(response);
      }
   });
}


function getdomain(domain)
{
  $.ajax({  
    url:BASE_URL+'home/getprofession_name',
    cache: false,
    type:'POST',
    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    data:{
      domain:domain,
    },    
    success: function(response)
    {  
      var d = JSON.parse(response);
      
      $("#profession_name").val(d.title);

      if(d.is_industry=='Yes')
      {
        $("input").val('');
        $("#industryCompany").show();
        $(".register1").hide();
      }
      else
      {
        $("input").val('');
        $("#industryCompany").hide();
        $(".register1").show(); 
      }
    }
  }); 
}


  var register = {

    'verifyOTP': function(otp){

      if($("#mobile").val()){
        var mobile = $.trim($("#mobile").val());
      }
      else{
        var mobile = $.trim($("#phone").val());
      }
      //$("#mobile").prop("readonly", true);


      if(mobile && otp.length==6)
      {
        $.ajax({  
          url: BASE_URL + 'verify-otp',
          cache: false,
          type:'POST',
          headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
          data:{
            mobile:mobile,otp:otp
          },    
          success: function(response)
          {  
            var d = JSON.parse(response);

            if(d.error==0)
            {
              $('#errormsg').html(d.message).removeClass('text-danger').removeClass('text-success').addClass('text-success');
              //$("#r_verifyOTP").prop("disabled", true);
            }
            else
            {
              $('#errormsg').html(d.message).removeClass('text-success').removeClass('text-danger').addClass('text-danger');
            }

          },
          complete: function(){
            /*$("#mobile").prop("readonly", false);
            $("#r_verifyOTP").prop("disabled", false);*/
          }
        });
      }else{
        return false;
      }
    },
    'sendOTP': function (mobile){
      var pattern_mobile = /^[7-9]{1}[0-9]{9}$/i;
      if(mobile.length==10 && pattern_mobile.test(mobile))
      {

        $.ajax({  
          url: BASE_URL + 'send-otp',
          cache: false,
          type:'POST',
          headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
          data:{
            mobile:mobile
          },    
          success: function(response)
          {  
            var d = JSON.parse(response);

            if(d.error==0)
            {
              $("#username").val(d.mobile);
              $('#errormsg').html(d.message).removeClass('text-danger').addClass('text-success');
              $(".r_verifyOTP").focus();
            }
            else
            {
              $('#errormsg').html(d.message).removeClass('text-success').addClass('text-danger');
            }

          }
        });
      }
    }
  };

