function like(id)
{
  var unique_session_id = $("#unique_session_id").val();
  $.ajax({
        url: BASE_URL + 'home/savelike',
        type:'POST',
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
        data: {id : id, unique_session_id : unique_session_id},
        success:function(response)
        {
          alert(response);
          $("#likeappend"+id).html(response);
        }
      });

}

function taggeduser(id)
{
    $('#Taggedusers').modal('show');
    var taguse = $('#givetaggeduser'+id).html();
    $("#appendgivetageed").html(taguse);
}



function  login_validation()
{

	/*alert("hiii");return false;*/

    var site_url = $('#site_url').val();
    var username = $('#username').val();
    var password = $("#password").val().trim();
    /*var pattern_name = /^[a-z A-Z]+$/;*/
    var pattern_username = /^[0-9]{10}$/i;
   
    if($.trim(username)=="")
    {
        $("#show_error").slideDown().html("Enter Mobile ");
        setTimeout(function(){$("#show_error").slideUp();},4000);
        $("#username").focus();
        return false;   
    }
    else if(!pattern_username.test(username))
    {
        $("#show_error").slideDown().html("Enter Valid Mobile");
        setTimeout(function(){$("#show_error").slideUp();},4000);
        $("#username").focus();
        return false;
     }
    
    if($.trim(password) =='')
        {
          $("#show_error").slideDown().html("Enter Password");
          setTimeout(function(){$("#show_error").slideUp("&nbsp;");},4000)
          $("#password").focus();
          return false;       
        }
    else 
    {
        $.ajax({
              url: BASE_URL + 'home/loginaction',
              type:'POST',
              headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
              data: {username : username, password : password},
              success:function(response)
              {
                var d = JSON.parse(response);

                if(d.error==0)
                {
                    /*console.log(d);*/
                    window.location.href= BASE_URL + 'home/dashboard';
                }
                else
                {   
                    $("#show_error").slideDown().text(d.message);
                    setTimeout(function(){ $('#show_error').val(''); }, 4000);
                }               
                
              }
            });
    }
}



function addpostvalidation()
{
     var post_where = $.trim($("#post_where").val());
     var addvertise = $.trim($("#addvertise").val());
     var tag_member = $.trim($("#tag_member").val());
     var user_image = $.trim($("#user_image").val());
     var location = $.trim($("#location").val());
    
  
   if(post_where == "")
        {
            $("#errormsg").slideDown().html("Please select where to post");
            setTimeout(function(){$("#errormsg").slideUp();},3000)
           // $("#first_name").focus();
            flag++;     
            return false;

        }

        if(addvertise == "")
        {
            $("#errormsg").slideDown().html("Please Enter Message");
            setTimeout(function(){$("#errormsg").slideUp();},3000)
            $("#addvertise").focus();
            flag++;     
            return false;

        }

          if(tag_member == "")
        {
            $("#errormsg").slideDown().html("Please tag member");
            setTimeout(function(){$("#errormsg").slideUp();},3000)
          //  $("#addvertise").focus();
            flag++;     
            return false;

        }

         if($.trim(user_image)=='')
        {   
            $("#errormsg").slideDown().html("Select Image");
            setTimeout(function(){$("#errormsg").slideUp();},3000)
            $("#user_image").focus();
            flag++;
            return false;
        }
         if(location == "")
        {
            $("#errormsg").slideDown().html("Please Enter Location");
            setTimeout(function(){$("#errormsg").slideUp();},3000)
            $("#location").focus();
            flag++;     
            return false;

        }

          $("#clicksbm1").click();
}