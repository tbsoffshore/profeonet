-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `fld_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `fld_member_name` varchar(255) DEFAULT NULL,
  `fld_member_uniquie_name` varchar(255) DEFAULT NULL,
  `fld_Iam` varchar(255) DEFAULT NULL,
  `fld_first_name` varchar(255) NOT NULL,
  `fld_last_name` varchar(255) NOT NULL,
  `fld_full_name` varchar(255) NOT NULL,
  `fld_email_id` varchar(50) NOT NULL,
  `fld_email_id_2` varchar(50) DEFAULT NULL,
  `fld_OTP` varchar(255) DEFAULT NULL,
  `fld_forget_otp` varchar(224) DEFAULT NULL,
  `fld_mobile_no` bigint(20) NOT NULL,
  `fld_mobile_no_2` bigint(20) DEFAULT NULL,
  `fld_username` varchar(255) NOT NULL,
  `fld_password` varchar(255) NOT NULL,
  `fld_show_password` varchar(255) NOT NULL,
  `fld_designation` varchar(50) DEFAULT NULL,
  `fld_dob` date DEFAULT NULL,
  `fld_qulification` varchar(50) DEFAULT NULL,
  `fld_country` varchar(255) DEFAULT NULL,
  `fld_state` varchar(255) DEFAULT NULL,
  `fld_district` varchar(255) DEFAULT NULL,
  `fld_date` date DEFAULT NULL,
  `fld_time` datetime DEFAULT NULL,
  `fld_active` tinyint(4) DEFAULT '0',
  `fld_company_name` varchar(100) DEFAULT NULL,
  `fld_company_address` varchar(120) DEFAULT NULL,
  `fld_company_website` varchar(50) DEFAULT NULL,
  `fld_memberimage` varchar(1000) DEFAULT NULL,
  `fld_created` datetime DEFAULT NULL,
  `chatting_status` tinyint(4) DEFAULT '0',
  `fld_total_experience` varchar(255) DEFAULT NULL,
  `fld_personal_webside` varchar(255) DEFAULT NULL,
  `fld_count` int(11) DEFAULT NULL,
  `status` enum('1','0') DEFAULT '1',
  `fld_member_type` varchar(255) DEFAULT NULL,
  `activateportfolio_status` enum('1','0') DEFAULT '0',
  `area_of_work` text,
  `not_to_be_disclosed` varchar(255) DEFAULT NULL,
  `until_vacancy_Exists` varchar(255) DEFAULT NULL,
  `Subscription_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `verify_otp` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`fld_member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `members` (`fld_member_id`, `fld_member_name`, `fld_member_uniquie_name`, `fld_Iam`, `fld_first_name`, `fld_last_name`, `fld_full_name`, `fld_email_id`, `fld_email_id_2`, `fld_OTP`, `fld_forget_otp`, `fld_mobile_no`, `fld_mobile_no_2`, `fld_username`, `fld_password`, `fld_show_password`, `fld_designation`, `fld_dob`, `fld_qulification`, `fld_country`, `fld_state`, `fld_district`, `fld_date`, `fld_time`, `fld_active`, `fld_company_name`, `fld_company_address`, `fld_company_website`, `fld_memberimage`, `fld_created`, `chatting_status`, `fld_total_experience`, `fld_personal_webside`, `fld_count`, `status`, `fld_member_type`, `activateportfolio_status`, `area_of_work`, `not_to_be_disclosed`, `until_vacancy_Exists`, `Subscription_date`, `expiry_date`, `verify_otp`, `created_at`, `updated_at`) VALUES
(1,	'ChetanWaghmare',	'ChetanWaghmare1',	'Candidate',	'Chetan',	'Waghmare',	'Chetan Waghmare',	'chetan170989@gmail.com',	'chetan170989@gmail.com',	'',	'',	9769684737,	0,	'9769684737',	'18663f8215abb71decec2b69602aa100',	'',	'',	'1989-09-17',	'M.Tech (Communication System Engineering)',	'101',	'22',	'2715',	'2018-04-13',	'2018-04-18 22:57:40',	1,	'',	'',	'',	'Scirra1917/images/1524738966candidate.jpg',	'0000-00-00 00:00:00',	1,	'3 - 5 yrs',	'',	0,	'1',	'Candidate',	'1',	'Cognitive Radio, Wireless Communication, Microwave Engineering, Computer Communications Network, Arduino Board, Embedded Systems, GNU Radio, Antenna Design and Fabrication',	'',	'',	'2018-04-14',	'2018-10-14',	'1',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00'),
(2,	'IshaJaiswal',	'IshaJaiswal1',	'Recruiter',	'Isha',	'Jaiswal',	'Isha Jaiswal',	'styledbyisha@gmail.com',	'ishaattanish@gmail.com',	'',	'',	7768850394,	0,	'8668593388',	'aa5b9e3924a7ba38857a84af6f5a7436',	'',	'Managing Director',	'0000-00-00',	'',	'101',	'22',	'2715',	'2018-04-13',	'2018-04-19 00:09:46',	1,	'TOSS Pvt. Ltd.',	'VikasNagar, Wardha road, Nagpur , Maharashtra',	'',	'Scirra1917/images/1527931663instamojo favi.png',	'2018-06-02 14:57:43',	1,	'',	'www.profeonet.com',	0,	'1',	'Recruiter',	'1',	'',	'',	'',	'2018-04-20',	'2018-10-20',	'1',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00'),
(98,	NULL,	NULL,	'Recruiter',	'john',	'doe',	'john doe',	'recruiter@gmail.com',	NULL,	'659451',	NULL,	8275629654,	NULL,	'recruiter',	'$2y$10$X7Mx28HbDCoSWbKHSzyveO.8UuqxWHvXNZ6AfGDBVR3yLHGn0h9xK',	'recruiter',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	'1',	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	'2018-06-21 10:29:38',	'2018-06-21 10:29:48'),
(99,	NULL,	NULL,	'Candidate',	'john',	'snow',	'john snow',	'candidate@candidate.com',	NULL,	'178620',	NULL,	8275629654,	NULL,	'candidate',	'$2y$10$8YfR0T4CUCxTKEAgb5.8pucKUbEJqmT6ETke4en0LW61O.NO4aWk6',	'root',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	'1',	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	'0',	'2018-06-21 12:03:41',	'2018-06-21 12:03:41'),
(100,	NULL,	NULL,	'Candidate',	'john',	'snow',	'john snow',	'candidate@candidate.com',	NULL,	'461582',	NULL,	8275629654,	NULL,	'candidate',	'$2y$10$iLnbhIGJWTbc9EVWqOSFKeQ1QrnxdVqgCv09LbQQr4pjGKN/wZYOa',	'root',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	'1',	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	'0',	'2018-06-21 12:03:56',	'2018-06-21 12:04:14'),
(101,	NULL,	NULL,	'Candidate',	'john',	'snow',	'john snow',	'candidate@candidate.com',	NULL,	'541204',	NULL,	8275629654,	NULL,	'candidate',	'$2y$10$acOjhO7nSF8lNOmt.J.kZON/khu3BjrAsHpwtEmwvmomvvYN4mFf2',	'candidate',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	'1',	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	'0',	'2018-06-21 12:06:09',	'2018-06-21 12:06:09'),
(102,	NULL,	NULL,	'Candidate',	'john',	'snow',	'john snow',	'candidate@candidate.com',	NULL,	'896299',	NULL,	8275629654,	NULL,	'candidate',	'$2y$10$6AULauKYTuectjfkRwLN9.nx5dcRVP7XMLFR77B0QE0C9MjOA6ejO',	'candidate',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	'1',	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	'2018-06-21 12:06:17',	'2018-06-21 12:06:28'),
(103,	NULL,	NULL,	'Recruiter',	'cddc',	'cdcd',	'cddc cdcd',	'dsadas@dsa.com',	NULL,	'761410',	NULL,	7894455555,	NULL,	'acd',	'$2y$10$3yIOicT5Mrf2Qsy1Sm959e2jsyq8hdGx9RQv6TL.bP1fGNHyxaehi',	'root',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	'1',	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	'2018-06-21 12:10:41',	'2018-06-21 12:10:53'),
(104,	NULL,	NULL,	'Candidate',	'fsdf',	'dsfdsf',	'fsdf dsfdsf',	'dsad@das.com',	NULL,	'649234',	NULL,	7899999999,	NULL,	'fdsfsd',	'$2y$10$qog4XfEYPbQEHgul3l5Ux.bM53tNfWf4eC/K4La62pDJFr6j0dIWi',	'root',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	'1',	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	'2018-06-21 13:08:44',	'2018-06-21 13:09:11'),
(105,	NULL,	NULL,	'Candidate',	'sadsa',	'dasdasd',	'sadsa dasdasd',	'dsad@sds.com',	NULL,	'104733',	NULL,	7984426456,	NULL,	'sdasdasd',	'$2y$10$cIesv42Q9YslFK8H8QWXFe4ojkX1lvcGRo9OZqyITDGs4rsdFhoje',	'root',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	'1',	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	'2018-06-21 13:09:56',	'2018-06-21 13:10:09'),
(106,	NULL,	NULL,	'Candidate',	'sadsd',	'sadsdsa',	'sadsd sadsdsa',	'dsad@das.com',	NULL,	'125068',	NULL,	7777777777,	NULL,	'dasd',	'$2y$10$g4bNIPY8EENX4/7uz0/3euP2Vgg3DWpRwnqiunwWuVGv3OOTxnn2G',	'root',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	'1',	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	'2018-06-21 13:10:56',	'2018-06-21 13:11:06'),
(107,	NULL,	NULL,	'Candidate',	'dsadsad',	'dsadas',	'dsadsad dsadas',	'dsa@dsa.com',	NULL,	'521653',	NULL,	7777777777,	NULL,	'dasdsa',	'$2y$10$IjKWaU5IJLoCHabyrd1CReOlBaAjOo5gjzi6.OdPCzumEa1qy1yhW',	'root',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	'1',	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	'0',	'2018-06-21 13:12:57',	'2018-06-21 13:12:57'),
(108,	NULL,	NULL,	'Candidate',	'dsadsad',	'dsadas',	'dsadsad dsadas',	'dsa@dsa.com',	NULL,	'688954',	NULL,	7777777777,	NULL,	'dasdsa',	'$2y$10$pgVhifZDVqDTPAcfwIEaseidITsBTrfpnU3rz4vL8pJcDoxRY1d4W',	'root',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	'1',	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	'2018-06-21 13:13:07',	'2018-06-21 13:13:17'),
(109,	NULL,	NULL,	'Candidate',	'sadsadsa',	'dsdsadsa',	'sadsadsa dsdsadsa',	'dsa@dsd.com',	NULL,	'279445',	NULL,	7777777777,	NULL,	'dsadsad',	'$2y$10$r2mKUDPoqdcyUiTIAWdu6.MpzoFkxh.qYEGtb/b1eNpew2/LAq3da',	'root',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL,	'1',	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	'2018-06-21 13:51:22',	'2018-06-21 13:51:33');

-- 2018-06-21 13:59:05
