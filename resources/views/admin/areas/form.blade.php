@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                          
                            <div class="text-light-blue">
                                <span style="font-size:22px">{{ $heading }} Area of Work</span>
                            <a href="#"  onclick="window.history.back();"class="btn btn-sm btn-warning pull-right">Back</a>
                        </div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                            <form action="{{ route($action) }}" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                              <div class="row">
                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Discipline &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorDiscipline"></span>
                                         @if ($errors->has('mst_discipline_id'))
                                              <span class="text-danger sid">{{ strip_tags($errors->first('mst_discipline_id')) }}</span>
                                        @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="mst_discipline_id" id="mst_discipline_id" data-url="{{ url('admin/areas/getDomains') }}" onchange="return getDomains(this);">
                                            <option value="">--Select Discipline--</option>
                                                @foreach($discipline_list as $row)
                                                     <option value="{{ $row->id }}" {{ ($row->id == old('mst_discipline_id')) || (!empty($Areas_data) && $row->id == $Areas_data->mst_discipline_id)?'selected':'' }} >{{ $row->discipline_title }}</option>

                                                    <!-- <option value="{{ $row->id }}" <?php if($heading!='Add'){if($row->id==$Areas_data->mst_discipline_id) echo 'selected'; } ?> >{{ $row->discipline_title }}</option> -->
                                                @endforeach
                                        </select>
                                    </div>
                                 </div>
                                </div>

                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Domain &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorDomain"></span>
                                          @if ($errors->has('mst_domain_id'))
                                              <span class="text-danger sid">{{ strip_tags($errors->first('mst_domain_id')) }}</span>
                                          @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="mst_domain_id" id="mst_domain_id" data-id='{{ $heading!='Add'?$Areas_data->mst_domain_id:'' }}'>
                                            <option value="">--Select Domain--</option>        
                                        </select>
                                    </div>
                                 </div>
                                </div>
                                <input type="hidden" value="{{ $heading!='Add'?$Areas_data->mst_domain_id:'' }}" id="hide_domain_id" name="">

                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Title &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorTitle"></span>
                                         @if ($errors->has('title'))
                                            <span class="text-danger sid">{{ strip_tags($errors->first('title')) }}</span>
                                          @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <input type="text" placeholder="Title" class="form-control" @if(!empty($Areas_data)) value="{{ $Areas_data['title'] }}" @endif name="title" id="title" autocomplete="off">
                                    </div>
                                 </div>
                                </div>
                                  
                              </div>
                                  <input type="hidden" name="id" id="primary_id" @if(!empty($Areas_data)) value="{{ $Areas_data['id'] }}" @endif>
                                                                
                                <div class="box-footer">
                                    <button class="btn btn-sm btn-primary" type="submit" onclick="return areasValidation();">Submit</button><!--  -->
                                    <a href="{{ route('areas/list') }}" class="btn btn-sm btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')
 <script type="text/javascript">
 
 </script>
 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/area.js') }}"></script>
 @stop
  
