@extends('admin/welcome')
@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-header with-border">
                    <div class="text-light-blue">
                        <span style="font-size:22px"><i class="fa fa-building-o"></i>&nbsp;&nbsp;Area of Work List</span>
                                      
                    <a class="btn btn-sm btn-primary pull-right" href="{{ route('areas/create') }}" onclick="showloader();"><i class="fa fa-plus"></i>&nbsp;Create</a> 
                </div>
                        <div class="clearfix">&nbsp;</div>
                         
                     

                    <div class="col-md-12" style="text-align:center">
                        <span class="msghide"></span>
                    </div>
                    <!-- <div class="clearfix"></div>-->
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="area_table" class="table table-striped table-bordered table-hover">
                            <thead class="table-head">
                                <tr>
                                    
                                    <th>Discipline</th>
                                    <th>Domain </th>
                                    <th>Title</th>
                                    <th>Status</th>
                                     <th>Actions</th>
                                </tr>
                            </thead>                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal inmodal" id="checkStatus" data-modal-color="lightblue" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated bounceInRight">   
            <form method="post" action="{{ route($statusAction) }}">
                {{ csrf_field() }}

                <div class="modal-body" style="height: 100px;padding-top: 10%">
                    <center>
                        <input type="hidden" name="id" id="statusId" style="display: none;">
                        <span style="font-size: 16px">Are you sure to change the status?</span>
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ok</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteData" data-modal-color="lightblue" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">   
            <form method="post" action="{{ route($deleteAction) }}">
                {{ csrf_field() }}       
                <div class="modal-body" style="height: 100px;padding-top: 10%">
                    <center>
                        <input type="hidden" name="id" id="deleteId" style="display: none;">
                        <span style="font-size: 16px">Are you sure want to delete this record ?</span>
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ok</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

@stop
@section('footer_js')
<script type="text/javascript">
$(document).ready(function() {
     $('#area_table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('areas/ajaxAreasList') }}",
        "columns":[
            { "data": "discipline.discipline_title" },
            { "data": "domain.title" },
            { "data": "title" },
            { "data": 'status',"render": function(data, type, row) {
            if(row.status=='Active')
            {
                return '<a href="#checkStatus" data-toggle="modal" class="label-warning label"  onclick="checkStatus('+row.id+')"> Active </a>';
            } 
            else 
            {
                
                return '<a href="#checkStatus" data-toggle="modal" class="label-danger label"  onclick="checkStatus('+row.id+')"> Inactive </a>';   
            }
            }},
            { "data": 'action', orderable: false, searchable: false},
        ]
     });
});
</script>
@stop

