@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                           
                            
                            <div class="text-light-blue">
                                <span style="font-size:25px">{{ $heading }} Banners</span>
                            <a href="#"  onclick="window.history.back();"class="btn btn-sm btn-warning pull-right">Back</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <form action="{{ route($action)}}" method="post" class="form-horizontal" id="form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                              <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                        <div class="col-xs-12">
                                            <label for="exampleInputEmail1">Title &nbsp;<span class="required">*</span></label>&nbsp;
                                             <span class="required" id="errorTitle"></span>
                                        </div>
                                        <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                            <input type="text" placeholder="Title" class="form-control" @if(isset($baners_data)) value="{{ $baners_data['title'] }}" @endif name="title" id="title" autocomplete="off">
                                        </div>
                                        <!-- <input type="hidden" name="id" id="primary_id" @if(isset($baners_data)) value="{{ $baners_data['id'] }}" @endif> -->
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                        <div class="col-xs-12">
                                            <label for="exampleInputEmail1">Description &nbsp;</label>&nbsp;
                                             <span class="required" id="errorDescription"></span>
                                        </div>
                                        <div class="col-xs-12">
                                            <textarea type="text" placeholder="Description" class="form-control ckeditor" name="description" id="description" autocomplete="off">@if(isset($baners_data)){{ $baners_data['description'] }} @endif</textarea>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Image &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorimage"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                      <input type="file"  class="form-control" name="image" id="image" autocomplete="off" >
                                      <span style="color:blue"><b>Note :</b>Required only JPEG, JPG, PNG file is allowed.</span><br>
                                     <!-- <span style="color:red"><b>Note :</b>Recommended Height & Width (100px x 150px). </span><br> -->

                                     @if(isset($baners_data))
                                     <img src="{{ url('public/assets/admin/dist/img/baners').'/'.$baners_data->image }}" width="150px"></br>
                                     {{ $baners_data->image }}
                                     @endif


                                    </div>
                                </div>
                                </div>
                                    <input type="hidden" id="old_image" name="old_image" @if(isset($baners_data)) value="{{ $baners_data['image'] }}" @endif> 

                                    <input type="hidden" name="id" id="primary_id" @if(isset($baners_data)) value="{{ $baners_data['id'] }}" @endif> 
                              </div>                               
                                <div class="box-footer">
                                    <button class="btn btn-primary btn-sm " type="button" onclick="banerAdd()">Submit</button>
                                    <a href="{{ route('baners/list') }}" class="btn btn-sm btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')
 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/master_validations.js') }}"></script>
 @stop
  
