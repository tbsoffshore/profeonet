@extends('admin/welcome')
@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-header with-border">
                    <div class="col-md-5 text-light-blue">
                        <span style="font-size:25px"><img src="{{ asset('public/assets/admin/dist/img/candidate.png') }}">&nbsp;&nbsp;Candidates List</span>
                    </div>

                    <div class="col-md-7 text-right" style="padding: 0px;">
                    </div>  

                    <div class="col-md-12" style="text-align:center">
                        <span class="msghide"></span>
                    </div>
                    <!-- <div class="clearfix"></div>             -->
                </div>
                <div class="box-body">
                    <div class="">
                        <table id="candidate_table" class="table table-striped table-bordered table-hover">
                            <thead class="table-head">
                                <tr> 
                                    <th>Name</th>                                   
                                    <th>Email Id</th>
                                    <th>Mobile No.</th>
                                    <th>Date Of Birth</th>
                                    <th>Qualification</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('footer_js')
<script type="text/javascript">
$(document).ready(function() {
     $('#candidate_table').DataTable({
        "processing": true,
        "serverSide": true,
        
        "ajax": "{{ route('candidates/ajaxCandidatesList') }}",
        "columns":[
            { "data": "fld_first_name" },            
            { "data": "fld_email_id" },            
            { "data": "fld_mobile_no" },            
            { "data": "fld_dob" },            
            { "data": "fld_qulification" }, 
            {  "data": 'action', orderable: false, searchable: false},
        ]
     });
});
</script>
@stop

