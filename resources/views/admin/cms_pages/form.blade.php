@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                           
                            
                            <div class="text-light-blue">
                                <span style="font-size:25px">Add cms pages</span>
                                <a href="#"  onclick="window.history.back();"class="btn btn-sm btn-warning pull-right">Back</a>
                            </div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                            <form action="{{ route($action)}}" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                              <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Page Name &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorSkill"></span>
                                    </div>
                                     <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="page_name" id="page_name">
                                            <option value="about-us">About Us</option>
                                            <option value="privacy-policy">Privacy Policy</option>
                                            <option value="terms-and-conditions">Terms And Conditions</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="id" id="primary_id" @if(isset($cms_data)) value="{{ $cms_data->id }}" @endif>
                                 </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                        <div class="col-xs-12">
                                            <label for="exampleInputEmail1">Title &nbsp;<span class="required">*</span></label>&nbsp;
                                             <span class="required" id="errorSkill"></span>
                                        </div>
                                        <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                            <input type="text" placeholder="Title" class="form-control" @if(isset($cms_data)) value="{{ $cms_data['title'] }}" @endif name="title" id="title" autocomplete="off">
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                        <div class="col-xs-12">
                                             <label for="exampleInputEmail1">Descriptions &nbsp;<span class="required">*</span></label>&nbsp;
                                             <span class="required" id="errorSkill"></span>
                                        </div>
                                        <div class="col-xs-12">
                                            <textarea placeholder="Descriptions" class="form-control ckeditor" name="description" id="description" autocomplete="off">@if(isset($cms_data)) {{ $cms_data['description'] }} @endif</textarea> 
                                        </div>
                                    </div>
                                </div>
                                  
                              </div>                          
                              <div class="box-footer">
                                    <button class="btn btn-sm btn-primary" type="submit" onclick="return cms_pagesAdd();">Submit</button>
                                    <a href="{{ route('cms_pages/list') }}" class="btn btn-sm btn-default" type="button" onclick="showloader();">Cancel</a>
                              </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')
 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/master_validations.js') }}"></script>
 @stop
  
