@extends('admin/welcome')
@section('content')
<div class="content-wrapper">
        
        <section class="content">
        <div class="row">
            <div class="col-lg-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <div class="col-md-6 text-light-green">
                         <h4><b>View Details</b></h4>
                         </div>
                         <div class="col-md-6">
                            <a href="{{ route('cms_pages/list') }}" class="btn btn-sm btn-warning pull-right">Back</a>
                         </div>  
                     </div>   
                     <div class="box-header with-border">
                    <div class="col-md-12 text-light-blue">
                        <div class="form-group">
                             <label>Page Name : </label> 
                             <span>{{ $cms_data['page_name'] }}</span>  
                        </div>
                    </div>
                    </div>

                     <div class="box-header with-border">
                      <div  class="col-md-3">
                        <div class="form-group"> 
                          <label>Title : </label>  
                        {{ $cms_data['title'] }}
                        </div>
                    </div>
                    <div  class="col-md-9">
                        <div class="form-group"> 
                          <label>Description : </label>  
                        {{ strip_tags($cms_data['description']) }}
                        </div>
                    </div>
                 </div>
                  
           
        </div>
    </div>
</section>
    
</div>
@stop


