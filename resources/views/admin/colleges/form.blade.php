@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                          
                            <div class="text-light-blue">
                                <span style="font-size:22px">{{ $heading }} Colleges</span>
                                <a href="#"  onclick="window.history.back();"class="btn btn-sm btn-warning pull-right">Back</a>
                            </div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                            <form action="{{ route($action) }}" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                              <div class="row">
                                
                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Select Discpline &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorDiscpline"></span>
                                        @if ($errors->has('mst_discipline_id'))
                                              <span class="text-danger sid">{{ strip_tags($errors->first('mst_discipline_id')) }}</span>
                                        @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="mst_discipline_id" id="mst_discipline_id" data-url="{{ url('admin/colleges/getDomain') }}" onchange="return getDomain(this);">
                                            <option value="">--Select Discpline--</option>
                                                @foreach($discpline_list as $row)
                                                     <option value="{{ $row->id }}" {{ ($row->id == old('mst_discipline_id')) || (!empty($Colleges_data) && $row->id == $Colleges_data->mst_discipline_id)?'selected':'' }} >{{ $row->discipline_title }}</option>

                                                    <!-- <option value="{{ $row->id }}" <?php if($heading!='Add'){if($row->id==$Colleges_data->mst_discipline_id) echo 'selected'; } ?> >{{ $row->discipline_title }}</option> -->
                                                @endforeach
                                        </select>
                                    </div>
                                 </div>
                                </div>
                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Select Domain &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorDomain"></span>
                                         @if ($errors->has('mst_domain_id'))
                                              <span class="text-danger sid">{{ strip_tags($errors->first('mst_domain_id')) }}</span>
                                          @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="mst_domain_id" id="mst_domain_id" data-id='{{ $heading!='Add'?$Colleges_data->mst_domain_id:'' }}'>
                                            <option value="">--Select Domain--</option>
                                                
                                        </select>
                                    </div>
                                 </div>
                                </div>

                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Title &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorCollege"></span>
                                          @if ($errors->has('title'))
                                            <span class="text-danger sid">{{ strip_tags($errors->first('title')) }}</span>
                                          @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <input type="text" placeholder="College Name" class="form-control" @if(!empty($Colleges_data)) value="{{ $Colleges_data['title'] }}" @endif name="title" id="title" value="" autocomplete="off">
                                    </div>
                                 </div>
                                </div>
                                  
                              </div>
                                  <input type="hidden" name="id" id="primary_id" @if(!empty($Colleges_data)) value="{{ $Colleges_data['id'] }}" @endif>
                                                                
                                <div class="box-footer">
                                    <button class="btn btn-sm btn-primary" type="submit" onclick="return collegeAdd();">Submit</button>
                                    <a href="{{ route('colleges/list') }}" class="btn btn-sm btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')

 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/master_validations.js') }}"></script>

 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/colleges.js') }}"></script>
 
 @stop
  
