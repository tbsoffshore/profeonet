@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                          
                            <div class="text-light-blue">
                                <span style="font-size:22px">{{ $heading }} Connections</span>
                            <a href="#"  onclick="window.history.back();"class="btn btn-sm btn-warning pull-right">Back</a>
                        </div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                            <form action="{{ route($action) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                              <div class="row">
                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Discipline &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorDiscipline"></span>
                                         @if ($errors->has('mst_discipline_id'))
                                              <span class="text-danger sid">{{ strip_tags($errors->first('mst_discipline_id')) }}</span>
                                          @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="mst_discipline_id" id="mst_discipline_id" data-url="{{ url('admin/connection/getDomains') }}" onchange="return getDomains(this);">
                                            <option value="">--Select Discipline--</option>
                                                @foreach($discipline_list as $row)
                                                    <option value="{{ $row->id }}" {{ ($row->id == old('mst_discipline_id')) || (!empty($Connections_data) && $row->id == $Connections_data->mst_discipline_id)?'selected':'' }} >{{ $row->discipline_title }}</option>
                                                @endforeach
                                                <!-- <option>{{ old('mst_discipline_id') }}</option> -->
                                        </select>
                                    </div>
                                 </div>
                                </div>

                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Domain &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorDomain"></span>
                                          @if ($errors->has('mst_domain_id'))
                                              <span class="text-danger sid">{{ strip_tags($errors->first('mst_domain_id')) }}</span>
                                          @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="mst_domain_id" id="mst_domain_id" data-url="{{ url('admin/connection/getAreaworks') }}" onchange="return getAreaworks(this)" data-id='{{ $heading!='Add'?$Connections_data->mst_domain_id:'' }}' >
                                            <option value="">--Select Domain--</option>        
                                        </select>
                                    </div>
                                 </div>
                                </div>
                                <input type="hidden" value="{{ $heading!='Add'?$Connections_data->mst_domain_id:'' }}" id="hide_domain_id" name="">


                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Area Of work &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorArea"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="area_of_work_id" id="area_of_work_id">
                                            <option value="">--Select Area of work--</option>
                                               <!--  @foreach($area_data as $row)
                                                    <option value="{{ $row->id }}" {{ $heading!='Add' && $row->id==$Connections_data->area_of_work_id?'selected':'' }}> {{ $row->title }} </option>
                                                @endforeach -->
                                        </select>
                                    </div>
                                 </div>
                                </div>
                                <input type="hidden" value="{{ $heading!='Add'?$Connections_data->area_of_work_id:'' }}" id="hide_area_of_work_id" name="">

                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Colleges &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorColleges"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="colleges_id" id="colleges_id">
                                            <option value="">--Select Colleges--</option>
                                                <!-- @foreach($college_data as $row)
                                                    <option value="{{ $row->id }}" {{ $heading!='Add' && $row->id==$Connections_data->colleges_id?'selected':'' }}> {{ $row->title }} </option>
                                                @endforeach -->
                                        </select>
                                    </div>
                                 </div>
                                </div>
                                <input type="hidden" value="{{ $heading!='Add'?$Connections_data->colleges_id:'' }}" id="hide_colleges_id" name="">

                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Select State &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorCountry"></span>
                                         @if ($errors->has('states_id'))
                                              <span class="text-danger sid">{{ strip_tags($errors->first('states_id')) }}</span>
                                        @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="states_id" id="states_id">
                                            <option value="">--Select State--</option>
                                                @foreach($state_data as $row)
                                                    <option value="{{ $row->id }}" {{ ($row->id == old('states_id')) || (!empty($Connections_data) && $row->id == $Connections_data->states_id)?'selected':'' }} >{{ $row->state_name }}</option>

                                                    <!-- <option value="{{ $row->id }}" {{ $heading!='Add' && $row->id==$Connections_data->states_id?'selected':'' }}> {{ $row->state_name }} </option> -->
                                                @endforeach
                                        </select>
                                    </div>
                                 </div>
                                </div>
                                <div class="col-xs-12">
                                <div class="col-xs-12 form-group">
                                        <div class="col-xs-12">
                                          <label for="exampleInputEmail1">Description&nbsp;<span class="required">*</span></label>&nbsp;
                                          <span class="required" id="errorDescription"></span>
                                        </div>
                                        <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                            <textarea type="text" name="description" id="description"  placeholder="Description" class="form-control ckeditor">@if((!empty($Connections_data))) {{ $Connections_data['description'] }} @endif</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Image &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errimage"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                      <input type="file"  class="form-control" name="image" id="image" autocomplete="off" accept="image/*">
                                      <span style="color:blue"><b>Note :</b>Required only JPEG, JPG, PNG file is allowed.</span><br>
                                     <!-- <span style="color:red"><b>Note :</b>Recommended Height & Width (100px x 150px). </span><br> -->

                                     @if(!empty($Connections_data))
                                     <img src="{{ url('public/assets/admin/dist/img/connections').'/'.$Connections_data->image }}" width="150px"></br>
                                     {{ $Connections_data->image }}
                                     @endif


                                    </div>
                                </div>
                                </div>
                                    <input type="hidden" id="old_image" name="old_image" @if(!empty($Connections_data)) value="{{ $Connections_data['image'] }}" @endif> 
                                  
                              </div>
                                  <input type="hidden" name="id" id="primary_id" @if(!empty($Connections_data)) value="{{ $Connections_data['id'] }}" @endif>
                                                                
                                <div class="box-footer">
                                    <button class="btn btn-sm btn-primary" type="submit" onclick="return connectionValidation();">Submit</button>
                                    <a href="{{ route('connections/list') }}" class="btn btn-sm btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')
 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/connection.js') }}"></script>
 @stop
  
