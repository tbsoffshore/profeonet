@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                           
                            
                            <div class="text-light-blue">
                                <span style="font-size:25px">{{ $heading }} Country</span>
                                <a href="#"  onclick="window.history.back();"class="btn btn-sm btn-warning pull-right">Back</a>
                            </div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                            <form action="{{ route($action)}}" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                              <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Country Name &nbsp;
                                         @if ($errors->has('country_name'))
                                         <span class="text-danger sid">{{ strip_tags($errors->first('country_name')) }}</span>
                                         @endif
                                        <span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorCountry"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <input type="text" placeholder="Country Name" class="form-control" @if(isset($country_data)) value="{{ $country_data['country_name'] }}" @endif name="country_name" id="country_name" autocomplete="off">
                                    </div>
                                    <input type="hidden" name="id" id="primary_id" @if(isset($country_data)) value="{{ $country_data['id'] }}" @endif>
                                </div>
                                </div> 
                              </div>                               
                                <div class="box-footer">
                                    <button class="btn btn-sm btn-primary" type="submit" onclick="return countryAdd();">Submit</button>
                                    <a href="{{ route('countries/list') }}" class="btn btn-sm btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')
<!--  <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/master_validations.js') }}"></script> -->
 @stop
  
