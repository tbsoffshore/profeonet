@extends('admin/welcome')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/admin/custom-css/dashboard.css')}}">
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
             <a href="{{ route('profession/list')}}" style="color:#fff;"><h4>0</h4> 
               <p>Total Professions</p></a>
           
            </div>
       
             <div class="icon">
               
               <i class="fa fa-user" aria-hidden="true"></i>

            </div> 
            <a href="{{ route('profession/list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <a href="{{ route('domain/list')}}" style="color:#fff;"><h4>0</h4>

              <p>Total Domains</p></a>
            </div>
            <div class="icon">
              <i class="fa fa-user" aria-hidden="true"></i>

            </div>
             <a href="{{ route('domain/list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      

    </section>
    <!-- /.content -->
  </div>
  @stop