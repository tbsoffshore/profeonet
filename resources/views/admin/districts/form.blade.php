@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                          
                            <div class="text-light-blue">
                            <span style="font-size:25px">{{ $heading }} District</span>
                            <a href="#"  onclick="window.history.back();"class="btn btn-sm btn-warning pull-right">Back</a>
                           </div>
                        <div class="clearfix"></div>   
            
                        <div class="box-body">
                            <form action="{{ route($action) }}" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                              <div class="row">
                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Select Country &nbsp;<span class="required">*</span></label>&nbsp;
                                         @if ($errors->has('country_id'))
                                         <span class="text-danger sid">{{ strip_tags($errors->first('country_id')) }}</span>
                                         @endif
                                         <span class="required" id="errorCountry"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="country_id" id="country_id" data-url="{{ url('admin/districts/getStates') }}" onchange="return getStates(this);">
                                            <option value="">--Select Country--</option>
                                              

                                                  @foreach($country_list as $row)
                                                   <option value="{{ $row->id }}" {{ ($row->id == old('country_id')) || 
                                                        (!empty($Districts_data) &&  $row->id == $Districts_data->country_id) ?'selected':'' }} >{{ $row->country_name }}
                                                    </option>
                                                @endforeach


                                        </select>
                                    </div>
                                 </div>
                                </div>

                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Select State &nbsp;<span class="required">*</span></label>&nbsp;
                                         @if ($errors->has('state_id'))
                                         <span class="text-danger sid">{{ strip_tags($errors->first('state_id')) }}</span>
                                         @endif
                                         <span class="required" id="errorState"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="state_id" id="state_id" data-id='{{ $heading!='Add'?$Districts_data->state_id:'' }}'>
                                            <option value="">--Select State--</option>
                                                
                                        </select>
                                     <!--    {{old('state_id')}} -->
                                     @if(Input::old('state_id'))
                                      
                                        <input type="hidden" value="{{old('state_id')}}" name="stateid" id="stateid">
                                           
                                     @endif
                                    </div>
                                    <input type="hidden" id="hide_state_id" value="{{ $heading!='Add'?$Districts_data->state_id:'' }}">
                                 </div>
                                </div>

                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">District Name &nbsp;<span class="required">*</span></label>&nbsp;
                                         @if ($errors->has('district_name'))
                                            <span class="text-danger sid">{{ strip_tags($errors->first('district_name')) }}</span>
                                         @endif
                                         <span class="required" id="errorDistrict"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <input type="text" placeholder="District Name" class="form-control"
                                        @if(isset($Districts_data)) value="{{ $Districts_data['district_name'] }}" @endif name="district_name" id="district_name" autocomplete="off">
                                    </div>
                                 </div>
                                </div>
                                  
                              </div>
                                  <input type="hidden" name="id" id="primary_id" @if(isset($Districts_data)) value="{{ $Districts_data['id'] }}" @endif>
                                                                
                                <div class="box-footer">
                                    <button class="btn btn-sm btn-primary" type="submit" onclick="return districtAdd();">Submit</button>
                                    <a href="{{ route('districts/list') }}" class="btn btn-sm btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')

<script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/master_validations.js') }}"></script>

 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/district.js') }}"></script>
 
 <script type="text/javascript">
     $(document).ready(function(){
       var state_id = $("#stateid").val();

        var url = BASE_URL+'admin/districts/getStates';
 /* alert(url);return false;*/
      var country_id = $("#country_id").val();
     var hide_state_id = $("#hide_state_id").val();

   $.ajax({  
      url:url,
      cache: false,
      type:'POST',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data:{country_id:country_id,state_id:state_id},
      success: function(response)
      {
          //alert(response);return false;
          $("#state_id").html(response);
      }
   });
      
    });
});
 </script>

 @stop
  
