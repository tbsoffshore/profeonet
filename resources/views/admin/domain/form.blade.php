@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                           
                            
                            <div class="text-light-blue">
                                <span style="font-size:25px">{{ $heading }} Domain</span>
                                <a href="#"  onclick="window.history.back();"class="btn btn-sm btn-warning pull-right">Back</a>
                            </div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                            <form action="{{ route($action)}}" method="post" class="form-horizontal" id="form">
                            {{ csrf_field() }}
                              <div class="row">

                                 <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Select Discipline &nbsp;<span class="required">*</span></label>&nbsp;
                                         @if ($errors->has('mst_discipline_id'))
                                            <span class="text-danger sid">{{ strip_tags($errors->first('mst_discipline_id')) }}</span>
                                         @endif
                                         <span class="required" id="errorDiscipline"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="mst_discipline_id" id="mst_discipline_id">
                                            <option value="">--Select Discipline--</option>
                                                @foreach($discipline_list as $row)
                                                    <option value="{{ $row->id }}" <?php if($heading!='Add'){if($row->id==$domain_data->mst_discipline_id) echo 'selected'; } ?>> {{ $row->discipline_title }} </option>
                                                @endforeach

                                                <!--   @foreach($discipline_list as $row)
                                                   <option value="{{ $row->id }}" {{ ($row->id == old('mst_discipline_id')) || (!empty($domain_data) &&  $row->id == $domain_data->mst_discipline_id) ?'selected':'' }} >{{ $row->discipline_title }}
                                                    </option>
                                                @endforeach -->
                                             
                                        </select>
                                    </div>
                                 </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Title &nbsp;<span class="required">*</span></label>&nbsp;
                                         @if ($errors->has('title'))
                                            <span class="text-danger sid">{{ strip_tags($errors->first('title')) }}</span>
                                         @endif
                                         <span class="required" id="errorTitle"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <input type="text" placeholder="Title" class="form-control" @if(isset($domain_data)) value="{{ $domain_data['title'] }}" @endif name="title" id="title" autocomplete="off">
                                    </div>
                                    <input type="hidden" name="id" id="primary_id" @if(isset($domain_data)) value="{{ $domain_data['id'] }}" @endif>
                                </div>
                                </div> 
                              </div>                               
                                <div class="box-footer">
                                    
                                    <button class="btn btn-sm btn-primary" type="submit" onclick="return domainAdd()">Submit</button>
                                    <a href="{{ route('domain/list') }}" class="btn btn-sm btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')
 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/master_validations.js') }}"></script>
 @stop
  
