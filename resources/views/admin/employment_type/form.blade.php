@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                           
                            
                            <div class="col-md-4 text-light-blue">
                                <span style="font-size:25px">{{ $heading }} Employment Types</span>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4"><span class="required" style="float:right;">* Fields required</span></div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                            <form action="{{ route($action)}}" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                              <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Employment Type &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorEmployment_type"></span>
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="text" placeholder="Employment Type" class="form-control" @if(isset($emp_type_data)) value="{{ $emp_type_data['title'] }}" @endif name="title" id="title" autocomplete="off">
                                    </div>
                                    <input type="hidden" name="id" id="primary_id" @if(isset($emp_type_data)) value="{{ $emp_type_data['id'] }}" @endif>
                                </div>
                                </div>
                                  
                              </div>
                                
                                                                
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit" onclick="return employment_typeAdd();">Submit</button>
                                    <a href="{{ route('employment_types/list') }}" class="btn btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')
 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/master_validations.js') }}"></script>
 @stop
  
