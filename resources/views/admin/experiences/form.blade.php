@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                           
                            
                            <div class="col-md-4 text-light-blue">
                                <span style="font-size:25px">{{ $heading }} Experiences</span>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4"><span class="required" style="float:right;">* Fields required</span></div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                            <form action="{{ route($action)}}" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                              <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Experience Title &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorExperience"></span>
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="text" placeholder="Experience Title" class="form-control" @if(isset($experience_data)) value="{{ $experience_data['title'] }}" @endif name="title" id="title" autocomplete="off">
                                    </div>
                                    <input type="hidden" name="id" id="primary_id" @if(isset($experience_data)) value="{{ $experience_data['id'] }}" @endif>
                                </div>
                                </div>
                                  
                              </div>
                                
                                                                
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit" onclick="return experienceAdd();">Submit</button>
                                    <a href="{{ route('experiences/list') }}" class="btn btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')
 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/master_validations.js') }}"></script>
 @stop
  
