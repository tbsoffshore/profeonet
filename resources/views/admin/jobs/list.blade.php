@extends('admin/welcome')
@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-header with-border">
                    <div class="col-md-5 text-light-blue">
                        <span style="font-size:25px"><i class="fa fa-fw fa-child"></i>&nbsp;Jobs Approval List</span>
                    </div>

                    <div class="col-md-7 text-right" style="padding: 0px;">
                        <div>               
                            <a class="btn btn-primary" title="Assign" onclick="approveJobs()" onclick="showloader();"><i class="fa fa-fw fa-check-square-o"></i>&nbsp;Approve</a>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                         
                    </div>  

                    <div class="col-md-12" style="text-align:center">
                        <span class="msghide"></span>
                    </div>
                    <!-- <div class="clearfix"></div>             -->
                </div>
                <div class="box-body">
                    <div class="">
                        <table id="jobs_table" class="table table-striped table-bordered table-hover">
                            <thead class="table-head">
                                <tr>
                                    <th>Job Title</th>                                   
                                    <th>Company Name</th>
                                    <th>Salary</th>
                                    <th style="width:60px;"><input type="checkbox" id="selectall">&nbsp;&nbsp;Select</th>
                                    <th>Action</th>
                                </tr>
                            </thead>                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('footer_js')
<script type="text/javascript">
$(document).ready(function() {
     $('#jobs_table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('jobs/ajaxJobsList') }}",
        "columns":[
            { "data": "fld_job_title" },
            { "data": "fld_company_name" },
            { "data": "fld_salary_from" },
            {  "data": 'link',"render": function(data, type, row) {
        return '<input type="checkbox" name="approve_jobs[]" onclick="getJobs('+row.fld_id+')" class="case check_box"'+row.fld_id+'" value="'+row.fld_id+'">';
              
    },orderable: false, searchable: false},
            { "data": "action" },
          
        ]
     });
});
</script>
@stop

