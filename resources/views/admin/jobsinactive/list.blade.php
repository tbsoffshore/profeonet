@extends('admin/welcome')
@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-header with-border">
                    <div class="col-md-5 text-light-blue">
                        <span style="font-size:25px"><i class="fa fa-fw fa-child"></i>&nbsp;Jobs Not Approval List</span>
                    </div>

                    <div class="col-md-7 text-right" style="padding: 0px;">
                        <div>               
                            <a class="btn btn-primary" title="Assign" onclick="approveJobs()" onclick="showloader();"><i class="fa fa-fw fa-check-square-o"></i>&nbsp;Approve</a>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                         
                    </div>  

                    <div class="col-md-12" style="text-align:center">
                        <span class="msghide"></span>
                    </div>
                    <!-- <div class="clearfix"></div>             -->
                </div>
                
                <div class="box-body">
                    <div class="">
                        <table id="jobs_table" class="table table-striped table-bordered table-hover">
                            <thead class="table-head">
                                <tr>
                                    <th>Sr No</th>                                   
                                    <th>Job Title</th>                                   
                                    <th>Company Name</th>
                                    <th>Salary</th>
                                    <th style="width:60px;"><input type="checkbox" id="selectall">&nbsp;&nbsp;Select</th>
                                    <th>Action</th>
                                </tr>
                            </thead>                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="approveJobsModel" data-modal-color="lightblue" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">   
            <form method="post" action="{{ url('admin/jobsinactive/approveJobs') }}">
                {{ csrf_field() }}       
                <div class="modal-body" style="height: 100px;padding-top: 10%">
                    <center>
                        <input type="hidden" name="selected_jobs" class="selected_jobs" id="selected_jobs" value=""/>
                        <span style="font-size: 16px">Are you sure want to approve this record ?</span>
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ok</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

@stop
@section('footer_js')
<script type="text/javascript">
$(document).ready(function() {
     $('#jobs_table').DataTable({
        "lengthChange": false,
        "filter": true,
        "filter": false,
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('jobsinactive/ajaxJobsInactiveList') }}",
        "columns":[
            { data: null, "sortable": false },
            { data: "fld_job_title" },
            { data: "fld_company_name" },
            { data: "fld_salary_from" },
            {  data: 'link',"render": function(data, type, row) {
        return '<input type="checkbox" name="approve_jobs[]" onclick="getJobs('+row.fld_id+')" class="case check_box"'+row.fld_id+'" value="'+row.fld_id+'">';
              
    },orderable: false, searchable: false},
            { "data": "action" },
          
        ],
    


     });

});
</script>
<script type="text/javascript">
$('#selectall').click(function(e){
     var checkvalue = $("#selectall").is(':checked');
            var allChecked = $("#getAllselected").val();

            var status = this.checked;
            $(".case").each( function() {
                $(this).prop("checked",status);
            });

            if(checkvalue==false)
            {
                $(".selected_all").val('');                
            }
            else
            {                
                $(".selected_all").val(allChecked);
            }

});


function approveJobs(){
    var arr = [];
    $('input.case:checkbox:checked').each(function () {
        arr.push($(this).val());
    });
    $('input.selected_jobs').val(arr);
    if(arr!=''){
        $("#approveJobsModel").modal('show');        
    }
}
</script>
@stop

