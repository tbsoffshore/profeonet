@extends('admin/welcome')
@section('content')
<div class="content-wrapper">
        
        <section class="content">
        <div class="row">
            <div class="col-lg-12">
                 <div class="box">
                    <div class="box-header with-border">
                          <div class="col-md-6 text-light-green">
                         <h4><b>View Details Of Job</b></h4>
                         </div>
                         <div class="col-md-6">
                            <a href="{{ route('jobs/list') }}" class="btn btn-sm btn-warning pull-right">Back</a>
                         </div>  
                     </div>   
                     <div class="box-header with-border">
                    <div class="col-md-12 text-light-blue">
                        <div class="form-group">
                             <label>Job Title : </label> 
                             <span>{{ $jobs_data['fld_job_title'] }}</span>  
                        </div>
                    </div>
                    </div>
                    <div class="box-header with-border">
                    <div class="col-md-12 text-light-blue">
                      <div class="form-group">  
                        <label>Company Name : </label>  
                        <span> {{ $jobs_data['fld_company_name'] }}</span>  
                      </div>  
                    </div>
                    </div> 

                     <div class="box-header with-border">
                    <div  class="col-md-3">
                        <div class="form-group"> 
                          <label>Country : </label>  
                        {{ $jobs_data['fld_country_id'] }}
                        </div>
                    </div>
                    <div  class="col-md-3">
                        <div class="form-group"> 
                          <label>State : </label>  
                        {{ $jobs_data['fld_state_id'] }}
                        </div>
                    </div>
                    <div  class="col-md-3">
                        <div class="form-group"> 
                          <label>District : </label>  
                        {{ $jobs_data['fld_district_id'] }}
                        </div>
                    </div>
                     <div  class="col-md-3">
                        <div class="form-group"> 
                          <label>Salary : </label>  
                        {{ $jobs_data['fld_salary_to']." - ".$jobs_data['fld_salary_from'] }}
                        </div>
                    </div>
                    <div  class="col-md-3">
                        <div class="form-group"> 
                          <label>Skills : </label>  
                        Java,PHP,C sharp .Net
                        </div>
                    </div>
                    <div  class="col-md-3">
                        <div class="form-group"> 
                          <label>Qualifications : </label>  
                        Post Graduate 
                        </div>
                    </div>
                     <div  class="col-md-3">
                        <div class="form-group"> 
                          <label>Jobs view count : </label>  
                       10 
                        </div>
                    </div>
                    <div  class="col-md-3">
                        <div class="form-group"> 
                          <label>Jobs posted by : </label>  
                       {{ $jobs_data['fld_company_name'] }}
                        </div>
                    </div>

                     </div>
                      <div class="box-header with-border">
                    <div class="col-md-12 text-light-blue">
                      <div class="form-group">  
                        <label>Job Description : </label>  
                        <span> {!! $jobs_data['fld_job_description'] !!}</span>  
                      </div>  
                    </div>
                    </div> 
                  
           
        </div>
    </div>
</section>
    
</div>
@stop


