@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                           
                            
                            <div class="text-light-blue">
                                <span style="font-size:25px">{{ $heading }} Notification Categories</span>
                                <a href="#"  onclick="window.history.back();"class="btn btn-sm btn-warning pull-right">Back</a>
                           </div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                            <form action="{{ route($action)}}" method="post" class="form-horizontal" id="form">
                            {{ csrf_field() }}
                              <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Title &nbsp;<span class="required">*</span></label>&nbsp;
                                         @if ($errors->has('title'))
                                            <span class="text-danger sid">{{ strip_tags($errors->first('title')) }}</span>
                                         @endif
                                         <span class="required" id="errorTitle"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4" >
                                        <input type="text" placeholder="Title" class="form-control" @if(isset($notification_data)) value="{{ $notification_data['title'] }}" @endif name="title" id="title" autocomplete="off">
                                    </div>
                                    <input type="hidden" name="id" id="primary_id" @if(isset($notification_data)) value="{{ $notification_data['id'] }}" @endif>
                                </div>
                                </div> 
                              </div>                               
                                <div class="box-footer">
                                    <button class="btn btn-sm btn-primary" type="submit" onclick="return notificationAdd()">Submit</button>
                                    <a href="{{ route('notification/list') }}" class="btn btn-sm btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')
 
 <script type="text/javascript">
     function notificationAdd()
     {
        var title = $.trim($("#title").val());
        var id = $.trim($("#primary_id").val());
      

    if(title == "")
    {

        $("#errorTitle").fadeIn().html("Required");

        setTimeout(function(){$("#errorTitle").html("&nbsp;");},3000)

        $("#title").focus();

        
        return false;
    }

   /* else
    {
        var datastring = "title="+title+"&id="+id;
    //alert(datastring);return false;
       $.ajax({
                type:'POST',
                url: BASE_URL + 'admin/discipline/check_duplication',
                data:datastring,
                cache:false,
                success:function(response){
                    //alert(response);return false;
                d = JSON.parse(response);

                if(d.error==1)
                {
                    $("#errorTitle").fadeIn().html(d.message);
                    setTimeout(function(){$("#errorTitle").html("&nbsp;");},3000)
                    $("#title").focus();
                    return false;       
                }
                else
                {
                    
                    $("#form").submit();
                    return false;
                    //window.location.href=BASE_URL + 'admin/baners/list';
                }
            }   
        });
    }*/
     }
 </script>
 @stop
  
