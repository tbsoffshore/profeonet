@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                          
                            <div class="text-light-blue">
                                <span style="font-size:22px">{{ $heading }} Notifications</span>
                                <a href="#"  onclick="window.history.back();"class="btn btn-sm btn-warning pull-right">Back</a>
                            </div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                        <form action="{{ route($action) }}" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                              <div class="row">
                                
                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Select Notification Category &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorDiscpline"></span>
                                        @if ($errors->has('mst_discipline_id'))
                                              <span class="text-danger sid">{{ strip_tags($errors->first('mst_discipline_id')) }}</span>
                                        @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="mst_notification_category_id" id="mst_notification_category_id" >
                                            <option value="">--Select Notification Category--</option>
                                                @foreach($ncategory_list as $row)
                                                     <option value="{{ $row->id }}" {{ ($row->id == old('mst_notification_category_id')) || (!empty($Notifications_data) && $row->id == $Notifications_data->mst_notification_category_id)?'selected':'' }} >{{ $row->title }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                 </div>
                                </div>

                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Select Scope&nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorDiscpline"></span>
                                        @if ($errors->has('mst_discipline_id'))
                                              <span class="text-danger sid">{{ strip_tags($errors->first('mst_discipline_id')) }}</span>
                                        @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="scope_id" id="scope_id" >
                                            <option value="">--Select Scope--</option>
                                                @foreach($scope_list as $row)
                                                     <option value="{{ $row->id }}" {{ ($row->id == old('scope_id')) || (!empty($Notifications_data) && $row->id == $Notifications_data->scope_id)?'selected':'' }} >{{ $row->title }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                 </div>
                               </div>

                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Select Discpline &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorDiscpline"></span>
                                        @if ($errors->has('mst_discipline_id'))
                                              <span class="text-danger sid">{{ strip_tags($errors->first('mst_discipline_id')) }}</span>
                                        @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="mst_discipline_id" id="mst_discipline_id" data-url="{{ url('admin/colleges/getDomain') }}" onchange="return getDomain(this);">
                                            <option value="">--Select Discpline--</option>
                                                @foreach($discpline_list as $row)
                                                     <option value="{{ $row->id }}" {{ ($row->id == old('mst_discipline_id')) || (!empty($Notifications_data) && $row->id == $Notifications_data->mst_discipline_id)?'selected':'' }} >{{ $row->discipline_title }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                 </div>
                                </div>
                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Select Domain &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorDomain"></span>
                                         @if ($errors->has('mst_domain_id'))
                                              <span class="text-danger sid">{{ strip_tags($errors->first('mst_domain_id')) }}</span>
                                          @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="mst_domain_id" id="mst_domain_id" data-id='{{ $heading!='Add'?$Colleges_data->mst_domain_id:'' }}'>
                                            <option value="">--Select Domain--</option>
                                                
                                        </select>
                                    </div>
                                 </div>
                                </div>

                                 <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Select Country &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorDiscpline"></span>
                                        @if ($errors->has('mst_discipline_id'))
                                              <span class="text-danger sid">{{ strip_tags($errors->first('mst_discipline_id')) }}</span>
                                        @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="country_id" id="country_id" data-url="{{ url('admin/notifications/getStates') }}" onchange="return getStates(this);">
                                            <option value="">--Select Country--</option>
                                                @foreach($country_list as $row)
                                                     <option value="{{ $row->id }}" {{ ($row->id == old('country_id')) || (!empty($Colleges_data) && $row->id == $Notifications_data->country_id)?'selected':'' }} >{{ $row->country_name }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                 </div>
                                </div>

                                 <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Select State &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorDomain"></span>
                                         @if ($errors->has('mst_domain_id'))
                                              <span class="text-danger sid">{{ strip_tags($errors->first('mst_domain_id')) }}</span>
                                          @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="state_id" id="state_id"  data-url="{{ url('admin/notifications/getDistricts') }}" onchange="return getDistricts(this);" data-id='{{ $heading!='Add'?$Notifications_data->state_id:'' }}'>
                                            <option value="">--Select State--</option>
                                                
                                        </select>
                                    </div>
                                 </div>
                                </div>

                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Select District &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorDomain"></span>
                                         @if ($errors->has('mst_domain_id'))
                                              <span class="text-danger sid">{{ strip_tags($errors->first('mst_domain_id')) }}</span>
                                          @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="district_id" id="district_id" >
                                            <option value="">--Select District--</option>
                                                
                                        </select>
                                    </div>
                                 </div>
                                </div>


                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Title &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorCollege"></span>
                                          @if ($errors->has('title'))
                                            <span class="text-danger sid">{{ strip_tags($errors->first('title')) }}</span>
                                          @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <input type="text" placeholder="Title" class="form-control" @if(!empty($Notifications_data)) value="{{ $Notifications_data['title'] }}" @endif name="title" id="title"  autocomplete="off">
                                    </div>
                                 </div>
                                </div>

                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Description &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorCollege"></span>
                                          @if ($errors->has('title'))
                                            <span class="text-danger sid">{{ strip_tags($errors->first('title')) }}</span>
                                          @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                     
                                      <textarea type="text" placeholder="Description" class="form-control ckeditor" name="description" id="description" autocomplete="off">
                                        @if(!empty($Notifications_data))){{ $Notifications_data['description'] }} @endif
                                      </textarea>
                                        

                                    </div>
                                 </div>
                                </div>

                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Notification Date &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorCollege"></span>
                                          @if ($errors->has('title'))
                                            <span class="text-danger sid">{{ strip_tags($errors->first('title')) }}</span>
                                          @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <input type="text" placeholder="dd/mm/yy" class="form-control" @if(!empty($Notifications_data)) value="{{ $Notifications_data['title'] }}" @endif name="notification_date" id="notification_date"  autocomplete="off">
                                    </div>
                                 </div>
                                </div>


                                 <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Website &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorCollege"></span>
                                          @if ($errors->has('title'))
                                            <span class="text-danger sid">{{ strip_tags($errors->first('title')) }}</span>
                                          @endif
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <input type="text" placeholder="Title" class="form-control" @if(!empty($Notifications_data)) value="{{ $Notifications_data['website'] }}" @endif name="website" id="website" autocomplete="off">
                                    </div>
                                 </div>
                                </div>
                                  
                              
                              </div>

                             
                                  
                              </div>
                                  <input type="hidden" name="id" id="primary_id" @if(!empty($Notifications_data)) value="{{ $Notifications_data['id'] }}" @endif>
                                                                
                                <div class="box-footer">
                                    <button class="btn btn-sm btn-primary" type="submit" onclick="return NotificationAdd();">Submit</button>
                                    <a href="{{ route('colleges/list') }}" class="btn btn-sm btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')

 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/master_validations.js') }}"></script>

 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/notifications.js') }}"></script>
 
 @stop
  
