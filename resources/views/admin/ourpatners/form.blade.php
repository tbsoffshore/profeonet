@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                           
                            
                            <div class="col-md-4 text-light-blue">
                                <span style="font-size:25px">{{ $heading }} Our Partners</span>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4"><span class="required" style="float:right;">* Fields required</span></div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                            <form action="{{ route($action)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                            {{ csrf_field() }}
                              <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Name &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorname"></span>
                                    </div>
                                    <div class="col-xs-5">
                                        <input type="text" placeholder="Name" class="form-control" @if(isset($OurPatners)) value="{{ $OurPatners['name'] }}" @endif name="name" id="name" autocomplete="off">
                                    </div>
                                </div>
                                </div> 
                                 <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Description &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errordescription"></span>
                                    </div>
                                    <div class="col-xs-5">
                                        <textarea type="text" name="description" id="description"  placeholder="Description" class="form-control">@if(isset($OurPatners)) {{ $OurPatners['description'] }} @endif</textarea>
                                       
                                    </div>
                                </div>
                                </div> 
                                  <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Image &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorimage"></span>
                                    </div>
                                    <div class="col-xs-5">
                                      
                                        <input type="file"  class="form-control" name="image" id="image" autocomplete="off" onclick="imageFile()">
                                        <span style="color:blue"><b>Note :</b>Required only JPEG, JPG, PNG file is allowed.</span><br>
                                     <span style="color:red"><b>Note :</b>Recommended Height & Width (100px x 150px). </span><br>

                                     @if(isset($OurPatners))
                                     <img src="{{ url('public/assets/admin/dist/img/ourpatners').'/'.$OurPatners->image }}" width="150px">

                                     @endif
                                    </div>
                                </div>
                                </div> 
                                     <input type="hidden" id="old_image" name="old_image" @if(isset($OurPatners)) value="{{ $OurPatners['image'] }}" @endif>  
                                    <input type="hidden" name="id" id="primary_id" @if(isset($OurPatners)) value="{{ $OurPatners['id'] }}" @endif>
                              </div>                               
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit" onclick="return ourpartnersVal();">Submit</button>
                                    <a href="{{ route('ourpatners/list') }}" class="btn btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')
 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/master_validations.js') }}"></script>
 @stop
  
