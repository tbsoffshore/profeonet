@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                           
                            
                            <div class="col-md-4 text-light-blue">
                                <span style="font-size:25px">{{ $heading }} Package</span>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4"><span class="required" style="float:right;">* Fields required</span></div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                            <form action="{{ url($action)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                            {{ csrf_field() }}
                              <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Title &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorpackage_title"></span>
                                    </div>
                                    <div class="col-xs-5">
                                        <input type="text" placeholder="Title" class="form-control" @if(isset($Packages)) value="{{ $Packages['package_title'] }}" @endif name="package_title" id="package_title" autocomplete="off">
                                    </div>
                                </div>
                                </div> 
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Amount &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorpackage_amount"></span>
                                    </div>
                                    <div class="col-xs-5">
                                        <input type="text" placeholder="Amount" class="form-control" @if(isset($Packages)) value="{{ $Packages['package_amount'] }}" @endif name="package_amount" id="package_amount" autocomplete="off" onkeypress="only_number(event)">
                                    </div>
                                </div>
                                </div> 
                                <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Duration &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorpackage_duration"></span>
                                    </div>
                                    <div class="col-xs-5">
                                        <input type="text" placeholder="Duration" class="form-control" @if(isset($Packages)) value="{{ $Packages['package_duration'] }}" @endif name="package_duration" id="package_duration" autocomplete="off" onkeypress="only_number(event)">
                                    </div>
                                </div>
                                </div> 
                                 <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Description &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorpackage_description"></span>
                                    </div>
                                    <div class="col-xs-5">
                                        <textarea type="text" name="package_description" id="package_description"  placeholder="Description" class="form-control">@if(isset($Packages)) {{ $Packages['package_description'] }} @endif</textarea>
                                       
                                    </div>
                                </div>
                                </div> 
                                  <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Image &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorpackage_image"></span>
                                    </div>
                                    <div class="col-xs-5">
                                      
                                        <input type="file"  class="form-control" name="package_image" id="package_image" autocomplete="off" onclick="imageVal('package_image','{{ $height}}','{{ $width}}')">
                                        <span style="color:blue"><b>Note :</b>Required only JPEG, JPG, PNG file is allowed.</span><br>
                                        @if($height!='' && $width!='')
                                        <span style="color:red"><b>Note :</b>Recommended Height & Width (100px x 150px). </span><br>
                                        @endif
                                         @if(isset($Packages))
                                         <img src="{{ url('public/assets/admin/dist/img/packages').'/'.$Packages->package_image }}" width="80px">
                                         @endif

                                    </div>
                                </div>
                                </div> 
                                     <input type="hidden" id="old_image" name="old_image" @if(isset($Packages)) value="{{ $Packages['package_image'] }}" @endif>  
                                    <input type="hidden" name="id" id="primary_id" @if(isset($Packages)) value="{{ $Packages['id'] }}" @endif>
                              </div>                               
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit" onclick="return packageVal();">Submit</button>
                                    <a href="{{ url('admin/packages') }}" class="btn btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')
 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/master_validations.js') }}"></script>
 @stop
  
