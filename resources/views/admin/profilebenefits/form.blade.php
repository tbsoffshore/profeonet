@extends('admin/welcome')
@section('content')
<div class="content-wrapper">
   <section class="content">
      <div class="row">
         <div class="col-lg-12">
            <div class="box box-primary">
               <div class="box-header with-border">
                  <div class="col-md-4 text-light-blue">
                     <span style="font-size:25px">{{ $heading }} Package</span>
                  </div>
                  <div class="col-md-4"></div>
                  <div class="col-md-4"><span class="required" style="float:right;">* Fields required</span></div>
                  <div class="clearfix"></div>
               </div>
               <div class="box-body">
                  <form action="{{ url($action)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                     {{ csrf_field() }}
                     <div class="col-md-12">
                        <div class="col-md-8">
                           <table class="table table-striped " width="100%" >
                              <thead style="border: 1px solid #EAEBEC;box-shadow: 0px 0px 0px 1px #ccc">
                                 <tr>
                                    <th width="20%">
                                       <center>Benefits</center>
                                    </th>
                                    @if(!isset($ProfileBenefits))
                                    <th width="1%"> <button  type="button" class="btn btn-info" onclick="validation('')" style="width: 100%;font-size: 16px"><b>+</b></button></th>
                                    @endif
                                 </tr>
                              </thead>
                              <tbody class="apend_td" id="professorTableBody">
                                 <tr class="tbl_tr_1 textmult trRow">
                                    <td>
                                       <input tabindex="4" type="text" name="benefits[]" id="benefits1" class="form-control benefits" @if(isset($ProfileBenefits)) value="{{$ProfileBenefits->benefits}}" @endif placeholder="Benefits" value=""  autocomplete="off">
                                    </td>
                                     @if(!isset($ProfileBenefits))
                                    <td>
                                       <button type="button" class="btn btn-danger m-b" style="width: 100%;font-size: 16px"  onclick="remove_tr($(this).closest('tr').index())" ><b>-</b></button> 
                                    </td>
                                     @endif
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                         <input type="hidden" name="id" id="primary_id" @if(isset($ProfileBenefits)) value="{{ $ProfileBenefits['id'] }}" @endif>
                     <div class="box-footer">
                        <button class="btn btn-primary" type="submit" onclick="return validation('submit');">Submit</button>
                        <a href="{{ url('admin/profilebenefits') }}" class="btn btn-default" type="button" onclick="showloader();">Cancel</a>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
@stop
@section('footer_js')
<script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/master_validations.js') }}"></script>
@stop

<script type="text/javascript">
    function validation(val)
    {
        var y=document.getElementById('professorTableBody');       
        var len = y.rows.length; 
        
        for(var i=1; i <= len;  i++)      
        {
            var benefits = $("#benefits"+i).val();  
           
            if(benefits=='')
            {  
                $("#benefits"+i).css("border-color", "red");
                setTimeout(function(){$("#benefits"+i).css("border-color", "#ccc");},3000);
                $("#benefits"+i).focus();
                return false;  
            }
        }
        if(val=='')
        {
            addrow();            
        }
    }
</script>
<script type="text/javascript">
   function addrow()
   {
       var y=document.getElementById('professorTableBody'); 
       var new_row = y.rows[0].cloneNode(true); 
       var len = y.rows.length; 
       new_number=Math.round(Math.exp(Math.random()*Math.log(10000000-0+1)))+0;
   
       var inp1 = new_row.cells[0].getElementsByTagName('input')[0];
       inp1.value = '';
       inp1.id = 'benefits'+(len+1);
       y.appendChild(new_row);        
   }
    
   function remove_tr(row)
   {  
       var y=document.getElementById('professorTableBody');
       var len = y.rows.length;
       if(len>1)
       {
         var i= (len-1);
         document.getElementById('professorTableBody').deleteRow(row);
       }
   }   
</script>