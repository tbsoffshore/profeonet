@extends('admin/welcome')
@section('content')
<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                           
                            
                            <div class="col-md-4 text-light-blue">
                                <span style="font-size:25px">{{ $heading }} </span>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4"><span class="required" style="float:right;">* Fields required</span></div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                            <form action="{{ route($action)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                            {{ csrf_field() }}
                              <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-6 form-group">
                                        <div class="col-xs-12">
                                            <label for="exampleInputEmail1">Site Title &nbsp;<span class="required">*</span></label>&nbsp;
                                             <span class="required" id="errorsite_title"></span>
                                        </div>
                                        <div class="col-xs-12">
                                            <input type="text" placeholder="Site Title " class="form-control" @if(isset($Settings)) value="{{ $Settings['site_title'] }}" @endif name="site_title" id="site_title" autocomplete="off">
                                        </div>
                                    </div>
                                     <div class="col-xs-6 form-group">
                                        <div class="col-xs-12">
                                            <label for="exampleInputEmail1">Mobile  &nbsp;<span class="required">*</span></label>&nbsp;
                                             <span class="required" id="errormobile1"></span>
                                        </div>
                                        <div class="col-xs-12">
                                            <input type="text" placeholder="Mobile" class="form-control" @if(isset($Settings)) value="{{ $Settings['mobile1'] }}" @endif name="mobile1" id="mobile1" autocomplete="off" onkeypress="only_number(event)">
                                        </div>
                                    </div>
                                </div>
                             
                                <div class="col-xs-12">
                                    <div class="col-xs-6 form-group">
                                        <div class="col-xs-12">
                                            <label for="exampleInputEmail1">Alternate No. &nbsp;<span class="required">*</span></label>&nbsp;
                                             <span class="required" id="errormobile2"></span>
                                        </div>
                                        <div class="col-xs-12">
                                            <input type="text" placeholder="Alternate No" class="form-control" @if(isset($Settings)) value="{{ $Settings['mobile2'] }}" @endif name="mobile2" id="mobile2" autocomplete="off" onkeypress="only_number(event)">
                                        </div>
                                    </div>
                                     <div class="col-xs-6 form-group">
                                        <div class="col-xs-12">
                                            <label for="exampleInputEmail1">Email &nbsp;<span class="required">*</span></label>&nbsp;
                                             <span class="required" id="erroremail"></span>
                                        </div>
                                        <div class="col-xs-12">
                                            <input type="text" placeholder="Email" class="form-control" @if(isset($Settings)) value="{{ $Settings['email'] }}" @endif name="email" id="email" autocomplete="off">
                                        </div>
                                    </div>
                                    
                                </div> 
                               
                                <div class="col-xs-12">
                                     <div class="col-xs-6 form-group">
                                        <div class="col-xs-12">
                                            <label for="exampleInputEmail1">Address &nbsp;<span class="required">*</span></label>&nbsp;
                                             <span class="required" id="erroraddress"></span>
                                        </div>
                                        <div class="col-xs-12">
                                             <textarea type="text" name="address" id="address"  placeholder="Address" class="form-control">@if(isset($Settings)) {{ $Settings['address'] }} @endif</textarea>
                                            
                                        </div>
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <div class="col-xs-12">
                                            <label for="exampleInputEmail1">Notification&nbsp;<span class="required">*</span></label>&nbsp;
                                             <span class="required" id="errornotification"></span>
                                        </div>
                                        <div class="col-xs-12">
                                             <textarea type="text" name="notification" id="notification"  placeholder="Notification" class="form-control">@if(isset($Settings)) {{ $Settings['notification'] }} @endif</textarea>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-xs-12">
                                   
                                    <div class="col-xs-6 form-group">
                                        <div class="col-xs-12">
                                            <label for="exampleInputEmail1">Copyright &nbsp;<span class="required">*</span></label>&nbsp;
                                             <span class="required" id="errorcopy_right"></span>
                                        </div>
                                        <div class="col-xs-12">
                                            <input type="text" placeholder="Name" class="form-control" @if(isset($Settings)) value="{{ $Settings['copy_right'] }}" @endif name="copy_right" id="copy_right" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <div class="col-xs-12">
                                            <label for="exampleInputEmail1">Facebook Link&nbsp;<span class="required">*</span></label>&nbsp;
                                             <span class="required" id="errorfacebook_link"></span>
                                        </div>
                                        <div class="col-xs-12">
                                            <input type="text" placeholder="Facebook Link" class="form-control" @if(isset($Settings)) value="{{ $Settings['facebook_link'] }}" @endif name="facebook_link" id="facebook_link" autocomplete="off">
                                        </div>
                                    </div>
                                </div> 
                               
                                <div class="col-xs-12">
                                    <div class="col-xs-6 form-group">
                                        <div class="col-xs-12">
                                            <label for="exampleInputEmail1">Twitter Link&nbsp;<span class="required">*</span></label>&nbsp;
                                             <span class="required" id="errortwitter_link"></span>
                                        </div>
                                        <div class="col-xs-12">
                                            <input type="text" placeholder="Twitter Link" class="form-control" @if(isset($Settings)) value="{{ $Settings['twitter_link'] }}" @endif name="twitter_link" id="twitter_link" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 form-group">
                                        <div class="col-xs-12">
                                            <label for="exampleInputEmail1">Insta Link &nbsp;<span class="required">*</span></label>&nbsp;
                                             <span class="required" id="errorinsta_link"></span>
                                        </div>
                                        <div class="col-xs-12">
                                            <input type="text" placeholder="Insta Link" class="form-control" @if(isset($Settings)) value="{{ $Settings['insta_link'] }}" @endif name="insta_link" id="insta_link" autocomplete="off">
                                        </div>
                                    </div>
                                </div> 
                              
                                  <div class="col-xs-12">
                                    <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Site Logo &nbsp;<span class="required">*</span></label>&nbsp;
                                         <span class="required" id="errorlogo"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-5 col-sm-5 col-lg-5">
                                      
                                        <input type="file"  class="form-control" name="logo" id="logo" autocomplete="off" onclick="imageLogoFile()">
                                        <span style="color:blue"><b>Note :</b>Required only JPEG, JPG, PNG file is allowed.</span><br>
                                     <span style="color:red"><b>Note :</b>Recommended Height & Width (50px x 148px). </span><br>

                                     <img src="{{ url('public/assets/admin/dist/img/logo').'/'.$Settings->logo }}" width="150px">

                                    </div>
                                </div>
                                </div> 
                                     <input type="hidden" id="old_logo" name="old_logo" @if(isset($Settings)) value="{{ $Settings['logo'] }}" @endif>  
                                    <input type="hidden" name="id" id="primary_id" @if(isset($Settings)) value="{{ $Settings['id'] }}" @endif>
                              </div>                               
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit" onclick="return settingsVal();">Update</button>
                                   
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')
 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/master_validations.js') }}"></script>
 @stop
  
