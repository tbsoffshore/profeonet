@extends('admin/welcome')
@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-header with-border">
                    <div class="col-md-5 text-light-blue">
                        <span style="font-size:25px"><img src="{{ asset('public/assets/admin/dist/img/skills.png') }}">&nbsp;&nbsp;Skills List</span>
                    </div>

                    <div class="col-md-7 text-right" style="padding: 0px;">
                        <div>
							<a class="btn btn-primary" title="Export Employee" href=""><i class="glyphicon glyphicon-export"></i>&nbsp;Export To Excel</a>
                            <a class="btn btn-primary" title="Download Format" download="employee.xls" href=""><i class="glyphicon glyphicon-download "></i>&nbsp;Download Format</a>
                            <a class="btn btn-primary" data-toggle="modal" data-target="#upload_employee_modal"><i class="glyphicon glyphicon-import "></i>&nbsp;Import from Excel</a>                             
                            <a class="btn btn-primary" href="{{ route('skills/create') }}" onclick="showloader();">Create</a> 
                        </div>
                        <div class="clearfix">&nbsp;</div>
                         
                    </div>  

                    <div class="col-md-12" style="text-align:center">
                        <span class="msghide"></span>
                    </div>
                    <!-- <div class="clearfix"></div>             -->
                </div>
                <div class="box-body">
                    <div class="">
                        <table id="skill_table" class="table table-striped table-bordered table-hover">
                            <thead class="table-head">
                                <tr> 
                                    <th>Title</th>                                   
                                    <th>Status</th>
                                    <th>Actions</th>
                                    
                                </tr>
                            </thead>                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="checkStatus" data-modal-color="lightblue" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated bounceInRight">   
            <form method="post" action="{{ route($statusAction) }}">
                {{ csrf_field() }}

                <div class="modal-body" style="height: 100px;padding-top: 10%">
                    <center>
                        <input type="hidden" name="id" id="statusId" style="display: none;">
                        <span style="font-size: 16px">Are you sure to change the status?</span>
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ok</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteData" data-modal-color="lightblue" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">   
            <form method="post" action="{{ route($deleteAction) }}">
                {{ csrf_field() }}       
                <div class="modal-body" style="height: 100px;padding-top: 10%">
                    <center>
                        <input type="hidden" name="id" id="deleteId" style="display: none;">
                        <span style="font-size: 16px">Are you sure want to delete this record ?</span>
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ok</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
@section('footer_js')
<script type="text/javascript">
$(document).ready(function() {
     $('#skill_table').DataTable({
        "processing": true,
        "serverSide": true,
        
        "ajax": "{{ route('skills/ajaxSkillList') }}",
        "columns":[
            { "data": "title" },
            {  "data": 'link',"render": function(data, type, row) {
        if(row.status=='Active'){
        return '<a href="#checkStatus" data-toggle="modal" class="label-warning label"  onclick="checkStatus('+row.id+')"> Active </a>';
                } else {
        return '<a href="#checkStatus" data-toggle="modal" class="label-danger label"  onclick="checkStatus('+row.id+')"> Inactive </a>';   
                }
    }},
            {  "data": 'action', orderable: false, searchable: false},
            
    
        ]
     });
});
</script>
@stop

