@extends('admin/welcome')
@section('content')


<div class="content-wrapper">  
<section class="content">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">                           
                            
                            <div class="text-light-blue">
                                <span style="font-size:25px">{{ $heading }} State</span>
                                 <a href="#"  onclick="window.history.back();"class="btn btn-sm btn-warning pull-right">Back</a>
                            </div>
                            <div class="clearfix"></div>   
                        </div>
                        <div class="box-body">
                            <form action="{{ route($action)}}" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                              <div class="row">
                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Select Country &nbsp;<span class="required">*</span></label>&nbsp; 
                                        @if ($errors->has('country_id'))
                                        <span class="text-danger sid">{{ strip_tags($errors->first('country_id')) }}</span>
                                        @endif
                                         <span class="required" id="errorCountry"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <select class="form-control" name="country_id" id="country_id">
                                            <option value="">--Select Country--</option>
                                                @foreach($country_list as $row)
                                                   <option value="{{ $row->id }}" {{ ($row->id == old('country_id')) || 
                                                        (!empty($state_data) &&  $row->id == $state_data->country_id) ?'selected':'' }} >{{ $row->country_name }}
                                                    </option>
                                                @endforeach
                                             
                                        </select>
                                    </div>
                                 </div>
                                </div>

                                <div class="col-xs-12">
                                 <div class="col-xs-12 form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">State Name &nbsp;
                                            @if ($errors->has('state_name'))
                                            <span class="text-danger sid">{{ strip_tags($errors->first('state_name')) }}</span>
                                            @endif
                                            <span class="required">*</span>
                                        </label>&nbsp;
                                         <span class="required" id="errorState"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                                        <input type="text"  placeholder="State Name" class="form-control" @if(!empty($state_data)) value="{{ $state_data['state_name'] }}" @endif name="state_name" id="state_name" autocomplete="off">
                                    </div>
                                    <input type="hidden" name="id" id="primary_id" @if(!empty($state_data)) value="{{ $state_data['id'] }}" @endif>
                                 </div>
                                </div>
                                  
                              </div>
                                
                                                                
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit" onclick="return stateAdd();">Submit</button>
                                    <a href="{{ route('states/list') }}" class="btn btn-default" type="button" onclick="showloader();">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 @stop
 @section('footer_js')
 <script type="text/javascript" src="{{ asset('public/assets/admin/custom-js/master_validations.js') }}"></script>
 @stop
  
