<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PROFEONET | Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('public/assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('public/assets/admin/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('public/assets/admin/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('public/assets/admin/dist/css/AdminLTE.min.css') }}">
  
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('public/assets/admin/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/assets/admin/custom-css/style.css') }}">
  <!-- Morris chart -->
  <!-- <link rel="stylesheet" href="{{ asset('assets/bower_components/morris.js/morris.css') }}"> -->
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset('public/assets/admin/bower_components/jvectormap/jquery-jvectormap.css') }}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ asset('public/assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('public/assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('public/assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('admin/dashboard') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>P</b>&nbsp;A</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>PROFEONET</b>&nbsp;Admin</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
         <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a> -->
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>          
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ asset('public/assets/admin/dist/img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ ucfirst(Auth::user()->name) }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{ asset('public/assets/admin/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">

                <p>
                  Admin - {{ ucfirst(Auth::user()->name) }}
                  <small>Super Admin</small>
                </p>
              </li>              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">Sign out</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </div>
              </li>
            </ul>
          </li>          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('public/assets/admin/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>         
        </div>
      </div>      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">       
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>           
          </a>         
        </li>
       
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Masters</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">
          <!--   <li><a href="{{ route('skills/list') }}"><img src="{{ asset('public/assets/admin/dist/img/skills_icons.png') }}"> Skills</a></li>
            <li><a href="{{ route('qualifications/list') }}"><i class="fa fa-graduation-cap"></i> Qualifications</a></li>

            <li><a href="{{ route('experiences/list') }}"><img src="{{ asset('public/assets/admin/dist/img/student_icons.png') }}"> Experiences</a></li> -->
            <li><a href="{{ route('countries/list') }}"><i class="fa fa-globe"></i> Countries</a></li>
            <li><a href="{{ route('states/list') }}"> <img src="{{ asset('public/assets/admin/dist/img/north-america_icons.png') }}"> States</a></li>
            <li><a href="{{ route('districts/list') }}"><i class="fa fa-database"></i> Districts</a></li>
            <li><a href="{{ route('profession/list') }}">  <i class="fa fa-graduation-cap"></i> Professions</a></li>
            <li><a href="{{ route('discipline/list') }}"> <i class="fa fa-dot-circle-o"></i>  Disciplines </a></li>
            <li><a href="{{ route('domain/list') }}"> <i class="fa fa-dot-circle-o"></i>  Domains</a></li>
            <li><a href="{{ route('baners/list') }}"> <i class="fa fa-flag-o"></i> Banners</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-cogs"></i>  
            <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">

            <li><a href="{{ route('settings/create') }}"><i class="fa fa-cog"></i> Web Settings</a></li>
           <!--  <li><a href="{{ route('ourpatners/list') }}"><img src="{{ asset('public/assets/admin/dist/img/student_icons.png') }}"> Our Pantners</a></li> -->
           <!--  <li><a href="{{ url('admin/packages') }}"><i class="fa fa-globe"></i> Packages</a></li>
            <li><a href="{{ url('admin/profilebenefits') }}"><i class="fa fa-dollar"></i> Profile Benefits</a></li> -->
            <li><a href="{{ route('cms_pages/list') }}"><i class="fa fa-stack-overflow"></i> CMS</a></li>
            
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-building-o"></i>  
            <span>Works</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">

            <li><a href="{{ route('areas/list') }}"><i class="fa fa-building-o"></i>Area Of Works</a></li>
          
            <li><a href="{{ route('colleges/list') }}"><i class="fa fa-home"></i> Colleges</a></li>
          </ul>


        </li>

        <li><a href="{{ route('connections/list') }}"><i class="fa fa-handshake-o"></i> Connections</a></li>
        <li><a href="{{ route('cgpa/list') }}"><i class="fa fa-cc"></i>Manage CGPA</a></li>
        <li><a href="{{ route('scategories/list') }}"><i class="fa fa-futbol-o"></i>Schollar Categories</a></li>
        <li><a href="{{ route('notification/list') }}"><i class="fa fa-bell"></i>Notification Categories</a></li>
        <li><a href="{{ route('notifications/list') }}"><i class="fa fa-bell"></i>Notifications</a></li>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
     
        <div class="alert  {{ Session::get('alert-class') }}">
         <i class="fa fa-check" aria-hidden="true"></i> <strong>{{ Session::get('session') }}</strong>
         
        </div>
      
    </div>
  </div>
  @yield('content')
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!-- <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div> -->
    <strong>Copyright &copy; 2018-2019 <a href="https://adminlte.io">Technobase IT Solutions Pvt. Ltd.</a></strong> All rights reserved.    
  </footer>  
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
     
        <div class="alert  {{ Session::get('alert-class') }}">
         <i class="fa fa-check" aria-hidden="true"></i> <strong>{{ Session::get('session') }}</strong>
         
        </div>
      
    </div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('public/assets/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/bower_components/jquery/dist/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/bower_components/jquery/dist/dataTables.bootstrap.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('public/assets/admin/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('public/assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="{{ asset('public/assets/admin/bower_components/raphael/raphael.min.js') }}"></script>

<!-- Sparkline -->
<script src="{{ asset('public/assets/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('public/assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('public/assets/admin/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('public/assets/admin/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('public/assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('public/assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('public/assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('public/assets/admin/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/assets/admin/dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/dist/js/demo.js') }}"></script>
<script src="{{ asset('public/assets/ckeditor/ckeditor.js') }}"></script>
@yield('footer_js')
<script type="text/javascript">

    
     $(function(){
    setTimeout(function(){
        $(".msghide").hide();
        }, 2000);
      });

     
    
$(function(){

      var url = window.location;

      $('ul.sidebar-menu a').filter(function() {

        return this.href == url;

      }).parent().addClass('active');



    // for treeview

    $('ul.treeview-menu a').filter(function() {

       return this.href == url;

    }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');

});

  function checkStatus(id)
  { 
      $("#statusId").val(id);
      $("#deleteId").val(id);
  }
<?php if(Session::has('session')) { ?>
    $("#myModal").modal('show');
    setTimeout(function()
    {
    $("#myModal").modal('hide');
},2000
        ); 
<?php } ?>
$(function(){
  $.ajaxSetup({
   headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
  });
});
</script>
 <script type="text/javascript">
        'use strict';
        const BASE_URL = '{!! url('/').'/' !!}';
    </script>
</body>
</html>