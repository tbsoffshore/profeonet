<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>PROFEONET</title> 
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{ asset('public/assets/front-end/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('public/assets/front-end/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('public/assets/front-end/css/mystyle.css') }}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/assets/front-end/images/fav-icon/apple-touch-icon.png') }}">
  <link rel="icon" type="image/png" href="{{ asset('public/assets/front-end/images/fav-icon/favicon-32x32.png') }}" sizes="32x32">
  <link rel="icon" type="image/png" href="{{ asset('public/assets/front-end/images/fav-icon/favicon-16x16.png') }}" sizes="16x16">
  @yield('css')

</head>
<body>
<div class="boxed_wrapper">
<header class="top-bar">
    <div class="container">
        <div class="clearfix">
            <div class="col-left float_left">
                <ul class="top-bar-info">
                    <li><i class="icon-technology"></i>Phone: (123) 0200 12345</li>
                    <li><i class="icon-note2"></i>help@profeonet.com</li>
                </ul>
            </div>
            <div class="col-right float_right">
                <ul class="social">
                    <li>Stay Connected: </li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                </ul>
                @if(empty(Session::get('web_session')))
                <div class="link">
                    <a href="{{ url('home/login') }}" class="thm-btn">LOGIN </a> |
                    <a href="{{ url('home/register') }}" class="thm-btn">REGISTER </a>
                </div>
                @endif
            </div>
                
                
        </div>
      </div>
</header>

<section class="theme_menu stricky">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="main-logo">
                    <a href="{{ url('/') }}"><img src="{{ asset('public/assets/front-end/images/logo/logo.png') }}" alt=""></a>
                </div>
            </div>
            <div class="col-md-8 menu-column">
                <nav class="menuzord" id="main_menu">
                   <ul class="menuzord-menu">
                        <?php $page = basename($_SERVER['PHP_SELF']);?>     
                        <li class="<?php if($page=='index.php'){?>active<?php } ?>"><a href="index.php">Home</a></li>

                        <li><a href="#">About us</a>
                        <ul class="dropdown">
                           <!--  <li><a href="about.php">About us</a></li> -->
                            <li><a href="{{ url('home/about-us') }}">About us</a></li>
                            <li><a href="team.php">Meet Our Team</a></li>
                            <li><a href="faq.php">FAQ’s</a></li>
                            <li><a href="testimonials.php">Client Feedback</a></li>
                         </ul>
                        </li>

                        <li><a href="service.php">services</a>
                        <ul class="dropdown">
                            <li><a href="service-1.php">Business Growth</a></li>
                            <li><a href="service-2.php">Sustainability</a></li>
                            <li><a href="service-3.php">Performance</a></li>
                            <li><a href="service-4.php">Advanced Analytics</a></li>
                            <li><a href="service-5.php">Organization</a></li>
                            <li><a href="service-6.php">Customer Insights</a></li>
                         </ul>
                            
                        </li>

                        <li><a href="project.php">Projects</a></li>

                        <li><a href="blog-grid.php">blog</a></li>
                      
                        <li><a href="contact.php">Contact us</a></li>

                        @if(!empty(Session::get('web_session')))
                        @php
                        $session_id = Session::get('web_session')->id;
                        $WebUsers = new App\Model\front_end\WebUsers;
                        $session_user = $WebUsers->where('id',$session_id)->first(); 
                        @endphp
                        <li><a href="#" class="paddinglimenu"> <span>
                        @if(empty($session_user->image)) <img src="{{ asset('public/assets/front-end/images/profile/user.png' ) }}" alt="" class="userprof"> 
                         @else 
                        <img src="{{ asset('public/assets/front-end/images/profile/$session_user->image') }}" alt="" class="userprof">
                        @endif
                        <span> </a>
                         <ul class="dropdown margint15">
                            <li><a href="{{ url('home/dashboard') }}">My Account</a></li>
                            <li><a href="{{ url('home/editprofile') }}">Edit Profile</a></li>
                            <li><a href="{{ url('home/changepassword') }}">Change Password</a></li>
                            <li><a href="{{ url('home/logout') }}">Logout</a></li>
                         </ul>
                         </li>
                         <input type="hidden" id="unique_session_id" value="{{ $session_id }}">
                         @endif

                         
                    </ul><!-- End of .menuzord-menu -->
                </nav> <!-- End of #main_menu -->
            </div>
            <div class="right-column">
                <div class="right-area">
                    <div class="nav_side_content">
                        <div class="search_option">
                            <button class="search tran3s dropdown-toggle color1_bg" id="searchDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search" aria-hidden="true"></i></button>
                            <form action="#" class="dropdown-menu" aria-labelledby="searchDropdown">
                                <input type="text" placeholder="Search">
                                <button><i class="fa fa-search" aria-hidden="true"></i></button>
                            </form>
                       </div>
                   </div>
                </div>
                    
            </div>
        </div>
                

   </div> <!-- End of .conatiner -->
</section>

@yield('content')

<div class="call-out">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <figure class="logo">
                    <a href="index-2.html"><img src="{{ asset('public/assets/front-end/images/logo/logo2.png' ) }}" alt=""></a>
                </figure>
            </div>
            <div class="col-md-9 col-sm-12">
                <div class="float_left">
                    <div class="size16 colorwhite paddingt20">Platform for all the faculties, researchers, industry professionals, PhD scholars, ME/M.Tech scholars, MSc scholars and MBA scholars from science, engineering and management discipline</div>
                </div>
                <!-- <div class="float_right">
                    <a href="contact.html" class="thm-btn-tr">Request Quote</a>
                </div> -->
            </div>
        </div>
                
    </div>
</div>
<footer class="main-footer">
    
    <!--Widgets Section-->
    <div class="widgets-section">
        <div class="container">
            <div class="row">
                <!--Big Column-->
                <div class="big-column col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        
                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget about-widget contact-widget">
                                <h3 class="footer-title">About Us</h3>
                                
                                <div class="widget-content">
                                    <div class="text"><p class="colorccc">The Profeonet consulting over 20 years of experience we’ll ensure you always get the best guidance.</p> </div>
                                       
                                     <ul class="social">
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget links-widget">
                                <h3 class="footer-title">Our Services</h3>
                                <div class="widget-content">
                                    <ul class="list">
                                        <li><a href="#" class="colorccc">Business Growth</a></li>
                                        <li><a href="#" class="colorccc">Sustainability</a></li>
                                        <li><a href="#" class="colorccc">Performance</a></li>
                                        <li><a href="#" class="colorccc">Advanced Analytics</a></li>
                                    
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Big Column-->
                <div class="big-column col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        

                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget contact-widget">
                                <h3 class="footer-title">Contact us</h3>
                                <div class="widget-content">
                                    <ul class="contact-info">
                                        <li><span class="icon-signs colorccc"></span>22/121 Apple Street, New York, <br>NY 10012, USA</li>
                                        <li><span class="icon-phone-call colorccc"></span> Phone: +123-456-7890</li>
                                        <li><span class="icon-e-mail-envelope colorccc"></span>Mail@Fortuneteam.com</li>
                                    </ul>
                                </div>
                          
                            </div>
                        </div>

                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget news-widget">
                                <h3 class="footer-title">Newsletter</h3>
                                <div class="widget-content">
                                    <!--Post-->
                                    <div class="text"><p class="colorccc">Sign up for latest product news</p></div>
                                    <!--Post-->
                                    <form action="#" class="default-form">
                                        <input type="email" placeholder="Email Address">
                                        <button type="submit" class="thm-btn">Subscribe Us</button>
                                    </form>
                                </div>
                                
                            </div>
                        </div>                     
                        
                    </div>
                </div>
                
             </div>
         </div>
     </div>
     
     <!--Footer Bottom-->
     <section class="footer-bottom">
        <div class="container">
            <div class="pull-left copy-text">
                <p class="colorccc">Copyrights © 2018. All Rights Reserved. Powered by  <a href="#"> <b> The Profeonet. </b></a></p>
                
            </div><!-- /.pull-right -->
            <div class="pull-right get-text">
                <ul>
                    <li><a href="#" class="colorccc">Support |  </a></li>
                    <li><a href="#" class="colorccc">Privacy & Policy |</a></li>
                    <li><a href="#" class="colorccc"> Terms & Conditions</a></li>
                </ul>
            </div><!-- /.pull-left -->
        </div><!-- /.container -->
    </section>
     
</footer>

<!-- Scroll Top Button -->
  <button class="scroll-top tran3s color2_bg">
    <span class="fa fa-angle-up"></span>
  </button>
  <!-- pre loader  -->
  <div class="preloader"></div>


  <!-- jQuery js -->
  <script src="{{ asset('public/assets/front-end/js/jquery.js') }}"></script>
  <!-- bootstrap js -->
  <script src="{{ asset('public/assets/front-end/js/bootstrap.min.js') }}"></script>
  <!-- jQuery ui js -->
  <script src="{{ asset('public/assets/front-end/js/jquery-ui.js') }}"></script>
  <!-- owl carousel js -->
  <script src="{{ asset('public/assets/front-end/js/owl.carousel.min.js') }}"></script>
  <!-- jQuery validation -->
  <script src="{{ asset('public/assets/front-end/js/jquery.validate.min.js') }}"></script>

  <!-- mixit up -->
  <script src="{{ asset('public/assets/front-end/js/wow.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/jquery.mixitup.min.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/jquery.fitvids.js') }}"></script>
    <script src="{{ asset('public/assets/front-end/js/bootstrap-select.min.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/menuzord.js') }}"></script>

  <!-- revolution slider js -->
  <script src="{{ asset('public/assets/front-end/js/jquery.themepunch.tools.min.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/jquery.themepunch.revolution.min.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/revolution.extension.actions.min.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/revolution.extension.carousel.min.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/revolution.extension.kenburn.min.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/revolution.extension.layeranimation.min.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/revolution.extension.migration.min.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/revolution.extension.navigation.min.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/revolution.extension.parallax.min.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/revolution.extension.slideanims.min.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/revolution.extension.video.min.js') }}"></script>

  <!-- fancy box -->
  <script src="{{ asset('public/assets/front-end/js/jquery.fancybox.pack.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/jquery.polyglot.language.switcher.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/nouislider.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/jquery.bootstrap-touchspin.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/SmoothScroll.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/jquery.appear.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/jquery.countTo.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/jquery.flexslider.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/imagezoom.js') }}"></script> 
    <script src="{{ asset('public/assets/front-end/js/bxslider.js') }}"></script> 
  <script id="map-script" src="{{ asset('public/assets/front-end/js/default-map.js') }}"></script>
  <script src="{{ asset('public/assets/front-end/js/custom.js') }}"></script>

  <script type="text/javascript">
        'use strict';
        const BASE_URL = '{!! url('/').'/' !!}';
    </script>

    <script type="text/javascript">
       setTimeout(function(){ $(".hideerror").slideUp("&nbsp;");},4000);
    </script>
@yield('footer_js')
</div>
</body>
</html>