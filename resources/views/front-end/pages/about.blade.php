@extends('front-end/common')
@section('content')
<link rel="stylesheet" href="{{ asset('public/assets/front-end/css/select2.min.css') }}" /> 

<div class="inner-banner text-center">
    <div class="container">
        <div class="box">
            <h3>{{ $cms_data['title'] }}</h3>
        </div><!-- /.box -->
        <div class="breadcumb-wrapper">
            <div class="clearfix">
                <div class="pull-left">
                    <ul class="list-inline link-list">
                        <li>
                            <a href="{{ url('/') }}">Home</a>
                        </li>
                        <li>
                            {{ $cms_data['title'] }}
                        </li>
                    </ul>
                </div>
                
            </div>
        </div>
    </div>
</div>
<section class="default-section sec-padd">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="text-content">
                    <!-- <h4>{{ $cms_data['title'] }}</h4> -->
                    <div class="text">
                        {!! $cms_data['description'] !!}
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <figure class="img-box">
                    <a href="#"><img src="{{ asset('public/assets/front-end/images/resource/12.jpg') }}" alt=""></a>
                </figure>
            </div>
        </div>
    </div>
</section>
@stop
