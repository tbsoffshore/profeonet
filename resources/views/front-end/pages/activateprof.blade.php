@extends('front-end/common')
@section('content')

<div class="inner-banner text-center">
    <div class="container">
        <div class="box">
            <h3>My Account</h3>
        </div><!-- /.box -->
      
    </div><!-- /.container -->
</div>
<div class="single-projects sec-padd-top">
    <div class="container">
        <div class="client-information">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="borderlight">
                	<div class="single-testimonial center margint20">
                		<center>
		                <figure class="img-box borderlight profilebox">
		                    <a href="#">

                    @if(empty($user->image)) <img src="{{ asset('public/assets/front-end/images/profile/user.png' ) }}" alt="" class="userpic"> 
                    @else 
                    <img src="{{ asset('public/assets/front-end/images/profile/$user->user_image') }}" alt="" class="userpic">
                    @endif

                            </a>
		                </figure>

		                <div class="content">
		                <br>
		                    <h5>{{ $user->first_name }} {{ $user->last_name }}</h5>
		                    @if(!empty($user->company_name)) <p class="author-title"><a href="#"> {{ $user->company_name }} </a></p> @endif
                    	</div>
                    	</center>
                	</div>
                	<br>
                	

                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5 "> <span class="bluelabel"> Profession </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel">{{ $user->profession_name }}   </span> </div>	
                	</div>
                	<div class="clearfix"></div>

                     @if(!empty($user->email)) 
                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5 "> <span class="bluelabel"> Email </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel"> {{ $user->email }}  </span> </div>	
                	</div>
                	<div class="clearfix"></div>
                    @endif

                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5 "> <span class="bluelabel"> Phone No. </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel"> {{ $user->mobile }}  </span> </div>	
                	</div>
                	<div class="clearfix"></div>

                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5"> <span class="bluelabel"> Address </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel"> Laxminagar, Nagpur. </span> </div>	
                	</div>
                	<div class="clearfix"></div>

                	

                    <div class="contact-info2">
                        <h4>Contact Us</h4>
                        <ul>
                            <li><i class="fa fa-phone"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope"></i><a href="#">Mailus@Experts.com</a></li>
                        </ul>
                        <button class="thm-btn" data-toggle="modal" data-target="#activateModal">Activate Profile</button>
                    </div>
                  </div>
                </div>


                <div class="col-md-9 col-sm-9 col-xs-12">
                  <div class="borderlight">
                            <div class="contact-info2 margint0 padding1330">
                            <center><span class="colorwhite size16 bold"> Activate Your Profile </span></center>
                           </div>

                        <div class="styled-form register-form">
                        <form method="post" action="{{ url('payment/payment')}}" enctype="multipart/form-data">
                      
                        {{ csrf_field() }}

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margint20">

                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingl0">
                              <label>Amount</label>
                              <div class="form-group">
                                 <span class="adon-icon"><span class="fa fa-inr"></span></span>
                                 <input type="text" value="199" placeholder="Amount*" readonly>
                                 <input type="hidden" name="amount" value="199">
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                                <label>Name</label>
                              <div class="form-group">
                                 <span class="adon-icon"><span class="fa fa-user"></span></span>
                                 <input type="text" value="{{ $user->first_name }} {{ $user->last_name }}" placeholder="Last Name *"  readonly>
                                <input type="hidden" name="last_name" value="{{ $user->first_name }} {{ $user->last_name }}">
                              </div>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingl0">
                             <label>Mobile</label>
                                <div class="form-group">
                                 <span class="adon-icon"><span class="fa fa-mobile"></span></span>
                                 <input type="text" value="{{ $user->mobile }}" placeholder="Mobile *" maxlength="10" onkeypress="return isNumberKey(event)"  readonly>
                                  <input type="hidden" name="mobile"  id="mobile" value="{{ $user->mobile }}">
                              </div>
                             
                           </div>
                           @if(!empty($user->email))
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                           <label>Email</label>
                               <div class="form-group">
                                 <span class="adon-icon"><span class="fa fa-envelope-o"></span></span>
                                 <input type="text" value="{{ $user->email }}"  placeholder="Email *"  readonly>
                                 <input type="hidden" name="email" id="email" value="{{ $user->email }}">
                                 <span class="required" id="email_error"></span>
                              </div>
                           </div>
                           @endif

                            @if(!empty($user->company_official_email))
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                             <label>Company Email</label>
                               <div class="form-group">
                                 <span class="adon-icon"><span class="fa fa-envelope-o"></span></span>
                                 <input type="text" name="cemail" id="cemail" value="{{ $user->company_official_email }}"  placeholder="Email *" readonly>
                              </div>
                           </div>
                           @endif
                        </div>
                

                        <button type="submit" class="btnblue floatright"> Pay Now</button>
                       
                        <div class="clearfix"></div>


                     </form>
                     </div>
							          <br>
                  	</div> <br>

                </div>
            </div>
        </div>
        <br><br>
       
    </div><br> 

</div>


<script type="text/javascript" src="{{ asset('public/assets/front-end/custom-js/RegisterValidations.js') }}"></script>
@stop

