@extends('front-end/common')
@section('content')

<div class="inner-banner text-center">
    <div class="container">
        <div class="box">
            <h3>My Account</h3>
        </div><!-- /.box -->
      
    </div><!-- /.container -->
</div>
<div class="single-projects sec-padd-top">
    <div class="container">
        <div class="client-information">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="borderlight">
                	<div class="single-testimonial center margint20">
                		<center>
		                <figure class="img-box borderlight profilebox">
		                    <a href="#">

                    @if(empty($user->image)) <img src="{{ asset('public/assets/front-end/images/profile/user.png' ) }}" alt="" class="userpic"> 
                    @else 
                    <img src="{{ asset('public/assets/front-end/images/profile/$user->user_image') }}" alt="" class="userpic">
                    @endif

                            </a>
		                </figure>

		                <div class="content">
		                <br>
		                    <h5>{{ $user->first_name }} {{ $user->last_name }}</h5>
		                    @if(!empty($user->company_name)) <p class="author-title"><a href="#"> {{ $user->company_name }} </a></p> @endif
                    	</div>
                    	</center>
                	</div>
                	<br>
                	

                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5 "> <span class="bluelabel"> Profession </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel">{{ $user->profession_name }}   </span> </div>	
                	</div>
                	<div class="clearfix"></div>

                     @if(!empty($user->email)) 
                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5 "> <span class="bluelabel"> Email </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel"> {{ $user->email }}  </span> </div>	
                	</div>
                	<div class="clearfix"></div>
                    @endif

                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5 "> <span class="bluelabel"> Phone No. </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel"> {{ $user->mobile }}  </span> </div>	
                	</div>
                	<div class="clearfix"></div>

                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5"> <span class="bluelabel"> Address </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel"> Laxminagar, Nagpur. </span> </div>	
                	</div>
                	<div class="clearfix"></div>

                	

                    <div class="contact-info2">
                        <h4>Contact Us</h4>
                        <ul>
                            <li><i class="fa fa-phone"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope"></i><a href="#">Mailus@Experts.com</a></li>
                        </ul>
                        <button class="thm-btn" data-toggle="modal" data-target="#activateModal">Activate Profile</button>
                    </div>
                  </div>
                </div>


                <div class="col-md-9 col-sm-9 col-xs-12">
                  <div class="borderlight">
                            <div class="contact-info2 margint0 padding1330">
                            <span class="colorwhite size16 bold"> Edit Profile </span>
                            <span class="colorwhite size16 bold pull-right"> I'm  {{ $user->profession_name }}  </span></div>

                        <div class="styled-form register-form">
                        <form method="post" action="{{ url('/').$editprofileaction}}" enctype="multipart/form-data">
                        <center> <div class="h20"><span id="errormsg" class="colorred"></span></div></center><div class="margint10"></div>
                        {{ csrf_field() }}

                        <input type="hidden" value="{{ $user->profession_name }}" id="profession_id">
                     
                        @if($user->profession_name!='Industry/Company')
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margint20">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingl0">
                              <div class="form-group">
                                 <span class="adon-icon"><span class="fa fa-user"></span></span>
                                 <input type="text" name="first_name" id="first_name" value="{{ $user->first_name }}" placeholder="First Name *">
                                 <span class="required" style="" id="errorfirst_name"></span>
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                              <div class="form-group">
                                 <span class="adon-icon"><span class="fa fa-user"></span></span>
                                 <input type="text" name="last_name" id="last_name" value="{{ $user->last_name }}" placeholder="Last Name *">
                                 <span class="required" id="errorlast_name"></span>
                              </div>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingl0">
                              <div class="form-group">
                                 <span class="adon-icon"><span class="fa fa-envelope-o"></span></span>
                                 <input type="text" name="email" id="email" value="{{ $user->email }}" placeholder="Email *">
                                 <span class="required" id="email_error"></span>
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                              <div class="form-group">
                                 <span class="adon-icon"><span class="fa fa-mobile"></span></span>
                                 <input type="text" name="mobile"  id="mobile" value="{{ $user->mobile }}" placeholder="Mobile *" maxlength="10" onkeypress="return isNumberKey(event)">
                                 <span class="required" id="errormobile"></span>
                              </div>
                           </div>
                        </div>
                       
                       
                       @else


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                 <span class="adon-icon"><span class="fa fa-user"></span></span>
                                 <input  class="form-control" type="text" name="company_name" id="company_name" value="{{ $user->company_name }}" placeholder="Company Name *">
                                 <span class="required" style="" id="errorcompany_name"></span>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding0">
                                 <div class="form-group">
                                    <span class="adon-icon"><span class="fa fa-building"></span></span>
                                    <select class="form-control" name="domain1_id " id="domain1">
                                       <option  value="">Select Domain1</option>
                                       @foreach($Domain as $row)
                                       <option value="{{ $row['id'] }}" @if($row['id']==$user->domain1_id) selected @endif >{{ $row['title'] }} {{ $row['id'] }}</option>
                                       @endforeach
                                    </select>
                                     
                                 </div>
                              </div>
                              <div class="col-md-4 padding0">
                                 <div class="form-group">
                                    <span class="adon-icon"><span class="fa fa-briefcase"></span></span>
                                    <select class="form-control" name="domain2_id " id="domain2">
                                       <option value="">Select Domain2</option>
                                       @foreach($Domain as $row2)
                                       <option value="{{ $row2['id'] }}" @if($user->domain2_id==$row2['id']) selected @endif >{{ $row2['title'] }}</option>
                                       @endforeach
                                    </select>
                                    
                                    <span class="required" style="" id="errordomain2"></span>
                                 </div>
                              </div>
                              <div class="col-md-4 padding0">
                                 <div class="form-group">
                                    <span class="adon-icon"><span class="fa fa-briefcase"></span></span>
                                    <select class="form-control" name="domain3_id " id="domain3">
                                       <option value="">Select Domain3</option>
                                       @foreach($Domain as $row3)
                                       <option value="{{ $row3['id'] }}" @if($row3['id']==$user->domain3_id) selected @endif >{{ $row3['title'] }}</option>
                                       @endforeach
                                    </select>
                                   
                                    <span class="required" style="" id="errordomain3"></span>
                                 </div>
                              </div>
                           </div>
                           <span class="required" style="" id="errordomain1"></span>
                           <div class="clearfix"></div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding0">
                                 <div class="form-group">
                                    <span class="adon-icon"><span class="fa fa-briefcase"></span></span>
                                    <input type="hidden" value="{{ url('home/getStates') }}" id="state_url">
                                    <select class="form-control" name="company_country_id" id="country_id_"  data-url="{{ url('home/getStates') }}" onchange="return getStates_2();">
                                       <option  value="">Select Country</option>
                                       @foreach($Countries as $row)
                                       <option value="{{ $row['id'] }}"  @if($row['id']==$user->company_country_id) selected @endif >{{ $row['country_name'] }}</option>
                                       @endforeach
                                    </select>
                                    <span class="required" style="" id="errorcountry"></span>
                                 </div>
                              </div>
                             
                             <input type="hidden" name="" value="{{ $user->company_state_id }}" id="hid_fld_state_">
                             <input type="hidden" name="" value="{{ $user->company_city_id }}" id="hid_fld_district_">

                              <div class="col-md-4 padding0">
                                 <div class="form-group">
                                    <span class="adon-icon"><span class="fa fa-briefcase"></span></span>
                                    <!-- <select class="form-control" name="company_state_id" id="state_id" data-url="{{ url('home/getDistricts') }}" onchange="return getDistricts_2('0');">
                                       <option  value="">Select State</option>
                                        
                                    </select> -->
                                      <select class="form-control" name="company_state_id" id="state_id_"  onchange="return getDistricts_2();">
                                        <option value="">--Select State--</option>
                                        </select>
                                 </div>
                              </div>
                              <div class="col-md-4 padding0">
                                 <div class="form-group">
                                    <span class="adon-icon"><span class="fa fa-briefcase"></span></span>
                                    <select class="form-control" name="company_city_id" id="district_id_">
                                       <option  value="">Select District</option>
                                   
                                    </select>
                                    <span class="required" style="" id="errordistrict"></span>
                                 </div>
                              </div>
                           </div>
                           <div class="clearfix"></div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding0">
                                 <div class="form-group">
                                    <span class="adon-icon"><span class="fa fa-user"></span></span>
                                    <input type="text" name="first_name1" id="firstname" value="{{ $user->first_name }}" placeholder="First Name *">
                                    <span class="required" style="" id="errorfirstname"></span>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                                 <div class="form-group">
                                    <span class="adon-icon"><span class="fa fa-user"></span></span>
                                    <input type="text" name="last_name1" id="lastname" value="{{ $user->last_name }}" placeholder="Last Name *">
                                    <span class="required" id="errorlastname"></span>
                                 </div>
                              </div>
                           </div>
                           <div class="clearfix"></div>


                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding0">
                                 <div class="form-group">
                                    <span class="adon-icon"><span class="fa fa-user"></span></span>
                                    <input type="text" name="website_link" id="web_link" value="{{ $user->website_link }}" placeholder="Company Website Link *">
                                    <span class="required" style="" id="errorweblink"></span>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                                 <div class="form-group">
                                    <span class="adon-icon"><span class="fa fa-user"></span></span>
                                    <input type="text" name="company_official_emai" id="cmpny_email" value="{{ $user->company_official_email }}" placeholder="Company Official Email  *">
                                    <span class="required" id="errorcmpny_email"></span>
                                 </div>
                              </div>
                           </div>
                           <div class="clearfix"></div>


                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding0">
                                 <div class="form-group">
                                    <span class="adon-icon"><span class="fa fa-mobile"></span></span>
                                    <input type="text" name="mobile1" id="phone" value="{{ $user->mobile }}" placeholder="Mobile No. *" maxlength="10">
                                    <span class="required" style="" id="error_mobile"></span>
                                 </div>
                              </div>
 
                           </div>
                           <div class="clearfix"></div>
                        </div>

                       @endif

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">

                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                                 <div class="form-group">
                                    <span class="adon-icon"><span class="fa fa-flag"></span></span>
                                    <input type="text" name="company_tagline" id="company_tagline" value="{{ $user->company_tagline }}" placeholder="Company Tagline. *">
                                  
                                 </div>
                              </div>


                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                                 <div class="form-group">
                                     <input class="form-control" id="user_image" type="file" name="user_image">
                                     @if($user->user_image)
                                    <img id="previewHolder" src="{{ asset('public/assets/front-end/images/profile/').'/'.$user->user_image }}" 
                                    class="profupload mt20"/>
                                    @endif
                                 </div>
                                 <input type="hidden" name="old_user_image" id="old_user_image" value="{{ $user->user_image }}">
                              </div>
                                       
                          <div class="clearfix"></div>

                         </div>


                        <button type="button" class="btnblue floatright" onclick="editprofilevalidation()"> UPDATE</button>
                        <button type="submit" id="clicksbm1" style="display: none"> </button>
                        <div class="clearfix"></div>


                     </form>
                     </div>
							          <br>
                  	</div> <br>

                </div>
            </div>
        </div>
        <br><br>
       
    </div><br> 

</div>



<div id="activateModal" class="modal fade margint80" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bgnavey">
        <button type="button" class="close colorwhite" data-dismiss="modal">&times;</button>
        <center><h4 class="modal-title colorwhite">{{ $packages_data->package_title }}</h4></center>
      </div>
      <div class="modal-body size16">
        <p class="lh35"> {!! $packages_data->package_description !!}  </p>
      </div>
      <div class="modal-footer">
         <center>
        <a type="button" class="btn thm-btn" href="{{ url('home/activateprofile') }}">Activate Portfolio</a>
        <button type="button" class="btn btn-default bigbtn" data-dismiss="modal">Cancel</button>
        </center>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript" src="{{ asset('public/assets/front-end/custom-js/RegisterValidations.js') }}"></script>
@stop

@section('footer_js')
<script type="text/javascript">
$( document ).ready(function() {

   var state_url = $("#state_url").val();
   getStates_2();
   getDistricts_2();  

});


 $("#image").change(function() {
  readURL(this);
 });
 function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader(); 
    reader.onload = function(e) {
      $('#previewHolder').attr('src', e.target.result).css({"width":"200px","height": "150px"});
    }
    reader.readAsDataURL(input.files[0]);
  }
}

function getStates_2()
{
  var url = BASE_URL+'home/getStates';
  var country_id = $("#country_id_").val();
  var state_id = $("#hid_fld_state_").val();

  $.ajax({  
      url:url,
      cache: false,
      type:'POST',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data:{country_id:country_id,state_id:state_id},
      success: function(response)
      {
          $("#state_id_").html(response);
          $("#district_id_").val('');
          getDistricts_2();
      }
   });
}

function getDistricts_2()
{
    var url = BASE_URL+'home/getDistricts';
    var country_id = $("#country_id_").val();
    var district_id = $("#hid_fld_district_").val();
    var state_id = $("#state_id_").val();
   $.ajax({  
      url:url,
      cache: false,
      type:'POST',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data:{state_id:state_id,district_id:district_id},
      success: function(response)
      {
          $("#district_id_").html(response);
      }
   });
}

</script>
@stop