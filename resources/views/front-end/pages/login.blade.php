@extends('front-end/common')
@section('content')

<section class="register-section sec-padd-top">
    <div class="container">
        <div class="row">
            
            <center>
            
            <div class="form-column column col-lg-4 col-md-4 col-sm-12 col-xs-12"> &nbsp;
            </div>
            <!--Form Column-->
            <div class="form-column column col-lg-4 col-md-4 col-sm-12 col-xs-12 borderlight">

                <div class="section-title margint10">
                    <h3>LOGIN</h3>
                    <div class="decor"></div>
                    <!-- <span class="error" id="show_error"> -->
                </div>

                 <div class="h30">
                 <div class="colorgreen hideerror bold" id="show_success">{{ Session::get('message') }} </div>
                 <div class="colorred text-center" id="show_error" style="display: none;"></div>
                 </div>
                
                <!--Login Form-->
               
                <div class="styled-form register-form">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                          <div class="form-group">
                            <span class="adon-icon"><span class="fa fa-mobile"></span></span>
                             <input type="text" id ="username" name="username" value="" placeholder="Mobile *" maxlength="10">
                       
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                          <div class="form-group">
                            <span class="adon-icon"><span class="fa fa-lock"></span></span>
                            <input type="password" name="password" id="password" value="" placeholder="Password *">
                           
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="clearfix">
                        <div class="form-group pull-left">
                            <button type="button" class="thm-btn thm-tran-bg" onclick="return login_validation()">LOGIN</button>
                        </div>
                      
                    </div>
                </div>
            </div>

            <div class="form-column column col-lg-4 col-md-4 col-sm-12 col-xs-12"> &nbsp;
            </div>
          </center>
        </div>
    </div>
</section>

<script type="text/javascript" src="{{ asset('public/assets/front-end/custom-js/login_validation.js') }}"></script>
@stop
