@extends('front-end/common')
@section('content')
<link rel="stylesheet" href="{{ asset('public/assets/front-end/css/select2.min.css') }}" /> 

<div class="inner-banner text-center">
    <div class="container">
        <div class="box">
            <h3>My Account</h3>
        </div><!-- /.box -->
      
    </div><!-- /.container -->
</div>
<div class="single-projects sec-padd-top">
    <div class="container">
        <div class="client-information">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="borderlight">
                	   <div class="single-testimonial center margint20">
                        <center>
                        <figure class="img-box borderlight profilebox">
                            <a href="#">

                            @if(empty($user->image)) <img src="{{ asset('public/assets/front-end/images/profile/user.png' ) }}" alt="" class="userpic"> 
                            @else 
                            <img src="{{ asset('public/assets/front-end/images/profile/$user->user_image') }}" alt="" class="userpic">
                            @endif

                                    </a>
                                </figure>

                                <div class="content">
                                <br>
                                    <h5>{{ $user->first_name }} {{ $user->last_name }}</h5>
                                    @if(!empty($user->company_name)) <p class="author-title"><a href="#"> {{ $user->company_name }} </a></p> @endif
                                </div>
                                </center>
                            </div>
                	<br>
                	

                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5 "> <span class="bluelabel"> Profession </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel">{{ $user->profession_name }}   </span> </div>	
                	</div>
                	<div class="clearfix"></div>

                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5 "> <span class="bluelabel"> Email </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel"> {{ $user->email }}  </span> </div>	
                	</div>
                	<div class="clearfix"></div>

                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5 "> <span class="bluelabel"> Phone No. </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel"> {{ $user->mobile }}  </span> </div>	
                	</div>
                	<div class="clearfix"></div>

                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5"> <span class="bluelabel"> Address </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel"> Laxminagar, Nagpur. </span> </div>	
                	</div>
                	<div class="clearfix"></div>

                	

                    <div class="contact-info2">
                        <h4>Contact Us</h4>
                        <ul>
                            <li><i class="fa fa-phone"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope"></i><a href="#">Mailus@Experts.com</a></li>
                        </ul>
                        <a href="#" class="thm-btn">Get Free Quote</a>
                    </div>
                  </div>
                </div>


                <div class="col-md-6 col-sm-6 col-xs-12 padding0">
                		<div class="col-md-12 col-sm-12 col-xs-12 padding0">

                		<div class="borderlight padding15">
                     
                     <form action="{{ route('home/save_post_add') }}" method="post" enctype="multipart/form-data">
                          {{ csrf_field() }}
                           <center>
                            <div class="h20"><span id="errormsg" class="colorred"></span></div></center>
                            <div class="margint10"></div>
                		 	<div class="col-md-12 col-sm-12 col-xs-12 padding0">
                            <div class="form-group">
							<select  class="form-control" id="post_where" name="connections">
								<option value="">Where to Post</option>
								<option value='Management Commercial Experts'>Management Commercial Experts</option>
								<option value='Communication Experts'>Communication Experts</option>
							</select>
							</div>
							</div>
                            <div class="clearfix"></div>

							<div class="col-md-12 col-sm-12 col-xs-12 padding0">
                            <div class="form-group">
								<textarea class="form-control" id="addvertise" name="addvertise" placeholder="Start Advertising Branding Connecting..."></textarea> 
							</div>
							</div><div class="clearfix"></div>
                            <div class="col-md-12 col-sm-12 col-xs-12 padding0">
                            <div class="form-group">
                            <select  class="form-control select2" name="tag_member[]" id="tag_member" multiple="multiple">
                                <option></option>
                                @if($tags)
                                 @foreach($tags as $row)
                                    <option value="{{ $row->id }}"> {{ $row->first_name }} </option>
                                 @endforeach
                                 @endif
                            
                            </select>
                            </div>
                            </div>
                            <div class="clearfix"></div>

							<div class="col-md-12 col-sm-12 col-xs-12 padding0">
							<div class="col-md-5 col-sm-5 col-xs-5 padding0">
                            <div class="form-group">
								<input class="form-control" type="file" required="" placeholder="Your Name *" value="" name="user_image" id="user_image" aria-required="true">
							</div>
							</div>
                            
                            <div class="col-md-2 col-sm-2 col-xs-2 padding0">
                            </div>
							<div class="col-md-5 col-sm-5 col-xs-5 padding0">
                            <div class="form-group">
								<input class="form-control" type="text" required="" placeholder="Type Location" value="" name="location" aria-required="true" id="location">
							</div>
							</div>

							</div>
                            <div class="clearfix"></div>

							<input type="button" value="POST" class="btnblue floatright" onclick="addpostvalidation()">
                            <button type="submit" id="clicksbm1" style="display: none"> </button>
							<div class="clearfix"></div>
                       </form>
                		</div>



                        @foreach($details as $posts)

                        @php 
                          $imp = array();
                          $Tags = DB::table('tags')->where('posts_id',$posts->id)->get();
                          foreach( $Tags as $tagsuser)
                          {
                          $imp[]=  $tagsuser->web_users_id;
                          }
                          
                        @endphp
                        <?php 
                        $impld = implode(',',$imp);   
                        $tag =DB::select("select * from web_users where id in (".$impld.")");             
                        $tag1=DB::select("select * from web_users where id in (".$impld.") and id != ".$tag[0]->id."");
                        $likescount =DB::select("select SUM(likes) as counlikes from post_likes where post_id = ".$posts->id." ");   
 
                        ?>
		                <div class="margint20 default-blog-news wow fadeInUp animated borderlight" style="visibility: visible; animation-name: fadeInUp;">
		                <div class="paddingtb10">
		                <div class="col-md-12 col-sm-12 col-xs-12 padding0">

                            <div id="givetaggeduser{{ $posts->id }}" style="display:none">
                                @foreach($tag1 as $usertags)
                                <div> {{ $usertags->first_name }} {{ $usertags->last_name }}  </div>
                                @endforeach
                            </div>
                        
		                	<div class="col-md-2 col-sm-2 col-xs-2">
                            @if(empty($user->image)) <img src="{{ asset('public/assets/front-end/images/profile/user.png' ) }}" alt="" class=" img-thumbnail img-responsive"> 
                            @else 
                            <img src="{{ asset('public/assets/front-end/images/profile/$user->user_image') }}" alt="" class=" img-responsive img-thumbnail">
                            @endif
		                	</div>

    		               <div class="col-md-8 col-sm-8 col-xs-12 padding0">
                             <span> <b>{{ $user->first_name }} {{ $user->last_name }}</b></span>&nbsp; with 
                             {{ $tag[0]->first_name }}  {{ $tag[0]->last_name }} and
                             <a class="sharewithname" onclick="taggeduser({{ $posts->id }})"> more... </a><br>
                             <span> {{ $user->profession_name }} </span><br>
                             <span> Technobase IT Solutions </span><br>
                              <span> 23-06-2018 17:00:32 </span>
                            </div>


        	               <div class="col-md-2 col-sm-2 col-xs-12 padding0">   
        	               <div class="dropdown_blog">
							  <span class="dropbtn_blog">Action  <i class="fa fa-chevron-down"></i></span>
							  <div class="dropdown-content_blog">
							    <a href="#">Edit</a>
							    <a href="#">Delete</a>
							  </div>
							</div>
        	               </div>

		                </div>
		                </div>



		                <div class="clearfix"></div><br>

		                    <figure class="img-holder">
		                        <a href="blog-details.html"><img src="{{ asset('public/assets/front-end/images/post/').'/'.$posts->image }}" alt="Posts"></a>
		                        <figcaption class="overlay">
		                            <div class="box">
		                                <div class="content">
		                                    <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
		                                </div>
		                            </div>
		                        </figcaption>
		                    </figure>
		                    <div class="padding10">
		                        <div class="text">
		                            <p> {{ $posts->comment }}</p>
                                <div class="col-md-12 margint10">
                                  <div class="col-md-4 colorblue"> <span onclick="like({{ $posts->id }})" class="cursorpointer">  <i class="fa fa-thumbs-up"></i> Like </span>  <span class="bold" id="likeappend{{ $posts->id }}"> &nbsp; {{ $likescount[0]->counlikes }} </span> </div>

                                  <div class="col-md-4 colorblue"> <center> <i class="fa fa-comment"></i> Comment 
                                  <span id="commentappend{{ $posts->id }}"> 0 </span> 
                                  </center> </div>

                                  <div class="col-md-4 colorblue paddingr20"> <span onclick="savepost({{ $posts->id }})" class="paddingl50"> <i class="fa fa-bookmark"></i> Save Post </span> 
                                  <span id="savedpostappend{{ $posts->id }}"> 0 </span> 
                                  </div>
                                </div>
                                </div>
		                       <!--  <div class="link pull-right">
		                            <a href="blog-details.html" class="default_link">Read More <i class="fa fa-angle-right"></i></a>
		                        </div> -->
		                        
		                    </div>
		                </div>
                        @endforeach


		                
		            </div>
                </div>


                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="borderlight">
                            <div class="contact-info2 margint0 padding1330"><center>
                            <span class="colorwhite size16 bold"> SMART SEARCH </span></center>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
							<select  class="form-control">
								<option>Select Smart Search</option>
								<option>Search Expert</option>
								<option>Search Industry</option>
								<option>Search Engineering Scholars</option>
								<option>Search Management Scholars</option>
								<option>Search Industry Professionals</option>
							</select>
							</div>
							</div><div class="clearfix"></div>

							<div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
							<select  class="form-control">
								<option>Select Domain</option>
								<option>Agriculture and Planning</option>
								<option>Biotechnology</option>
								<option>Chemistry</option>
								<option>Mathematics</option>
								<option>Physics</option>
							</select>
							</div>
							</div><div class="clearfix"></div>

							<div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
							<select  class="form-control">
								<option>Select Country</option>
								<option>India</option>
							</select>
							</div>
							</div><div class="clearfix"></div>

							<div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
							<select  class="form-control">
								<option>Select State</option>
								<option>Maharashtra</option>
								<option>Kerala</option>
								<option>Punjab</option>
								<option>Asaam</option>
								<option>Jammu</option>
							</select>
							</div>
							</div><div class="clearfix"></div>

							<div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
							<select  class="form-control">
								<option>Select City</option>
								<option>Nagpur</option>
								<option>Mumbai</option>
								<option>Pune</option>
							
							</select>
							</div>
							</div><div class="clearfix"></div>

							<input type="submit" value="SEARCH" class="btnblue floatright">
							<div class="clearfix"></div>
							<br>
                  		</div> <br>


                  	  <div class="popular_news borderlight padding15">
                        <div class="inner-title">
                            <span class="bold size16 ">Upcomming Events+Activities</span class="bold size16 ">
                        </div>

                        <div class="popular-post">
                            <div class="item">
                                <div class="post-thumb"><a href="#"><img src="{{ asset('public/assets/front-end/images/blog/post-1.jpg') }}" alt=""></a></div>
                                <a href="#"><h5>Finance & legal <br>throughout project.</h5></a>
                                <div class="post-info"><i class="fa fa-calendar"></i>Jan 08, 2017 </div>
                            </div>
                            <div class="item">
                                <div class="post-thumb"><a href="#"><img src="{{ asset('public/assets/front-end/images/blog/post-2.jpg') }}" alt=""></a></div>
                                <a href="#"><h5>What makes us best <br>in the world? </h5></a>
                                <div class="post-info"><i class="fa fa-calendar"></i>Dec 18, 2016</div>
                            </div>
                            <div class="item">
                                <div class="post-thumb"><a href="#"><img src="{{ asset('public/assets/front-end/images/blog/post-3.jpg') }}" alt=""></a></div>
                                <a href="#"><h5>Why People go with <br>Experts.</h5></a>
                                <div class="post-info"><i class="fa fa-calendar"></i>Nov 21, 2016 </div>
                            </div>
                        </div>
                    </div>



                </div>


            </div>
        </div>
        <br><br>



        <div class="project-analysis">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="section-title">
                        <h3>Project Analysis</h3>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="text">

          
                    <div class="text margint20">
                        <p>The challenge is to bring company seds whose web presence is boring up to date. The challenge sed is to ensure that when a ut client visits your websites they feel positive about your company projects.  </p>
                    </div>
                    <div class="analysis-chart">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-6">    
                                <div class="single-item center">
                                    <img src="{{ asset('public/assets/front-end/images/service/1.png') }}" alt="">
                                    <h4>Analysis One</h4>
                                </div>
                                
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">    
                                <div class="single-item center">
                                    <img src="{{ asset('public/assets/front-end/images/service/2.png') }}" alt="">
                                    <h4>Analysis Two</h4>
                                </div>
                                
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">    
                                <div class="single-item center">
                                    <img src="{{ asset('public/assets/front-end/images/service/3.png') }}" alt="">
                                    <h4>Analysis Three</h4>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                        <p>
                            The project analyst providers critical data support to technical Research and analysis functions may include sed budget tracking financial sed forecasting projects evaluation and monitoring, maintanence compliance regulations and performing establisheed fact that a readers.
                      	</p>
                    </div>
                    <h5 class="graph-title">Project growth analysis Apr’2016 to Dec’2016</h5>
                    <div class="img-box">
                        <img src="{{ asset('public/assets/front-end/images/project/29.jpg' ) }}" alt="">
                    </div><br><br>
                </div>
            </div>
        </div>
       
    </div><br><br><br>   

</div>



<div id="Taggedusers" class="modal fade margint80" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tagged Users</h4>
      </div>
      <div class="modal-body">
        <p id="appendgivetageed"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script src="{{ asset('public/assets/front-end/js/jquery.js') }}"></script>
<script src="{{ asset('public/assets/front-end/js/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('public/assets/front-end/custom-js/dashboard.js') }}"></script>
<script type="text/javascript">
$('.select2').select2({  placeholder: "Tags" });
</script>
@stop
