@extends('front-end/common')
@section('content')

<section class="register-section sec-padd-top">
    <div class="container">
        <div class="row">
            
            <center>
            
            <div class="form-column column col-lg-3 col-md-3 col-sm-12 col-xs-12"> &nbsp;
            </div>
            <!--Form Column-->
            <div class="form-column column col-lg-6 col-md-6 col-sm-12 col-xs-12 borderlight">

                <div class="section-title margint10">
                    <h3>OTP Verification</h3>
                    <div class="decor"></div>
                    <!-- <span class="error" id="show_error"> -->
                </div>

                 <div class="h80">
                 <div class="colorgreen bold size16"><!-- {{ Session::get('message') }}  --> 
                  Hi {{ $reg_user->first_name }} {{ $reg_user->last_name }} , Your Primary Registration has been done. <br>
                 
                 </div>
                 <div class="colorgray size14">
                  You will get OTP on your registered Mobile Number, Verify OTP for succesfull Login.
                 </div>
                 <div class="colorred text-center bold size16" id="show_error" style="display: none;"></div>
                 </div>
                
                <!--Login Form-->
               
                <div class="styled-form register-form">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">

                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 padding0">
                        <label class="margint10"> Enter OTP</label>
                     </div>

                      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                          <div class="form-group">
                            <span class="adon-icon"><span class="fa fa-rate"></span></span>
                             <input type="text" id="otp" name="otp" value="" placeholder="Enter OTP *">

                        </div>
                     </div>

                    </div>

                    <div class="clearfix"></div>

                    <center>
                    <div class="clearfix">
                        <div class="form-group pull-left">
                            <button type="button" class="thm-btn thm-tran-bg" onclick="return otp_verification()">Verify OTP</button>
                        </div>
                    </div>
                    </center>
                </div>
            </div>

            <div class="form-column column col-lg-3 col-md-3 col-sm-12 col-xs-12"> &nbsp;

            </div>
          </center>
        </div>
    </div>
</section>
<input type="hidden" id="verify_otp" name="verify_otp" value="{{ $reg_user->otp }}">
<input type="hidden" id="user_id" name="user_id" value="{{ $reg_user->id }}">
<script type="text/javascript" src="{{ asset('public/assets/front-end/custom-js/login_validation.js') }}"></script>
@stop
