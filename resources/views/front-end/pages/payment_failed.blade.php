@extends('front-end/common')
@section('content')

<div class="inner-banner text-center">
    <div class="container">
        <div class="box">
            <h3>My Account</h3>
        </div><!-- /.box -->
      
    </div><!-- /.container -->
</div>
<div class="single-projects sec-padd-top">
    <div class="container">
        <div class="client-information">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="borderlight">
                	<div class="single-testimonial center margint20">
                		<center>
		                <figure class="img-box borderlight profilebox">
		                    <a href="#">

                    @if(empty($user->image)) <img src="{{ asset('public/assets/front-end/images/profile/user.png' ) }}" alt="" class="userpic"> 
                    @else 
                    <img src="{{ asset('public/assets/front-end/images/profile/$user->user_image') }}" alt="" class="userpic">
                    @endif

                            </a>
		                </figure>

		                <div class="content">
		                <br>
		                    <h5>{{ $user->first_name }} {{ $user->last_name }}</h5>
		                    @if(!empty($user->company_name)) <p class="author-title"><a href="#"> {{ $user->company_name }} </a></p> @endif
                    	</div>
                    	</center>
                	</div>
                	<br>
                	

                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5 "> <span class="bluelabel"> Profession </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel">{{ $user->profession_name }}   </span> </div>	
                	</div>
                	<div class="clearfix"></div>

                     @if(!empty($user->email)) 
                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5 "> <span class="bluelabel"> Email </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel"> {{ $user->email }}  </span> </div>	
                	</div>
                	<div class="clearfix"></div>
                    @endif

                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5 "> <span class="bluelabel"> Phone No. </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel"> {{ $user->mobile }}  </span> </div>	
                	</div>
                	<div class="clearfix"></div>

                	<div class="col-md-12 padding0 margint10">
                	<div class="col-md-5"> <span class="bluelabel"> Address </span> <span class="floatright"> : </span>
                	</div>
                	<div class="col-md-7 paddingl0"> <span class="graylabel"> Laxminagar, Nagpur. </span> </div>	
                	</div>
                	<div class="clearfix"></div>

                	

                    <div class="contact-info2">
                        <h4>Contact Us</h4>
                        <ul>
                            <li><i class="fa fa-phone"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope"></i><a href="#">Mailus@Experts.com</a></li>
                        </ul>
                        <button class="thm-btn" data-toggle="modal" data-target="#activateModal">Activate Profile</button>
                    </div>
                  </div>
                </div>


                <div class="col-md-9 col-sm-9 col-xs-12">
                  <div class="borderlight">
                            <div class="contact-info2 margint0 padding1330">
                            <center><span class="colorwhite size16 bold"> Payment Failed  </span></center>
                           </div>

                           <div class="styled-form register-form">

                            <div class="col-md-12"><center>
                            <i class="fa fa-times round box-icon-large box-icon-center box-icon-red mt30"></i>
                            <br>

                            <label class="sec-title-two colorred size20 padding20 "> Opps !! Your payment has been failed. </label>
                              
                            
                            <div class="clearfix"></div>
                           
                             </center>
                             <br>
                            </div><br> <br>
                            <div class="clearfix"></div>
                        
                        </div>

                    
                  	</div> <br>

                </div>
            </div>
        </div>
        <br><br>
       
    </div><br> 

</div>


<script type="text/javascript" src="{{ asset('public/assets/front-end/custom-js/RegisterValidations.js') }}"></script>
@stop

