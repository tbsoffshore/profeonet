@extends('front-end/common')
@section('content')
<section class="register-section sec-padd-top">
 <div class="container">
  <div class="row">
   <center>
      <div class="form-column column col-lg-3 col-md-3 col-sm-12 col-xs-12"> &nbsp;
      </div>
      <!--Form Column-->
      <div class="form-column column col-lg-6 col-md-6 col-sm-12 col-xs-12 borderlight">
         <div class="section-title margint10">
            <h3>Register Now</h3>
            <div class="decor"></div>
         </div>
         <!--Login Form-->
         <div class="styled-form register-form" id="myDiv">
            <div class="h20"><span id="errormsg" class="text-danger"></span></div><div class="margint10"></div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                  <div class="form-group">
                     <span class="adon-icon"><span class="fa fa-briefcase"></span></span>
                     <select class="form-control" name="profession_id" id="profession_id" onchange="getdomain(this.value)">
                        <option value="">Select Profession</option>
                        @foreach($Profession as $row)
                        <option value="{{ $row['id'] }}" data-change="{{ $row['is_industry'] }}">{{ $row['title'] }}</option>
                        @endforeach
                     </select>
                     <span class="required" style="" id="errorfirst_profession_id"></span>
                     <input type="hidden" id="profession_name" name="profession_name" value="">
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="register1">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingl0">
                        <div class="form-group">
                           <span class="adon-icon"><span class="fa fa-user"></span></span>
                           <input type="text" name="first_name" id="first_name" value="" placeholder="First Name *">
                           <span class="required" style="" id="errorfirst_name"></span>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                        <div class="form-group">
                           <span class="adon-icon"><span class="fa fa-user"></span></span>
                           <input type="text" name="last_name" id="last_name" value="" placeholder="Last Name *">
                           <span class="required" id="errorlast_name"></span>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingl0">
                        <div class="form-group">
                           <span class="adon-icon"><span class="fa fa-mobile"></span></span>
                           <input type="text" name="mobile"  id="mobile" value="" placeholder="Mobile *" maxlength="10" onkeypress="return isNumberKey(event)" oninput="register.sendOTP(this.value)">
                           <span class="required" id="errormobile"></span>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                        <div class="form-group">
                           <span class="adon-icon"><span class="fa fa-mobile"></span></span>
                           <input type="text" name="r_verifyOTP" class="r_verifyOTP" value="" placeholder="Verify Mobile : OTP *" onkeypress="return isNumberKey(event)" oninput="register.verifyOTP(this.value)">
                           <span class="required" id="r_verifyOTP"></span>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingl0">
                        <div class="form-group">
                           <span class="adon-icon"><span class="fa fa-user"></span></span>
                           <input type="text" name="username" id="username" value="" placeholder="Username *" readonly="">
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                        <div class="form-group">
                           <span class="adon-icon"><span class="fa fa-envelope-o"></span></span>
                           <input type="text" name="email" id="email" value="" placeholder="Email *">
                           <span class="required" id="email_error"></span>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingl0">
                        <div class="form-group">
                           <span class="adon-icon"><span class="fa fa-unlock-alt"></span></span>
                           <input type="password" name="password"  id="password" value="" placeholder="Password *">
                           <span class="required" id="password_error"></span>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                        <div class="form-group">
                           <span class="adon-icon"><span class="fa fa-unlock-alt"></span></span>
                           <input type="password" name="show_password " id="confirm_password" value="" placeholder="Confirm Password *"><span class="required" id="confirm_password_error"></span>
                        </div>
                     </div>
                  </div>
               </div>
               <div id="industryCompany" style="display:none">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                        <div class="form-group">
                           <span class="adon-icon"><span class="fa fa-user"></span></span>
                           <input  class="form-control" type="text" name="company_name" id="company_name" value="" placeholder="Company Name *">
                           <span class="required" style="" id="errorcompany_name"></span>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding0">
                           <div class="form-group">
                              <span class="adon-icon"><span class="fa fa-briefcase"></span></span>
                              <select class="form-control" name="domain1_id " id="domain1">
                                 <option  value="">Select Domain1</option>
                                 @foreach($Domain as $row)
                                 <option value="{{ $row['id'] }}">{{ $row['title'] }}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                        <div class="col-md-4 padding0">
                           <div class="form-group">
                              <span class="adon-icon"><span class="fa fa-briefcase"></span></span>
                              <select class="form-control" name="domain2_id " id="domain2">
                                 <option value="">Select Domain2</option>
                                 @foreach($Domain as $row)
                                 <option value="{{ $row['id'] }}">{{ $row['title'] }}</option>
                                 @endforeach
                              </select>
                              <span class="required" style="" id="errordomain2"></span>
                           </div>
                        </div>
                        <div class="col-md-4 padding0">
                           <div class="form-group">
                              <span class="adon-icon"><span class="fa fa-briefcase"></span></span>
                              <select class="form-control" name="domain3_id " id="domain3">
                                 <option value="">Select Domain3</option>
                                 @foreach($Domain as $row)
                                 <option value="{{ $row['id'] }}">{{ $row['title'] }}</option>
                                 @endforeach
                              </select>
                              <span class="required" style="" id="errordomain3"></span>
                           </div>
                        </div>
                     </div>
                     <span class="required" style="" id="errordomain1"></span>
                     <div class="clearfix"></div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding0">
                           <div class="form-group">
                              <span class="adon-icon"><span class="fa fa-briefcase"></span></span>
                              <input type="hidden" value="{{ url('home/getStates') }}" id="state_url">
                              <select class="form-control" name="company_country_id" id="country_id"  data-url="{{ url('home/getStates') }}" onchange="return getStates(this);">
                                 <option  value="">Select Country</option>
                                 @foreach($Countries as $row)
                                 <option value="{{ $row['id'] }}"  @if($row['sortname']=='IN') selected @endif >{{ $row['country_name'] }}</option>
                                 @endforeach
                              </select>
                              <span class="required" style="" id="errorcountry"></span>
                           </div>
                        </div>
                        <div class="col-md-4 padding0">
                           <div class="form-group">
                              <span class="adon-icon"><span class="fa fa-briefcase"></span></span>
                              <select class="form-control" name="company_state_id" id="state_id" data-url="{{ url('home/getDistricts') }}" onchange="return getDistricts(this);">
                                 <option  value="">Select State</option>
                                
                              </select>
                              <span class="required" style="" id="errorstate"></span>
                           </div>
                        </div>
                        <div class="col-md-4 padding0">
                           <div class="form-group">
                              <span class="adon-icon"><span class="fa fa-briefcase"></span></span>
                              <select class="form-control" name="company_city_id" id="district_id">
                                 <option  value="">Select District</option>
                                
                              </select>
                              <span class="required" style="" id="errordistrict"></span>
                           </div>
                        </div>
                     </div>
                     <div class="clearfix"></div>

                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding0">
                           <div class="form-group">
                              <span class="adon-icon"><span class="fa fa-user"></span></span>
                              <input type="text" name="first_name1" id="firstname" value="" placeholder="First Name *">
                              <span class="required" style="" id="errorfirstname"></span>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                           <div class="form-group">
                              <span class="adon-icon"><span class="fa fa-user"></span></span>
                              <input type="text" name="last_name1" id="lastname" value="" placeholder="Last Name *">
                              <span class="required" id="errorlastname"></span>
                           </div>
                        </div>
                     </div>
                     <div class="clearfix"></div>


                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding0">
                           <div class="form-group">
                              <span class="adon-icon"><span class="fa fa-user"></span></span>
                              <input type="text" name="website_link" id="web_link" value="" placeholder="Company Website Link *">
                              <span class="required" style="" id="errorweblink"></span>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                           <div class="form-group">
                              <span class="adon-icon"><span class="fa fa-user"></span></span>
                              <input type="text" name="company_official_emai" id="cmpny_email" value="" placeholder="Company Official Email  *">
                              <span class="required" id="errorcmpny_email"></span>
                           </div>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding0">
                           <div class="form-group">
                              <span class="adon-icon"><span class="fa fa-mobile"></span></span>
                              <input type="text" name="mobile1" id="phone" value="" placeholder="Mobile No. *" maxlength="10" onkeypress="return isNumberKey(event)" oninput="register.sendOTP(this.value)">
                              <span class="required" style="" id="error_mobile"></span>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                           <div class="form-group">
                              <span class="adon-icon"><span class="fa fa-mobile"></span></span>
                              <input type="text" name="r_verifyOTP" class="r_verifyOTP" value="" placeholder="Verify Mobile : OTP *" onkeypress="return isNumberKey(event)" oninput="register.verifyOTP(this.value)">
                              <span class="required" id="r_verifyOTP"></span>
                           </div>
                        </div>

                       
                     </div>
                     <div class="clearfix"></div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding0">
                           <div class="form-group">
                              <span class="adon-icon"><span class="fa fa-unlock-alt"></span></span>
                              <input type="password" name="password1"  id="pswd" value="" placeholder="Password *">
                              <span class="required" id="error_password"></span>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 paddingr0">
                           <div class="form-group">
                              <span class="adon-icon"><span class="fa fa-unlock-alt"></span></span>
                              <input type="password" name="show_password1" id="confirm_pswd" value="" placeholder="Confirm Password *"><span class="required" id="confirm_password_error"></span>
                           </div>
                        </div>
                     </div>
                    
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div class="clearfix">
                  <div class="form-group pull-left">
                     <button type="button" class="thm-btn thm-tran-bg" onclick="return checkvalidation()">Register here</button>
                     {{-- <button type="submit" id="sbtmclick"> &nbsp;</button> --}}
                  </div>
                  <div class="form-group padd-top-15 pull-right">
                     * Enter Valid Mobile Number to get OTP for successfully Register.
                  </div>
               </div>
            {{-- </form> --}}
         </div>
      </div>
      <div class="form-column column col-lg-3 col-md-3 col-sm-12 col-xs-12"> &nbsp;
      </div>
   </center>
  </div>
 </div>
</section>

<script type="text/javascript" src="{{ asset('public/assets/front-end/custom-js/RegisterValidations.js') }}"></script>
@stop

@section('footer_js')
<script type="text/javascript">
$( document ).ready(function() {

  var domain = $("#profession_id").val();
 	var state_url = $("#state_url").val();

  if(domain)
  {
   	getdomain(domain);
  }
  if(state_url)
  {
   	getStates1(state_url);
  }

});
</script>
@stop