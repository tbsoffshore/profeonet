<?php
/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| Here is where you can register web routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| contains the "web" middleware group. Now create something great!

|

*/

Route::get('/admin', function () {	

    return view('admin/login/login');

});



Route::get('/admin/dashboard', 'Admin\DashboardController@index');



Route::get('home', function(){

	return redirect('admin/dashboard');

});



//Skills



Route::get('/admin/skills/list', 'Admin\SkillsController@index')->name('skills/list');

Route::get('/admin/skills/create', 'Admin\SkillsController@create')->name('skills/create');

Route::post('/admin/skills/create_action', 'Admin\SkillsController@create_action')->name('skills/create_action');

Route::get('/admin/skills/ajaxSkillList', 'AjaxdataController@ajaxSkillList')->name('skills/ajaxSkillList');

Route::get('/admin/skills/update/{id}', 'Admin\SkillsController@update')->name('skills/update');

Route::post('/admin/skills/update_action', 'Admin\SkillsController@update_action')->name('skills/update_action');

Route::post('/admin/skills/change_action', 'Admin\SkillsController@change_action')->name('skills/change_action');

Route::post('/admin/skills/delete_action', 'Admin\SkillsController@delete_action')->name('skills/delete_action');



//Qualifications

Route::get('/admin/qualifications/list', 'Admin\QualificationsController@index')->name('qualifications/list');

Route::get('/admin/qualifications/create', 'Admin\QualificationsController@create')->name('qualifications/create');

Route::post('/admin/qualifications/create_action', 'Admin\QualificationsController@create_action')->name('qualifications/create_action');

Route::get('/admin/qualifications/update/{id}', 'Admin\QualificationsController@update')->name('qualifications/update');

Route::post('/admin/qualifications/update_action', 'Admin\QualificationsController@update_action')->name('qualifications/update_action');

Route::get('/admin/qualifications/ajaxQualificationList', 'AjaxdataController@ajaxQualificationList')->name('qualifications/ajaxQualificationList');

Route::post('/admin/qualifications/change_action', 'Admin\QualificationsController@change_action')->name('qualifications/change_action');

Route::post('/admin/qualifications/delete_action', 'Admin\QualificationsController@delete_action')->name('qualifications/delete_action');



//ourpatners

Route::get('/admin/ourpatners/list', 'Admin\OurPatnersController@index')->name('ourpatners/list');

Route::get('/admin/ourpatners/create', 'Admin\OurPatnersController@create')->name('ourpatners/create');

Route::post('/admin/ourpatners/create_action', 'Admin\OurPatnersController@create_action')->name('ourpatners/create_action');

Route::get('/admin/ourpatners/ajaxOurPatnersList', 'AjaxdataController@ajaxOurPatnersList')->name('ourpatners/ajaxOurPatnersList');

Route::get('/admin/ourpatners/update/{id}', 'Admin\OurPatnersController@update')->name('ourpatners/update');

Route::post('/admin/ourpatners/update_action', 'Admin\OurPatnersController@update_action')->name('ourpatners/update_action');

Route::post('/admin/ourpatners/change_action', 'Admin\OurPatnersController@change_action')->name('ourpatners/change_action');

Route::post('/admin/ourpatners/delete_action', 'Admin\OurPatnersController@delete_action')->name('ourpatners/delete_action');





//Experiences

Route::get('/admin/experiences/list', 'Admin\ExperiencesController@index')->name('experiences/list');

Route::get('/admin/experiences/create', 'Admin\ExperiencesController@create')->name('experiences/create');

Route::post('/admin/experiences/create_action', 'Admin\ExperiencesController@create_action')->name('experiences/create_action');

Route::get('/admin/experiences/ajaxExperienceList', 'AjaxdataController@ajaxExperienceList')->name('experiences/ajaxExperienceList');

Route::get('/admin/experiences/update/{id}', 'Admin\ExperiencesController@update')->name('experiences/update');

Route::post('/admin/experiences/update_action', 'Admin\ExperiencesController@update_action')->name('experiences/update_action');

Route::post('/admin/experiences/change_action', 'Admin\ExperiencesController@change_action')->name('experiences/change_action');

Route::post('/admin/experiences/delete_action', 'Admin\ExperiencesController@delete_action')->name('experiences/delete_action');





//Countries

Route::get('/admin/countries/list', 'Admin\CountriesController@index')->name('countries/list');

Route::get('/admin/countries/create', 'Admin\CountriesController@create')->name('countries/create');

Route::post('/admin/countries/create_action', 'Admin\CountriesController@create_action')->name('countries/create_action');

Route::get('/admin/countries/update/{id}', 'Admin\CountriesController@update')->name('countries/update');

Route::post('/admin/countries/update_action', 'Admin\CountriesController@update_action')->name('countries/update_action');

Route::get('/admin/countries/ajaxCountryList', 'AjaxdataController@ajaxCountryList')->name('countries/ajaxCountryList');

Route::post('/admin/countries/change_action', 'Admin\CountriesController@change_action')->name('countries/change_action');

Route::post('/admin/countries/delete_action', 'Admin\CountriesController@delete_action')->name('countries/delete_action');





//States

Route::get('/admin/states/list', 'Admin\StatesController@index')->name('states/list');

Route::get('/admin/states/create', 'Admin\StatesController@create')->name('states/create');

Route::post('/admin/states/create_action', 'Admin\StatesController@create_action')->name('states/create_action');



Route::get('/admin/states/update/{id}', 'Admin\StatesController@update')->name('states/update');

Route::post('/admin/states/update_action', 'Admin\StatesController@update_action')->name('states/update_action');



Route::get('/admin/states/ajaxStatesList', 'AjaxdataController@ajaxStatesList')->name('states/ajaxStatesList');



Route::post('/admin/states/change_action', 'Admin\StatesController@change_action')->name('states/change_action');



Route::post('/admin/states/delete_action', 'Admin\StatesController@delete_action')->name('states/delete_action');





//Districts

Route::get('/admin/districts/list', 'Admin\DistrictsController@index')->name('districts/list');

Route::get('/admin/districts/create', 'Admin\DistrictsController@create')->name('districts/create');

Route::post('/admin/districts/create_action', 'Admin\DistrictsController@create_action')->name('districts/create_action');

Route::post('/admin/districts/getStates', 'Admin\DistrictsController@getStates')->name('districts/getStates');

Route::post('/admin/districts/getDistricts', 'Admin\DistrictsController@getDistricts')->name('districts/getDistricts');

Route::get('/admin/districts/update/{id}', 'Admin\DistrictsController@update')->name('districts/update');

Route::post('/admin/districts/update_action', 'Admin\DistrictsController@update_action')->name('districts/update_action');

Route::get('/admin/districts/ajaxDistrictList', 'AjaxdataController@ajaxDistrictList')->name('districts/ajaxDistrictList');

Route::post('/admin/districts/change_action', 'Admin\DistrictsController@change_action')->name('districts/change_action');



Route::post('/admin/districts/delete_action', 'Admin\DistrictsController@delete_action')->name('districts/delete_action');



//Employement Type

Route::get('/admin/employment_types/list', 'Admin\Employment_typesController@index')->name('employment_types/list');

Route::get('/admin/employment_types/create', 'Admin\Employment_typesController@create')->name('employment_types/create');

Route::post('/admin/employment_types/create_action', 'Admin\Employment_typesController@create_action')->name('employment_types/create_action');

Route::get('/admin/employment_types/update/{id}', 'Admin\Employment_typesController@update')->name('employment_types/update');

Route::post('/admin/employment_types/update_action', 'Admin\Employment_typesController@update_action')->name('employment_types/update_action');

Route::get('/admin/employment_types/ajaxemployment_typeList', 'AjaxdataController@ajaxEmployment_typeList')->name('employment_type/ajaxemployment_typeList');

Route::post('/admin/employment_types/change_action', 'Admin\Employment_typesController@change_action')->name('employment_types/change_action');

Route::post('/admin/employment_types/delete_action', 'Admin\Employment_typesController@delete_action')->name('employment_types/delete_action');





//Recruiters

Route::get('/admin/recruiters/list', 'Admin\RecruitersController@index')->name('recruiters/list');

Route::get('/admin/recruiters/view/{id}', 'Admin\RecruitersController@show')->name('recruiters/view');

Route::get('/admin/recruiters/ajaxRecruiterList', 'AjaxdataController@ajaxRecruiterList')->name('recruiters/ajaxRecruiterList');



//Candidates

Route::get('/admin/candidates/list', 'Admin\RecruitersController@candidate_index')->name('candidates/list');

Route::get('/admin/candidates/view/{id}', 'Admin\RecruitersController@candidate_show')->name('candidates/view');

Route::get('/admin/candidates/ajaxCandidatesList', 'AjaxdataController@ajaxCandidatesList')->name('candidates/ajaxCandidatesList');



//Jobs Approval

Route::get('/admin/jobs/list', 'Admin\JobsController@index')->name('jobs/list');

Route::get('/admin/jobs/view/{id}', 'Admin\JobsController@job_view')->name('jobs/view');

Route::get('/admin/jobs/ajaxJobsList', 'AjaxdataController@ajaxJobsList')->name('jobs/ajaxJobsList');





//settings groups



Route::get('/admin/settings/create', 'Admin\SettingsController@create')->name('settings/create');

Route::post('/admin/settings/update_action', 'Admin\SettingsController@update_action')->name('settings/update_action');

/*

Route::group(['prefix'=>'admin/settings'], function(){

    Route::get('/create', 'Admin/SettingsController@create');

    Route::post('/update_action',  'Admin/SettingsController@update_action');

  //  return 'dsad';

});*/





//cms

Route::get('/admin/cms_pages/list', 'Admin\Cms_pagesController@index')->name('cms_pages/list');

Route::get('/admin/cms_pages/create', 'Admin\Cms_pagesController@create')->name('cms_pages/create');

Route::post('/admin/cms_pages/create_action', 'Admin\Cms_pagesController@create_action')->name('cms_pages/create_action');

Route::get('/admin/cms_pages/ajaxSkillList', 'AjaxdataController@ajaxCms_pagesList')->name('cms_pages/ajaxCms_pagesList');

Route::get('/admin/cms_pages/update/{id}', 'Admin\Cms_pagesController@update')->name('cms_pages/update');

Route::post('/admin/cms_pages/update_action', 'Admin\Cms_pagesController@update_action')->name('cms_pages/update_action');

Route::post('/admin/cms_pages/change_action', 'Admin\Cms_pagesController@change_action')->name('cms_pages/change_action');

Route::post('/admin/cms_pages/delete_action', 'Admin\Cms_pagesController@delete_action')->name('cms_pages/delete_action');

Route::get('/admin/cms_pages/show/{id}', 'Admin\Cms_pagesController@show')->name('cms_pages/show');



//Packages

Route::group(['prefix'=>'admin/packages'], function(){

    Route::get('/', 'Admin\PackagesController@index');

    Route::get('/create', 'Admin\PackagesController@create');

    Route::post('/create_action',  'Admin\PackagesController@create_action');

    Route::get('/ajaxPackagesList',  'AjaxdataController@ajaxPackagesList');

    Route::get('/update/{id}',  'Admin\PackagesController@update');

    Route::post('/update_action',  'Admin\PackagesController@update_action');

    Route::post('/change_action',  'Admin\PackagesController@change_action');

    Route::post('/delete_action',  'Admin\PackagesController@delete_action');

    Route::get('/show/{id}',  'Admin\PackagesController@show');

});



//Profile Benefites

Route::group(['prefix'=>'admin/profilebenefits'], function(){

    Route::get('/', 'Admin\ProfileBenefitsController@index');

    Route::get('/create', 'Admin\ProfileBenefitsController@create');

    Route::post('/create_action',  'Admin\ProfileBenefitsController@create_action');

    Route::get('/ajaxProfileBenefitsList',  'AjaxdataController@ajaxProfileBenefitsList');

    Route::get('/update/{id}',  'Admin\ProfileBenefitsController@update');

    Route::post('/update_action',  'Admin\ProfileBenefitsController@update_action');

    Route::post('/change_action',  'Admin\ProfileBenefitsController@change_action');

    Route::post('/delete_action',  'Admin\ProfileBenefitsController@delete_action');

    Route::get('/show/{id}',  'Admin\ProfileBenefitsController@show');

});



// Inactive Jobs

//Profile Benefites

Route::get('/admin/jobsinactive/list', 'Admin\JobsInactiveController@index')->name('jobsinactive/list');

Route::get('/admin/jobsinactive/view/{id}', 'Admin\JobsInactiveController@job_view')->name('jobsinactive/view');

Route::get('/admin/jobsinactive/ajaxJobsInactiveList', 'AjaxdataController@ajaxJobsInactiveList')->name('jobsinactive/ajaxJobsInactiveList');

Route::post('/admin/jobsinactive/approveJobs', 'Admin\JobsInactiveController@approveJobs')->name('jobsinactive/approveJobs');


//Profession

Route::get('/admin/profession/list', 'Admin\ProfessionController@index')->name('profession/list');

Route::get('/admin/profession/create', 'Admin\ProfessionController@create')->name('profession/create');

Route::post('/admin/profession/create_action', 'Admin\ProfessionController@create_action')->name('profession/create_action');

Route::get('/admin/profession/update/{id}', 'Admin\ProfessionController@update')->name('profession/update');

Route::post('/admin/profession/update_action', 'Admin\ProfessionController@update_action')->name('profession/update_action');

Route::get('/admin/profession/ajaxProfessionList', 'AjaxdataController@ajaxProfessionList')->name('profession/ajaxProfessionList');

Route::post('/admin/profession/change_action', 'Admin\ProfessionController@change_action')->name('profession/change_action');

Route::post('/admin/profession/delete_action', 'Admin\ProfessionController@delete_action')->name('profession/delete_action');
Route::post('/admin/profession/check_duplication', 'Admin\ProfessionController@check_duplication')->name('profession/check_duplication');


//Domain

Route::get('/admin/domain/list', 'Admin\DomainController@index')->name('domain/list');

Route::get('/admin/domain/create', 'Admin\DomainController@create')->name('domain/create');

Route::post('/admin/domain/create_action', 'Admin\DomainController@create_action')->name('domain/create_action');

Route::get('/admin/domain/update/{id}', 'Admin\DomainController@update')->name('domain/update');

Route::post('/admin/domain/update_action', 'Admin\DomainController@update_action')->name('domain/update_action');

Route::get('/admin/domain/ajaxDomainList', 'AjaxdataController@ajaxDomainList')->name('domain/ajaxDomainList');

Route::post('/admin/domain/change_action', 'Admin\DomainController@change_action')->name('domain/change_action');

Route::post('/admin/domain/delete_action', 'Admin\DomainController@delete_action')->name('domain/delete_action');
Route::post('/admin/domain/check_duplication', 'Admin\DomainController@check_duplication')->name('domain/check_duplication');


//Baners

Route::get('/admin/baners/list', 'Admin\BanerController@index')->name('baners/list');

Route::get('/admin/baners/create', 'Admin\BanerController@create')->name('baners/create');

Route::post('/admin/baners/create_action', 'Admin\BanerController@create_action')->name('baners/create_action');

Route::get('/admin/baners/update/{id}', 'Admin\BanerController@update')->name('baners/update');

Route::post('/admin/baners/update_action', 'Admin\BanerController@update_action')->name('baners/update_action');

Route::get('/admin/baners/ajaxBanerList', 'AjaxdataController@ajaxBanerList')->name('baners/ajaxBanerList');

Route::post('/admin/baners/change_action', 'Admin\BanerController@change_action')->name('baners/change_action');

Route::post('/admin/baners/delete_action', 'Admin\BanerController@delete_action')->name('baners/delete_action');
Route::post('/admin/baners/check_duplication', 'Admin\BanerController@check_duplication')->name('baners/check_duplication');

//Areas

Route::get('/admin/areas/list', 'Admin\AreasController@index')->name('areas/list');


//Colleges

Route::get('/admin/colleges/list', 'Admin\CollegesController@index')->name('colleges/list');

Route::get('/admin/colleges/create', 'Admin\CollegesController@create')->name('colleges/create');

Route::post('/admin/colleges/getDomain', 'Admin\CollegesController@getDomain')->name('colleges/getDomain');

Route::post('/admin/colleges/getColleges', 'Admin\CollegesController@getColleges')->name('colleges/getColleges');

Route::post('/admin/colleges/create_action', 'Admin\CollegesController@create_action')->name('colleges/create_action');

Route::get('/admin/colleges/update/{id}', 'Admin\CollegesController@update')->name('colleges/update');

Route::post('/admin/colleges/update_action', 'Admin\CollegesController@update_action')->name('colleges/update_action');

Route::get('/admin/colleges/ajaxCollegesList', 'AjaxdataController@ajaxCollegesList')->name('colleges/ajaxCollegesList');

Route::post('/admin/colleges/change_action', 'Admin\CollegesController@change_action')->name('colleges/change_action');

Route::post('/admin/colleges/delete_action', 'Admin\CollegesController@delete_action')->name('colleges/delete_action');
Route::post('/admin/colleges/check_duplication', 'Admin\CollegesController@check_duplication')->name('colleges/check_duplication');


/*Area of Work*/


Route::get('/admin/areas/list', 'Admin\AreasController@index')->name('areas/list');

Route::get('/admin/areas/create', 'Admin\AreasController@create')->name('areas/create');

Route::post('/admin/areas/create_action', 'Admin\AreasController@create_action')->name('areas/create_action');

Route::post('/admin/areas/getDomains', 'Admin\AreasController@getDomains')->name('areas/getDomains');

/*Route::post('/admin/areas/getDistricts', 'Admin\AreasController@getDistricts')->name('areas/getDistricts');*/
Route::get('/admin/areas/update/{id}', 'Admin\AreasController@update')->name('areas/update');

Route::post('/admin/areas/update_action', 'Admin\AreasController@update_action')->name('areas/update_action');
Route::get('/admin/areas/ajaxAreasList', 'AjaxdataController@ajaxAreasList')->name('areas/ajaxAreasList');

Route::post('/admin/areas/change_action', 'Admin\AreasController@change_action')->name('areas/change_action');
Route::post('/admin/areas/delete_action', 'Admin\AreasController@delete_action')->name('areas/delete_action');

//Connection
Route::get('/admin/connections/list', 'Admin\ConnectionsController@index')->name('connections/list');
Route::get('/admin/connections/create', 'Admin\ConnectionsController@create')->name('connections/create');
Route::post('/admin/connections/create_action', 'Admin\ConnectionsController@create_action')->name('connections/create_action');
Route::post('/admin/connections/getDomains', 'Admin\ConnectionsController@getDomains')->name('connections/getDomains');
Route::post('/admin/connections/getAreaworks', 'Admin\ConnectionsController@getAreaworks')->name('connections/getAreaworks');
Route::post('/admin/connections/getCollege', 'Admin\ConnectionsController@getCollege')->name('connections/getCollege');
Route::get('/admin/connections/update/{id}', 'Admin\ConnectionsController@update')->name('connections/update');
Route::post('/admin/connections/update_action', 'Admin\ConnectionsController@update_action')->name('connections/update_action');
Route::get('/admin/connections/ajaxConnectionList', 'AjaxdataController@ajaxConnectionList')->name('connections/ajaxConnectionList');
Route::post('/admin/connections/change_action', 'Admin\ConnectionsController@change_action')->name('connections/change_action');
Route::post('/admin/connections/delete_action', 'Admin\ConnectionsController@delete_action')->name('connections/delete_action');


//discipline 

Route::get('/admin/discipline/list', 'Admin\DiscplineController@index')->name('discipline/list');
Route::get('/admin/discipline/create', 'Admin\DiscplineController@create')->name('discipline/create');

Route::post('/admin/discipline/create_action', 'Admin\DiscplineController@create_action')->name('discipline/create_action');

Route::get('/admin/discipline/update/{id}', 'Admin\DiscplineController@update')->name('discipline/update');

Route::post('/admin/discipline/update_action', 'Admin\DiscplineController@update_action')->name('discipline/update_action');

Route::get('/admin/discipline/ajaxDisciplineList', 'Admin\DiscplineController@ajaxDisciplineList')->name('discipline/ajaxDisciplineList');

Route::post('/admin/discipline/change_action', 'Admin\DiscplineController@change_action')->name('discipline/change_action');

Route::post('/admin/discipline/delete_action', 'Admin\DiscplineController@delete_action')->name('discipline/delete_action');
Route::post('/admin/discipline/check_duplication', 'Admin\DiscplineController@check_duplication')->name('discipline/check_duplication');

/*=== CGPA ===*/
Route::get('/admin/cgpa/list', 'Admin\CgpaController@index')->name('cgpa/list');
Route::get('/admin/cgpa/create', 'Admin\CgpaController@create')->name('cgpa/create');
Route::post('/admin/cgpa/create_action', 'Admin\CgpaController@create_action')->name('cgpa/create_action');
Route::get('/admin/cgpa/update/{id}', 'Admin\CgpaController@update')->name('cgpa/update');
Route::post('/admin/cgpa/update_action', 'Admin\CgpaController@update_action')->name('cgpa/update_action');
Route::get('/admin/cgpa/ajaxCgpaList', 'AjaxdataController@ajaxCgpaList')->name('cgpa/ajaxCgpaList');
Route::post('/admin/cgpa/change_action', 'Admin\CgpaController@change_action')->name('cgpa/change_action');
Route::post('/admin/cgpa/delete_action', 'Admin\CgpaController@delete_action')->name('cgpa/delete_action');
Route::post('/admin/cgpa/check_duplication', 'Admin\CgpaController@check_duplication')->name('cgpa/check_duplication');


/*==Scategories  =*/
Route::get('/admin/scategories/list', 'Admin\ScategoriesController@index')->name('scategories/list');
Route::get('/admin/scategories/create', 'Admin\ScategoriesController@create')->name('scategories/create');
Route::post('/admin/scategories/create_action', 'Admin\ScategoriesController@create_action')->name('scategories/create_action');
Route::get('/admin/scategories/update/{id}', 'Admin\ScategoriesController@update')->name('scategories/update');
Route::post('/admin/scategories/update_action', 'Admin\ScategoriesController@update_action')->name('scategories/update_action');
Route::get('/admin/scategories/ajaxScategoriesList', 'AjaxdataController@ajaxScategoriesList')->name('scategories/ajaxScategoriesList');
Route::post('/admin/scategories/change_action', 'Admin\ScategoriesController@change_action')->name('scategories/change_action');
Route::post('/admin/scategories/delete_action', 'Admin\ScategoriesController@delete_action')->name('scategories/delete_action');
Route::post('/admin/scategories/check_duplication', 'Admin\ScategoriesController@check_duplication')->name('scategories/check_duplication');

//Notification 

Route::get('/admin/notification/list', 'Admin\NotificationCategoryController@index')->name('notification/list');
Route::get('/admin/notification/create', 'Admin\NotificationCategoryController@create')->name('notification/create');

Route::post('/admin/notification/create_action', 'Admin\NotificationCategoryController@create_action')->name('notification/create_action');

Route::get('/admin/notification/update/{id}', 'Admin\NotificationCategoryController@update')->name('notification/update');

Route::post('/admin/notification/update_action', 'Admin\NotificationCategoryController@update_action')->name('notification/update_action');

Route::get('/notification/ajaxNotificationList', 'Admin\NotificationCategoryController@ajaxNotificationList')->name('notification/ajaxNotificationList');

Route::post('/admin/notification/change_action', 'Admin\NotificationCategoryController@change_action')->name('notification/change_action');

Route::post('/admin/notification/delete_action', 'Admin\NotificationCategoryController@delete_action')->name('notification/delete_action');
Route::post('/admin/notification/check_duplication', 'Admin\NotificationCategoryController@check_duplication')->name('notification/check_duplication');


//Notifications

Route::get('/admin/notifications/list', 'Admin\NotificationsController@index')->name('notifications/list');

Route::get('/admin/notifications/create', 'Admin\NotificationsController@create')->name('notifications/create');

Route::post('/admin/notifications/getDomain', 'Admin\NotificationsController@getDomain')->name('notifications/getDomain');

Route::post('/admin/notifications/getColleges', 'Admin\NotificationsController@getColleges')->name('notifications/getColleges');

Route::post('/admin/notifications/create_action', 'Admin\NotificationsController@create_action')->name('notifications/create_action');

Route::get('/admin/notifications/update/{id}', 'Admin\NotificationsController@update')->name('notifications/update');

Route::post('/admin/notifications/update_action', 'Admin\NotificationsController@update_action')->name('notifications/update_action');

Route::get('/admin/notifications/ajaxNotificationsList', 'Admin\NotificationsController@ajaxNotificationsList')->name('notifications/ajaxNotificationsList');

Route::post('/admin/notifications/change_action', 'Admin\NotificationsController@change_action')->name('notifications/change_action');

Route::post('/admin/notifications/delete_action', 'Admin\NotificationsController@delete_action')->name('notifications/delete_action');
Route::post('/admin/notifications/check_duplication', 'Admin\NotificationsController@check_duplication')->name('notifications/check_duplication');

Route::post('/admin/notifications/getStates', 'Admin\NotificationsController@getStates')->name('notifications/getStates');

Route::post('/admin/notifications/getDistricts', 'Admin\NotificationsController@getDistricts')->name('notifications/getDistricts');