 <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){
	return view('front-end.pages.index');
});


Route::prefix('home')->group(function () {
	Route::get('register', 'front_end\UserController@register');
	Route::post('userregister', 'front_end\UserController@userregister');
	Route::get('login', 'front_end\UserController@login');
	Route::post('loginaction', 'front_end\UserController@loginaction');
	Route::get('dashboard', 'front_end\UserController@dashboard');
	Route::post('getStates', 'front_end\UserController@getStates');
	Route::post('getDistricts', 'front_end\UserController@getDistricts');
	Route::post('checkduplication', 'front_end\UserController@checkduplication_reg');
	Route::post('checkduplication1', 'front_end\UserController@checkduplication_reg1');
	Route::post('getprofession_name', 'front_end\UserController@getprofession_name');
	Route::get('otpverification/{id}', 'front_end\UserController@otpverification');
	Route::post('verifyotp', 'front_end\UserController@verifyotp');
	Route::get('logout', 'front_end\UserController@logout');
	Route::get('editprofile', 'front_end\UserController@editprofile');
	Route::get('changepassword', 'front_end\UserController@changepassword');
	Route::post('editprofileaction', 'front_end\UserController@editprofileaction');
	Route::get('activateprofile', 'front_end\UserController@activateprofile');
	Route::post('savelike', 'front_end\PostaddController@savelike')->name('home/savelike');
	Route::post('save_post_add', 'front_end\PostaddController@save_post_add')->name('home/save_post_add');


	/*Cms Page data*/
	Route::get('about-us', 'front_end\CmsController@about_us');
	
});

Route::post('send-otp', 'front_end\UserController@sendOTP');
Route::post('verify-otp', 'front_end\UserController@verifyOTP');

Route::prefix('payment')->group(function () {
Route::post('payment', 'front_end\PaymentController@payment')->name('payment');
Route::get('returnurl', 'front_end\PaymentController@returnurl')->name('returnurl');
Route::get('payment_success', 'front_end\PaymentController@payment_success')->name('payment/payment_success');
Route::get('payment_failed', 'front_end\PaymentController@payment_failed')->name('payment/payment_failed');

});